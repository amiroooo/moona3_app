package com.example.amir.moona3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.dd.CircularProgressButton;
import com.github.library.bubbleview.BubbleTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Amir on 12/31/2016.
 */

public class RoundFragmentField extends Fragment {
    private static String PARCELABLE_ITEM = "parcel_item";
    private RoundItem roundItem1;
    private RoundItem roundItem2;
    private View root;

    private CircularProgressButton startButton;

    private Button saveObstacleButton;
    private TextView cronMainTextView;
    private AutofitTextView groupName;

    private ElegantNumberButton leftTop;

    private RelativeLayout relativeLayout;

    private CardView pointsCardView;
    private CardView obstacleCard;
    private CardView editRoundCard;


    private Toast toastObject;
    private BubbleTextView bubbleTextVew;


    private RecyclerView recyclerView1;
    private RecyclerView recyclerView2;

    private ArrayList<RoundPartItem> adapterData1;
    private RecyclerRoundPartAdapter roundItemAdapter1;


    private TextView rightGroupTitleTextView;
    private TextView leftGroupTitleTextView;

    private ArrayList<RoundPartItem> adapterData2;
    private RecyclerRoundPartAdapter roundItemAdapter2;

    private Button chooseLeftRoundButton;
    private Button chooseRightRoundButton;

    private Button moveLeftButton;
    private Button moveRightButton;
    private int initialPosition = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    public String getRoundItem1Id(){
        if (roundItem1 == null){
            return "";
        }
        return roundItem1.get_id();
    }
    public String getRoundItem2Id(){
        if (roundItem2 == null){
            return "";
        }
        return roundItem2.get_id();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemDeletion(ItemToDelete event){
        if ((event.getDocumentId()).equals(roundItem1.get_id())){
            FragmentManager frmgr = getFragmentManager();
            if (frmgr != null) {

                if (((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1 != null){
                    ((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1.cancel();
                    ((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1 = null;
                }

                frmgr.popBackStack();
            }
        }
        if ((event.getDocumentId()).equals(roundItem2.get_id())){
            FragmentManager frmgr = getFragmentManager();
            if (frmgr != null) {
                if (((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1 != null){
                    ((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1.cancel();
                    ((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1 = null;
                }
                frmgr.popBackStack();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notifyChangeInRoundItem(MoveFirstToSecond event) {
        if (roundItem1.get_id().equals(event.getRoundId())) {
            moveFirstToSecondView();
        }
    }

    public void moveFirstToSecondView(){

        Config.adapterData2 = adapterData1;
        Config.roundItemAdapter2 = roundItemAdapter1;
        Config.roundItem2 = roundItem1;
        roundItem2 = roundItem1;
        roundItemAdapter2 = roundItemAdapter1;
        adapterData2 = adapterData1;
        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView2.setAdapter(Config.roundItemAdapter2);
        recyclerView2.setItemViewCacheSize(20);
        rightGroupTitleTextView.setText("קבוצה מארחת: " + getGroupNameByRoundId(roundItem2.get_id()) + " חדר " + getGroupNumByRoundId(roundItem2.get_id()));

    }

    public void loadRightView(){
        if (Config.roundItem2 != null) {

            if (Config.roundItemAdapter2 != null) {
                roundItemAdapter2 = Config.roundItemAdapter2;
                adapterData2 = Config.adapterData2;
            }else{
                roundItemAdapter2 = new RecyclerRoundPartAdapter(Config.roundItem2.get_id()) ;
                adapterData2 = buildAdapterDataByRound(Config.roundItem2, roundItemAdapter2);
                roundItemAdapter2.setData(adapterData2);
            }

            roundItem2 = Config.roundItem2;
            recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView2.setAdapter(roundItemAdapter2);
            recyclerView2.setItemViewCacheSize(20);
            rightGroupTitleTextView.setText("קבוצה מארחת: " + getGroupNameByRoundId(roundItem2.get_id()) + " חדר " + getGroupNumByRoundId(roundItem2.get_id()));

        }
    }

    public void loadLeftView(){
        if (Config.roundItem1 != null) {
            roundItem1 = Config.roundItem1;
            if (Config.roundItemAdapter1 != null) {
                roundItemAdapter1 = Config.roundItemAdapter1;
                adapterData1 = Config.adapterData1;
            }else{
                roundItemAdapter1 = new RecyclerRoundPartAdapter(Config.roundItem1.get_id()) ;
                adapterData1 = buildAdapterDataByRound(roundItem1, roundItemAdapter1);
                roundItemAdapter1.setData(adapterData1);
            }

            recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView1.setAdapter(roundItemAdapter1);
            recyclerView1.setItemViewCacheSize(20);
            leftGroupTitleTextView.setText("קבוצה מארחת: " + getGroupNameByRoundId(roundItem1.get_id()) + " חדר " + getGroupNumByRoundId(roundItem1.get_id()));

        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoundPartChangeData(RoundPartDataTypeChange event){
        if (adapterData1 != null){
            for (int i = 0; i < adapterData1.size(); i++) {
                if (adapterData1.get(i).get_id().equals(event.get_id())){
                    recyclerView1.getRecycledViewPool().clear();
                    adapterData1.set(i, (RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                    if (event.isDataRequireRefresh()) {
                        if (roundItemAdapter1 != null && event.get_id().equals(roundItemAdapter1.getWorkTeamPart().get_id())){
                            roundItemAdapter1.setTeamCrackerPart((RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                        }
                        roundItemAdapter1.notifyItemChanged(i);
                    }
                }
            }
        }
        if (adapterData2 != null){
            for (int i = 0; i < adapterData2.size(); i++) {
                if (adapterData2.get(i).get_id().equals(event.get_id())){
                    recyclerView2.getRecycledViewPool().clear();
                    adapterData2.set(i, (RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                    if (event.isDataRequireRefresh()) {
                        if (roundItemAdapter2 != null && event.get_id().equals(roundItemAdapter2.getWorkTeamPart().get_id())){
                            roundItemAdapter2.setTeamCrackerPart((RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                        }
                        roundItemAdapter2.notifyItemChanged(i);
                    }
                }
            }
        }
    }


    private void showTalkbackDurationTooltip (String data) {
        if ( bubbleTextVew != null && bubbleTextVew.getVisibility () != View.VISIBLE ) {
            bubbleTextVew.setText (data);
            bubbleTextVew.setVisibility ( View.VISIBLE );
            AnimHelper.scaleInOut ( bubbleTextVew, 300, 0.15f );
            bubbleTextVew.postDelayed ( new Runnable () {
                @Override
                public void run ( ) {
                    bubbleTextVew.setVisibility ( View.GONE );
                }
            }, 1000 );
        }
    }


    private void bindRoundData() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            initialPosition = bundle.getInt("position");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.round_fragment, container, false);
        initViews(root);
        bindData();
        initListeners();
        pager.setCurrentItem(initialPosition-1);
        return root;
    }



    private void initViews(View v) {

        relativeLayout = (RelativeLayout)v.findViewById(R.id.frag_relative);
        recyclerView1 = (RecyclerView)v.findViewById(R.id.round_item_recycler1);
        recyclerView2 = (RecyclerView)v.findViewById(R.id.round_item_recycler2);

        rightGroupTitleTextView = (TextView)v.findViewById(R.id.round_title_right);
        leftGroupTitleTextView = (TextView)v.findViewById(R.id.round_title_left);
        chooseLeftRoundButton = (Button)v.findViewById(R.id.choose_round_button);
        chooseRightRoundButton = (Button)v.findViewById(R.id.choose_round_button2);

        moveLeftButton = (Button)v.findViewById(R.id.to_left_button);
        moveRightButton = (Button)v.findViewById(R.id.to_right_button);
        DoubleRoundPagerAdapter adapter = new DoubleRoundPagerAdapter();
        pager = (ViewPager) v.findViewById(R.id.container2);
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("test", "onPageSelected: "+position);
                Config.lastChosenPos = position+1;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    ViewPager pager;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }




    private ArrayList<RoundPartItem> buildAdapterDataByRound(RoundItem roundItem, RecyclerRoundPartAdapter roundItemAdapter){
        HashMap<String,RoundPartItem> roundParts = getRoundParts(roundItem);
        ArrayList<RoundPartItem> adapterData = new ArrayList<>();
        switch (roundItem.getRound_type()){
            case 0:{
                RoundPartItem cronoItem = roundParts.get("main_cronometer");
                if (cronoItem != null){
                    adapterData.add(cronoItem);
                }

                RoundPartItem phase1Item = roundParts.get("phase1");
                if (phase1Item != null){
                    adapterData.add(phase1Item);
                }
                RoundPartItem phase2Item = roundParts.get("phase2");
                if (phase2Item != null){
                    adapterData.add(phase2Item);
                }
                RoundPartItem phase3Item = roundParts.get("phase3");
                if (phase3Item != null){
                    adapterData.add(phase3Item);
                }
                RoundPartItem copterItem = roundParts.get("copter");
                if (copterItem != null){
                    adapterData.add(copterItem);
                }
                RoundPartItem catcherItem = roundParts.get("catcher");
                if (catcherItem != null){
                    adapterData.add(catcherItem);
                }
                RoundPartItem innovationItem = roundParts.get("innovation");
                if (innovationItem != null){
                    adapterData.add(innovationItem);
                }


                break;
            }
            case 1:{

                RoundPartItem cronoItem = roundParts.get("main_cronometer");
                if (cronoItem != null){
                    adapterData.add(cronoItem);
                }
                RoundPartItem comunityScoreItem = roundParts.get("community_score");
                if (comunityScoreItem != null){
                    adapterData.add(comunityScoreItem);
                }
                break;
            }
            case 2:{
                roundItemAdapter.setIsRoundPuzzle(true);
                RoundPartItem cronoItem = roundParts.get("main_cronometer");
                if (cronoItem != null){
                    adapterData.add(cronoItem);
                }
                RoundPartItem timeItem = roundParts.get("time");
                if (timeItem != null){
                    adapterData.add(timeItem);
                }
                RoundPartItem storyItem = roundParts.get("story");
                if (storyItem != null){
                    adapterData.add(storyItem);
                }
                RoundPartItem atmosphereItem = roundParts.get("atmosphere");
                if (atmosphereItem != null){
                    adapterData.add(atmosphereItem);
                }
                RoundPartItem puzzle_diversityItem = roundParts.get("puzzle_diversity");
                if (puzzle_diversityItem != null){
                    adapterData.add(puzzle_diversityItem);
                }
                RoundPartItem mechanism_diversityItem = roundParts.get("mechanism_diversity");
                if (mechanism_diversityItem != null){
                    adapterData.add(mechanism_diversityItem);
                }
                RoundPartItem cluesItem = roundParts.get("clues");
                if (cluesItem != null){
                    adapterData.add(cluesItem);
                }
                RoundPartItem learning_connectionItem = roundParts.get("learning_connection");
                if (learning_connectionItem != null){
                    adapterData.add(learning_connectionItem);
                }

                RoundPartItem effectItem = roundParts.get("effect");
                if (effectItem != null){
                    adapterData.add(effectItem);
                }

                RoundPartItem room_impressionItem = roundParts.get("room_impression");
                if (room_impressionItem != null){
                    adapterData.add(room_impressionItem);
                }

                RoundPartItem excelled_puzzleItem = roundParts.get("excelled_puzzle");
                if (excelled_puzzleItem != null){
                    adapterData.add(excelled_puzzleItem);
                }


                RoundPartItem innovationItem = roundParts.get("innovation");
                if (innovationItem != null){
                    adapterData.add(innovationItem);
                }


                RoundPartItem team_workItem = roundParts.get("team_work");
                if (team_workItem != null){
                    adapterData.add(team_workItem);
                    roundItemAdapter.setTeamCrackerPart(team_workItem);
                }

                break;
            }
            case 3:{

                RoundPartItem cronoItem = roundParts.get("main_cronometer");
                if (cronoItem != null){
                    adapterData.add(cronoItem);
                }
                RoundPartItem comunityScoreItem = roundParts.get("community_score");
                if (comunityScoreItem != null){
                    adapterData.add(comunityScoreItem);
                }
                RoundPartItem escapePackage = roundParts.get("escape_package");
                if (escapePackage != null){
                    adapterData.add(escapePackage);
                }
                break;
            }
        }
        return adapterData;
    }




    private void bindData(){
        loadRightView();
        loadLeftView();
    }


    private String getGroupNameByRoundId(String round_id) {
        RoundItem ri = (RoundItem) Config.roundsMap.get(round_id);
        if (ri != null) {
            GroupItem gi = (GroupItem) Config.groupsMap.get(ri.getGroup());
            return gi.getGroupName();
        }
        return "";
    }


    private String getGroupNumByRoundId(String round_id) {
        RoundItem ri = (RoundItem) Config.roundsMap.get(round_id);
        if (ri != null) {
            GroupItem gi = (GroupItem) Config.groupsMap.get(ri.getGroup());
            return gi.getNumberAsString();
        }
        return "";
    }


    private HashMap<String,RoundPartItem> getRoundParts(RoundItem roundItem){
        HashMap<String,RoundPartItem> result =  new HashMap<>();
        JSONObject value = null;
        try {
            value = new JSONObject().put("main_timer",roundItem.getRound_time()).put("data_type","main_timer").put("progressed","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        result.put("main_cronometer",new RoundPartItem("0",roundItem.get_id(),"main_cronometer", value ));
        Iterator<RecyclerItem> iterator = Config.roundPartsMap.values().iterator();
        while(iterator.hasNext()) {
            RoundPartItem roundPartItem = (RoundPartItem)iterator.next();
            if (roundPartItem.getRound_id().equals(roundItem.get_id())){
                result.put(roundPartItem.getRound_part_type(),roundPartItem);
            }
        }
        return result;
    }




    private void initListeners(){
        chooseLeftRoundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.nextPick = 1;
                Toast.makeText(getContext(),"בחר סיבוב לצד שמאלי",Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        });
        chooseRightRoundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.nextPick = 2;
                Toast.makeText(getContext(),"בחר סיבוב לצד ימיני",Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        });

        moveLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(0);
            }
        });
        moveRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(1);
            }
        });

    }

    private void handleRequestFinishMatchRound() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Do you want the match to move to review?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestReviewRound();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder1.create();
        alert.show();
    }

    private void requestReviewRound() {
        JSONArray params = new JSONArray();
        params.put(roundItem1.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL + Config.REVIEW_ROUND ,params, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "review_round");
    }


    @Override
    public void onPause() {
        super.onPause();
    }


    void onBackPressed(){
        FragmentManager frmgr = getFragmentManager();
        if (frmgr != null) {
            frmgr.popBackStack();
        }
    }

    public boolean isRound1Done(){
        if (roundItemAdapter2 != null) {
            ArrayList<RoundPartItem> roundPartItems = roundItemAdapter1.getData();
            boolean isAllDone = true;
            for (int i = 0; i < roundPartItems.size(); i++) {
                RoundPartItem roundPartItem = roundPartItems.get(i);
                if (!roundPartItem.isProgressed()) {
                    isAllDone = false;
                    break;
                }
            }
            return isAllDone;
        }
        return false;
    }

    public boolean isRound2Done(){
        if (roundItemAdapter2 != null) {
            ArrayList<RoundPartItem> roundPartItems = roundItemAdapter2.getData();
            boolean isAllDone = true;
            for (int i = 0; i < roundPartItems.size(); i++) {
                RoundPartItem roundPartItem = roundPartItems.get(i);
                if (!roundPartItem.isProgressed()) {
                    isAllDone = false;
                    break;
                }
            }
            return isAllDone;
        }
        return false;
    }

    class DoubleRoundPagerAdapter extends PagerAdapter {

        public Object instantiateItem(View collection, int position) {

            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.page_one;
                    break;
                case 1:
                    resId = R.id.page_two;
                    break;
            }
            return root.findViewById(resId);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }
    }


}
