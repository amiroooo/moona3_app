package com.example.amir.moona3;

/**
 * Created by Amir on 1/22/2017.
 */

public class SelectedStageEvent {
    public SelectedStageEvent(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    private String _id;
}
