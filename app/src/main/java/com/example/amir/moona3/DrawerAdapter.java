package com.example.amir.moona3;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.CircularProgressButton;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.util.List;
import java.util.Locale;

public class DrawerAdapter<T> extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    T data1 = null;
    T data2 = null;
    MainCountDownTimer countdownTimer1;
    MainCountDownTimer countdownTimer2;
    boolean isAllowedChangeTime1 = true;
    boolean isAllowedChangeTime2 = true;
    private List<T> mObjects;
    private CircularProgressButton[] circularButtonArray;

    public DrawerAdapter(Context mContext, int layoutResourceId, T data1,T data2) {
        super();
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data1 = data1;
        this.data2 = data2;
        circularButtonArray = new CircularProgressButton[2];
    }

    public void startTimerForAll(){
        notifyDataSetChanged();
        if (circularButtonArray[0] != null){
            circularButtonArray[0].postDelayed(new Runnable() {
                @Override
                public void run() {
                    isAllowedChangeTime1 = true;
                    circularButtonArray[0].performClick();
                }
            },1000);
        }
        if (circularButtonArray[1] != null){
            circularButtonArray[1].postDelayed(new Runnable() {
                @Override
                public void run() {
                    isAllowedChangeTime2 = true;
                    circularButtonArray[1].performClick();
                }
            },1000);
        }
    }


    public void updateItem(T item,int position){
        if (position == 0){
            data1 = item;
        }
        if (position == 1){
            data2 = item;
        }
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        if (position == 0){
            return data1;
        }
        if (position == 1){
            return data2;
        }
        return mObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        RoundItem data = null;

        if (position == 0){
            data = (RoundItem)data1;
        }
        if (position == 1){
            data = (RoundItem)data2;
        }

        final RoundItem daaaa = data;
        View listItem = convertView;

        LayoutInflater inflater = ((MainActivity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);

        //ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.imageViewIcon);
        TextView groupGame = (TextView) listItem.findViewById(R.id.group_title);
        if (data != null){
            GroupItem gi = (GroupItem)Config.groupsMap.get(data.getGroup());
            groupGame.setText("קבוצה: " + gi.getGroupName() + ", חדר: " +gi.getNumberAsString());
        }

        final CircularProgressButton circularButton = (CircularProgressButton)listItem.findViewById(R.id.circular_timer_button_start);
        if (position == 0){
            circularButtonArray[0] = circularButton;
        }
        if (position == 1){
            circularButtonArray[1] = circularButton;
        }

        final TextView cronometer = (TextView)listItem.findViewById(R.id.crono_timer_text) ;
        final Button pauseButton = (Button)listItem.findViewById(R.id.pause_round);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (circularButton.getProgress() > 0 && circularButton.getProgress() < 100) {
                    if (position == 0) {
                        if (isAllowedChangeTime1) {
                            isAllowedChangeTime1 = false;
                            circularButton.pauseProgress();
                            pauseButton.setText("המשך");
                        } else {
                            isAllowedChangeTime1 = true;
                            circularButton.resumeProgress();
                            pauseButton.setText("עצור");
                        }
                    } else {
                        if (isAllowedChangeTime2) {
                            isAllowedChangeTime2 = false;
                            circularButton.pauseProgress();
                            pauseButton.setText("המשך");
                        } else {
                            isAllowedChangeTime2 = true;
                            circularButton.resumeProgress();
                            pauseButton.setText("עצור");
                        }
                    }
                }
                //((MainActivity) mContext).mDrawerLayout.closeDrawers();
            }
        });
        int roundTime = 40;
        if(daaaa != null){
            roundTime = daaaa.getRound_time();
        }
        final int finalroundTime = roundTime;
        circularButton.setOnClickListener(new View.OnClickListener() {
            boolean isAllowedChangeTime = true;
            @Override
            public void onClick(View v) {
                if (circularButton.getProgress()==0) {
                    roundScanStartCounter(1000 * 60 * finalroundTime,position,circularButton,cronometer);
                }else{
                    if (position == 0) {
                        if (isAllowedChangeTime1) {
                            isAllowedChangeTime1 = false;
                            circularButton.pauseProgress();
                        } else {
                            isAllowedChangeTime1 = true;
                            circularButton.resumeProgress();
                        }
                    }else{
                        if (isAllowedChangeTime2) {
                            isAllowedChangeTime2 = false;
                            circularButton.pauseProgress();
                        } else {
                            isAllowedChangeTime2 = true;
                            circularButton.resumeProgress();
                        }
                    }
                }
            }
        });

        //String folder = data[position];


        //imageViewIcon.setImageResource(folder.icon);
        //textViewName.setText(folder.name);

        return listItem;
    }
    private void roundScanStartCounter(final int kkk, int pos,CircularProgressButton circularButton,final TextView chronometer){
        MainCountDownTimer countdownTimer = countdownTimer2;
        if (pos == 0){
            countdownTimer = countdownTimer1;
        }

        if (countdownTimer != null){
            countdownTimer.cancel();
        }
        if (pos == 0) {
            countdownTimer1 = new MainCountDownTimer(kkk, 1000) {

                public void onTick(long millisUntilFinished) {
                    timeLeft = millisUntilFinished;
                    Log.d("dddd", "onTick1: " + millisUntilFinished);
                    if (chronometer != null && isAllowedChangeTime1) {
                        int roundTime = 40;
                        if (data1 != null) {
                            roundTime = ((RoundItem) data1).getRound_time();
                        }
                        long timePassed = 1000 * 60 * roundTime - millisUntilFinished;
                        long second = (timePassed / 1000) % 60;
                        long minute = (timePassed / (1000 * 60)) % 60;
                        String time = String.format(Locale.ENGLISH, "%02d:%02d", minute, second);
                        chronometer.setText(time);
                    }

                    int left = (int) (millisUntilFinished * 100 / kkk);
                    if (left == 100) {
                        left = 99;
                    }
                    circularButton.setProgress(left);
                }

                public void onFinish() {
                    if (chronometer != null) {
                        chronometer.setText("");
                    }
                    circularButton.setProgress(100);
                    Toast.makeText(circularButton.getContext(), R.string.main_round_timer_done, Toast.LENGTH_LONG).show();
                    Vibrator v = (Vibrator) circularButton.getContext().getSystemService(circularButton.getContext().VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                }
            };
            countdownTimer1.cronometer = chronometer;
            countdownTimer1.circularButton = circularButton;
            countdownTimer1.start();
        }else{
            countdownTimer2 = new MainCountDownTimer(kkk, 1000) {

                public void onTick(long millisUntilFinished) {
                    timeLeft = millisUntilFinished;
                    Log.d("dddd", "onTick2: " + millisUntilFinished);
                    if (chronometer != null && isAllowedChangeTime2) {
                        int roundTime = 40;
                        if (data2 != null) {
                            roundTime = ((RoundItem) data2).getRound_time();
                        }
                        long timePassed = 1000 * 60 * roundTime - millisUntilFinished;
                        long second = (timePassed / 1000) % 60;
                        long minute = (timePassed / (1000 * 60)) % 60;
                        String time = String.format(Locale.ENGLISH, "%02d:%02d", minute, second);
                        chronometer.setText(time);
                    }

                    int left = (int) (millisUntilFinished * 100 / kkk);
                    if (left == 100) {
                        left = 99;
                    }
                    circularButton.setProgress(left);
                }

                public void onFinish() {
                    if (chronometer != null) {
                        chronometer.setText("");
                    }
                    circularButton.setProgress(100);
                    Toast.makeText(circularButton.getContext(), R.string.main_round_timer_done, Toast.LENGTH_LONG).show();
                    Vibrator v = (Vibrator) circularButton.getContext().getSystemService(circularButton.getContext().VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                }
            };
            countdownTimer2.cronometer = chronometer;
            countdownTimer2.circularButton = circularButton;
            countdownTimer2.start();
        }

    }
}