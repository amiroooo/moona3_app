package com.example.amir.moona3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.dd.CircularProgressButton;
import com.github.library.bubbleview.BubbleTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Amir on 12/31/2016.
 */

public class RoundFragment extends Fragment {
    private static String PARCELABLE_ITEM = "parcel_item";
    private RoundItem roundItem1;
    private RoundItem roundItem2;
    private View root;

    private CircularProgressButton startButton;

    private Button saveObstacleButton;
    private TextView cronMainTextView;
    private AutofitTextView groupName;

    private ElegantNumberButton leftTop;

    private RelativeLayout relativeLayout;

    private CardView pointsCardView;
    private CardView obstacleCard;
    private CardView editRoundCard;


    private Toast toastObject;
    private BubbleTextView bubbleTextVew;


    private RecyclerView recyclerView1;
    private RecyclerView recyclerView2;

    private ArrayList<RoundPartItem> adapterData1;
    private RecyclerRoundPartAdapter roundItemAdapter1;


    private TextView rightGroupTitleTextView;


    private ArrayList<RoundPartItem> adapterData2;
    private RecyclerRoundPartAdapter roundItemAdapter2;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    public String getRoundItem1Id(){
        if (roundItem1==null){
            return "";
        }
        return roundItem1.get_id();
    }
    public String getRoundItem2Id(){
        if (roundItem2==null){
            return "";
        }
        return roundItem2.get_id();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemDeletion(ItemToDelete event){
        if ((event.getDocumentId()).equals(roundItem1.get_id())){
            FragmentManager frmgr = getFragmentManager();
            if (frmgr != null) {

                if (((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1 != null){
                    ((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1.cancel();
                    ((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1 = null;
                }

                frmgr.popBackStack();
            }
        }
        if ((event.getDocumentId()).equals(roundItem2.get_id())){
            FragmentManager frmgr = getFragmentManager();
            if (frmgr != null) {
                if (((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1 != null){
                    ((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1.cancel();
                    ((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1 = null;
                }
                frmgr.popBackStack();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notifyChangeInRoundItem(MoveFirstToSecond event) {
        if (roundItem1.get_id().equals(event.getRoundId())) {
            moveFirstToSecondView();
        }
    }

    public void moveFirstToSecondView(){

        Config.adapterData2 = adapterData1;
        Config.roundItemAdapter2 = roundItemAdapter1;
        Config.roundItem2 = roundItem1;
        roundItem2 = roundItem1;
        roundItemAdapter2 = roundItemAdapter1;
        adapterData2 = adapterData1;
        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView2.setAdapter(Config.roundItemAdapter2);
        recyclerView2.setItemViewCacheSize(20);
        rightGroupTitleTextView.setText("קבוצה מארחת: " + getGroupNameByRoundId(roundItem2.get_id()) + " חדר " + getGroupNumByRoundId(roundItem2.get_id()));

    }

    public void loadSecondView(){
            if (Config.roundItemAdapter2 != null) {
                adapterData2 = Config.adapterData2;
                roundItemAdapter2= Config.roundItemAdapter2;
                roundItem2 = Config.roundItem2;
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView2.setAdapter(Config.roundItemAdapter2);
                recyclerView2.setItemViewCacheSize(20);
                rightGroupTitleTextView.setText("קבוצה מארחת: " + getGroupNameByRoundId(roundItem2.get_id()) + " חדר " + getGroupNumByRoundId(roundItem2.get_id()));
            }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoundPartChangeData(RoundPartDataTypeChange event){
        if (adapterData1 != null){
            for (int i = 0; i < adapterData1.size(); i++) {
                if (adapterData1.get(i).get_id().equals(event.get_id())){
                    recyclerView1.getRecycledViewPool().clear();
                    adapterData1.set(i, (RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                    if (event.isDataRequireRefresh()) {
                        if (roundItemAdapter1 != null && event.get_id().equals(roundItemAdapter1.getWorkTeamPart().get_id())){
                            roundItemAdapter1.setTeamCrackerPart((RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                        }
                        roundItemAdapter1.notifyItemChanged(i);
                    }
                }
            }
        }
        if (adapterData2 != null){
            for (int i = 0; i < adapterData2.size(); i++) {
                if (adapterData2.get(i).get_id().equals(event.get_id())){
                    recyclerView2.getRecycledViewPool().clear();
                    adapterData2.set(i, (RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                    if (event.isDataRequireRefresh()) {
                        if (roundItemAdapter2 != null && event.get_id().equals(roundItemAdapter2.getWorkTeamPart().get_id())){
                            roundItemAdapter2.setTeamCrackerPart((RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                        }
                        roundItemAdapter2.notifyItemChanged(i);
                    }
                }
            }
        }
    }





    private void showTalkbackDurationTooltip (String data) {
        if ( bubbleTextVew != null && bubbleTextVew.getVisibility () != View.VISIBLE ) {
            bubbleTextVew.setText (data);
            bubbleTextVew.setVisibility ( View.VISIBLE );
            AnimHelper.scaleInOut ( bubbleTextVew, 300, 0.15f );
            bubbleTextVew.postDelayed ( new Runnable () {
                @Override
                public void run ( ) {
                    bubbleTextVew.setVisibility ( View.GONE );
                }
            }, 1000 );
        }
    }


    private void bindRoundData() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            roundItem1 = bundle.getParcelable(PARCELABLE_ITEM);
        }
        if (Config.roundItem2 != null){
            roundItem2 = Config.roundItem2;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.round_fragment2, container, false);
        initViews(root);
        bindData();
        initListeners();
        return root;
    }



    private void initViews(View v) {

        relativeLayout = (RelativeLayout)v.findViewById(R.id.frag_relative);
        recyclerView1 = (RecyclerView)v.findViewById(R.id.round_item_recycler);
        recyclerView2 = (RecyclerView)v.findViewById(R.id.round_item_recycler2);

        rightGroupTitleTextView= (TextView)v.findViewById(R.id.title);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private ArrayAdapter<String> getAdapterForSpinner(ArrayList<RecyclerItem> recyclerItems,String obstacle, int type){
        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("none");
        for(RecyclerItem recyclerItem : recyclerItems){
            String testString = recyclerItem.getOfficialName();
            if (testString != null){
                if (obstacle.equals(testString)){
                    stringList.add(0,testString);
                }else
                stringList.add(testString);
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter;
        if (type ==1){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.red_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.red_spinner_dropdown);
        }else if (type ==2){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.blue_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.blue_spinner_dropdown);
        }else {
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, stringList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        return spinnerArrayAdapter;
    }

    private ArrayAdapter<String> getAdapterForSpinnerNoNull(ArrayList<RecyclerItem> recyclerItems,String selected,int type){
        ArrayList<String> stringList = new ArrayList<>();
        if (selected.equals("none")){
            stringList.add(0,selected);
        }
        for(RecyclerItem recyclerItem : recyclerItems){
            String testString = recyclerItem.getOfficialName();
            if (testString != null){
                if (selected.equals(recyclerItem.getOfficialName())){
                    stringList.add(0,recyclerItem.getOfficialName());
                }else
                    stringList.add(recyclerItem.getOfficialName());
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter;
        if (type ==1){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.red_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.red_spinner_dropdown);
        }else if (type ==2){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.blue_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.blue_spinner_dropdown);
        }else {
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, stringList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        return spinnerArrayAdapter;
    }

    private ArrayAdapter<String> getAdapterForSpinnerNoNull(ArrayList<RecyclerItem> recyclerItems,long selected){
        ArrayList<String> stringList = new ArrayList<>();
        for(RecyclerItem recyclerItem : recyclerItems){
            if (recyclerItem instanceof  RoundItem){
                RoundItem round =  (RoundItem)recyclerItem;
                /*
                if (round.getRoundTime() == selected){
                    stringList.add(0,Long.toString(round.getRoundTime()));
                }else
                    stringList.add(Long.toString(round.getRoundTime()));
                */
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, stringList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        return spinnerArrayAdapter;
    }

    private void bindData(){
        if (roundItem1 != null) {
            HashMap<String,RoundPartItem> roundParts = getRoundParts(roundItem1.get_id());
            adapterData1 = new ArrayList<>();
            roundItemAdapter1 = new RecyclerRoundPartAdapter(roundItem1.get_id());
            switch (roundItem1.getRound_type()){
                case 0:{
                    RoundPartItem cronoItem = roundParts.get("main_cronometer");
                    if (cronoItem != null){
                        adapterData1.add(cronoItem);
                    }

                    RoundPartItem phase1Item = roundParts.get("phase1");
                    if (phase1Item != null){
                        adapterData1.add(phase1Item);
                    }
                    RoundPartItem phase2Item = roundParts.get("phase2");
                    if (phase2Item != null){
                        adapterData1.add(phase2Item);
                    }
                    RoundPartItem phase3Item = roundParts.get("phase3");
                    if (phase3Item != null){
                        adapterData1.add(phase3Item);
                    }
                    RoundPartItem copterItem = roundParts.get("copter");
                    if (copterItem != null){
                        adapterData1.add(copterItem);
                    }
                    RoundPartItem catcherItem = roundParts.get("catcher");
                    if (catcherItem != null){
                        adapterData1.add(catcherItem);
                    }
                    RoundPartItem innovationItem = roundParts.get("innovation");
                    if (innovationItem != null){
                        adapterData1.add(innovationItem);
                    }


                    break;
                }
                case 1:{

                    RoundPartItem cronoItem = roundParts.get("main_cronometer");
                    if (cronoItem != null){
                        adapterData1.add(cronoItem);
                    }
                    RoundPartItem comunityScoreItem = roundParts.get("community_score");
                    if (comunityScoreItem != null){
                        adapterData1.add(comunityScoreItem);
                    }
                    break;
                }
                case 2:{
                    roundItemAdapter1.setIsRoundPuzzle(true);
                    RoundPartItem cronoItem = roundParts.get("main_cronometer");
                    if (cronoItem != null){
                        adapterData1.add(cronoItem);
                    }
                    RoundPartItem timeItem = roundParts.get("time");
                    if (timeItem != null){
                        adapterData1.add(timeItem);
                    }
                    RoundPartItem storyItem = roundParts.get("story");
                    if (storyItem != null){
                        adapterData1.add(storyItem);
                    }
                    RoundPartItem atmosphereItem = roundParts.get("atmosphere");
                    if (atmosphereItem != null){
                        adapterData1.add(atmosphereItem);
                    }
                    RoundPartItem puzzle_diversityItem = roundParts.get("puzzle_diversity");
                    if (puzzle_diversityItem != null){
                        adapterData1.add(puzzle_diversityItem);
                    }
                    RoundPartItem mechanism_diversityItem = roundParts.get("mechanism_diversity");
                    if (mechanism_diversityItem != null){
                        adapterData1.add(mechanism_diversityItem);
                    }
                    RoundPartItem cluesItem = roundParts.get("clues");
                    if (cluesItem != null){
                        adapterData1.add(cluesItem);
                    }
                    RoundPartItem learning_connectionItem = roundParts.get("learning_connection");
                    if (learning_connectionItem != null){
                        adapterData1.add(learning_connectionItem);
                    }

                    RoundPartItem effectItem = roundParts.get("effect");
                    if (effectItem != null){
                        adapterData1.add(effectItem);
                    }

                    RoundPartItem room_impressionItem = roundParts.get("room_impression");
                    if (room_impressionItem != null){
                        adapterData1.add(room_impressionItem);
                    }

                    RoundPartItem excelled_puzzleItem = roundParts.get("excelled_puzzle");
                    if (excelled_puzzleItem != null){
                        adapterData1.add(excelled_puzzleItem);
                    }


                    RoundPartItem innovationItem = roundParts.get("innovation");
                    if (innovationItem != null){
                        adapterData1.add(innovationItem);
                    }


                    RoundPartItem team_workItem = roundParts.get("team_work");
                    if (team_workItem != null){
                        adapterData1.add(team_workItem);
                        roundItemAdapter1.setTeamCrackerPart(team_workItem);
                    }

                    break;
                }
                case 3:{

                    RoundPartItem cronoItem = roundParts.get("main_cronometer");
                    if (cronoItem != null){
                        adapterData1.add(cronoItem);
                    }
                    RoundPartItem comunityScoreItem = roundParts.get("community_score");
                    if (comunityScoreItem != null){
                        adapterData1.add(comunityScoreItem);
                    }
                    RoundPartItem escapePackage = roundParts.get("escape_package");
                    if (escapePackage != null){
                        adapterData1.add(escapePackage);
                    }
                    break;
                }
            }


            roundItemAdapter1.setData(adapterData1);
            recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView1.setAdapter(roundItemAdapter1);
            recyclerView1.setItemViewCacheSize(20);
            rightGroupTitleTextView.setVisibility(View.VISIBLE);
            rightGroupTitleTextView.setText("קבוצה: " + getGroupNameByRoundId(roundItem1.get_id()) + " חדר " + getGroupNumByRoundId(roundItem1.get_id()));


        }
        //loadSecondView();
    }


    private String getGroupNameByRoundId(String round_id) {
        RoundItem ri = (RoundItem) Config.roundsMap.get(round_id);
        if (ri != null) {
            GroupItem gi = (GroupItem) Config.groupsMap.get(ri.getGroup());
            return gi.getGroupName();
        }
        return "";
    }


    private String getGroupNumByRoundId(String round_id) {
        RoundItem ri = (RoundItem) Config.roundsMap.get(round_id);
        if (ri != null) {
            GroupItem gi = (GroupItem) Config.groupsMap.get(ri.getGroup());
            return gi.getNumberAsString();
        }
        return "";
    }


    private HashMap<String,RoundPartItem> getRoundParts(String roundId){
        HashMap<String,RoundPartItem> result =  new HashMap<>();
        JSONObject value = null;
        try {
            value = new JSONObject().put("main_timer",roundItem1.getRound_time()).put("data_type","main_timer").put("progressed","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        result.put("main_cronometer",new RoundPartItem("0",roundItem1.get_id(),"main_cronometer", value ));
        Iterator<RecyclerItem> iterator = Config.roundPartsMap.values().iterator();
        while(iterator.hasNext()) {
            RoundPartItem roundPartItem = (RoundPartItem)iterator.next();
            if (roundPartItem.getRound_id().equals(roundId)){
                result.put(roundPartItem.getRound_part_type(),roundPartItem);
            }
        }
        return result;
    }




    private void initListeners(){

    }

    private void handleRequestFinishMatchRound() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Do you want the match to move to review?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestReviewRound();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder1.create();
        alert.show();
    }

    private void requestReviewRound() {
        JSONArray params = new JSONArray();
        params.put(roundItem1.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.REVIEW_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "review_round");
    }

    private void handleRequestResetRound() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Do you want to reset this round?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestResetRound();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder1.create();
        alert.show();
    }


    public void requestResetRound(){
        JSONArray params = new JSONArray();
        Log.d("RoundFragment", "requestResetRound: id:  "+ roundItem1.get_id());
        params.put(roundItem1.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.RESET_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                onBackPressed();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "reset_round");
    }




    private void handleRequestApproveRound() {

        JSONArray params = new JSONArray();
        params.put(roundItem1.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.APPROVE_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //roundItem.setIsDone(4);
                startButton.setCompleteText("Complete");
                startButton.setIdleText("Complete");
                startButton.setProgress(0);
                startButton.setProgress(100);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "approve_round");



    }

    private void handleRequestEditRound() {
        JSONArray params = new JSONArray();
        params.put(roundItem1.get_id());

        //params.put(date.getTime());

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                    onBackPressed();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "update_round_obstacles");



    }






    @Override
    public void onPause() {
        /*
        if (cronometer != null) {
            cronometer.setText("");
            //cronometer.setTextColor(ContextCompat.getColor(getContext(), R.color.colorRed));
        }
        */
        /*
        if (countDownTimer2 != null){
            countDownTimer2.cancel();
        }

        if (countDownTimer1 != null){
            countDownTimer1.cancel();
        }
        */

        super.onPause();
    }



    private void handleRequestStartRound() {

        JSONArray params = new JSONArray();

        params.put(roundItem1.get_id());


        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.START_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*
                if(countDownTimer2 != null) {
                    countDownTimer2.cancel();
                }
                startButton.setProgress(-1);
                */
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "start_round");
    }


    private void handleRequestUpdateGroupPoints(final int group,final int obs,final int sign) {
        // && checkLimits(group,obs,sign)


            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Updating Points...");
            pDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SERVER_URL + Config.ADD_POINTS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.equals("success")){
                        showTalkbackDurationTooltip ("Points added successfully.");
                    }else if (response.equals("failed")){
                        showTalkbackDurationTooltip ("This obstacle points at Maximum.");
                    }else if (response.equals("stateDone")){
                        showTalkbackDurationTooltip ("The round is already approved.");
                    }else{
                        showTalkbackDurationTooltip ("Round didn't start yet.");
                        if (Config.AdminStatus >0 && Config.AdminStatus<3){
                            if (!response.equals(roundItem1.get_id())){
                                FindRoundByIdAndShowPerma findRoundByIdAndShowPerma = new FindRoundByIdAndShowPerma(response);
                                EventBus.getDefault().post(findRoundByIdAndShowPerma);
                            }
                        }
                    }

                    pDialog.cancel();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                    Context con = getContext();
                    if (con != null){
                        if (toastObject != null) {
                            toastObject.cancel();
                        }
                        toastObject = Toast.makeText(con,"Adding Points Failed!",Toast.LENGTH_LONG);
                        toastObject.show();
                    }
                    con = null;
                    pDialog.cancel();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    if (group == 1) {

                        params.put("groupName", roundItem1.getGroup());

                    }
                    params.put("roundId", roundItem1.get_id());
                    return params;
                }
            };
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_points");

    }

    private void handleRequestDeleteRound() {

    }


    private void handleRequestSaveRound() {


        JSONArray params = new JSONArray();

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.ADD_NEW_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_new_round");

    }

    void onBackPressed(){

        if (roundItem1.getRound_type() == 0 && Config.isUserFieldReferee() && !isRound1Done()){
            return;
        }
        if (roundItem1.getRound_type() == 1 && Config.isUserCommunityReferee() && !isRound1Done()){
            return;
        }
        if (roundItem2.getRound_type() == 0 && Config.isUserFieldReferee() && !isRound2Done()){
            return;
        }
        if (roundItem2.getRound_type() == 1 && Config.isUserCommunityReferee() && !isRound2Done()){
            return;
        }
        FragmentManager frmgr = getFragmentManager();
        if (frmgr != null) {

            if (((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1 != null){
                ((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1.cancel();
                ((RecyclerRoundPartAdapter)(recyclerView1.getAdapter())).mainCountdownTimer1 = null;
            }
            if (((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1 != null){
                ((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1.cancel();
                ((RecyclerRoundPartAdapter)(recyclerView2.getAdapter())).mainCountdownTimer1 = null;
            }
            frmgr.popBackStack();
        }
    }

    public boolean isRound1Done(){
        ArrayList<RoundPartItem> roundPartItems = roundItemAdapter1.getData();
        boolean isAllDone = true;
        for (int i = 0; i < roundPartItems.size(); i++) {
            RoundPartItem roundPartItem = roundPartItems.get(i);
            if (!roundPartItem.isProgressed()){
                isAllDone = false;
                break;
            }
        }
        return isAllDone;
    }
    public boolean isRound2Done(){
        ArrayList<RoundPartItem> roundPartItems = roundItemAdapter2.getData();
        boolean isAllDone = true;
        for (int i = 0; i < roundPartItems.size(); i++) {
            RoundPartItem roundPartItem = roundPartItems.get(i);
            if (!roundPartItem.isProgressed()){
                isAllDone = false;
                break;
            }
        }
        return isAllDone;
    }

    class WizardPagerAdapter extends PagerAdapter {

        public Object instantiateItem(View collection, int position) {

            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.page_one;
                    break;
                case 1:
                    resId = R.id.page_two;
                    break;
            }
            return root.findViewById(resId);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }
    }


}
