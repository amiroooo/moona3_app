package com.example.amir.moona3;

/**
 * Created by Amir on 1/15/2017.
 */
public class ItemToDelete {
    public String getDocumentId() {
        return documentId;
    }

    private String documentId;
    private String collectionName;

    public ItemToDelete(String collectionName,String documentID) {
        this.collectionName = collectionName;
        this.documentId = documentID;
    }

    public String getCollectionName() {
        return collectionName;
    }
}
