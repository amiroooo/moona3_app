package com.example.amir.moona3;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Amir on 3/23/2018.
 */

public class Game1RoundItem extends RoundPartItem {
    private Integer phaseNum;
    final private static Integer MAX_IN_BASKET = 25;
    public Game1RoundItem(RoundPartItem other) {
        super(other);
    }

    public Integer getCurrentBallsInsideBucket() {
        JSONObject j = getValue();
        try {
            Integer entered = j.getInt("entered");
            return entered;
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

}
