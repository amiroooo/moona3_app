package com.example.amir.moona3;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

/**
 * Created by Amir on 3/26/2018.
 */

public class ImprovedSpinner extends android.support.v7.widget.AppCompatSpinner {
    public ImprovedSpinner(Context context) {
        super(context);
    }

    public ImprovedSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImprovedSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setSelection(int position, boolean animate, boolean ceaseFireOnItemClickEvent) {
        OnItemSelectedListener l = getOnItemSelectedListener();
        if (ceaseFireOnItemClickEvent) {
            setOnItemSelectedListener(null);
        }

        super.setSelection(position, animate);

        if (ceaseFireOnItemClickEvent) {
            setOnItemSelectedListener(l);
        }
    }

}