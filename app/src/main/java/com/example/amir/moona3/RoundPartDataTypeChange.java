package com.example.amir.moona3;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Amir on 3/24/2018.
 */

public class RoundPartDataTypeChange {
    private String _id;
    private JSONObject updatedJson;

    public RoundPartDataTypeChange(String _id, JSONObject updatedJson) {
        this._id = _id;
        this.updatedJson = updatedJson;
    }

    public String get_id() {
        return _id;
    }

    public boolean isDataRequireRefresh(){
        boolean isAnyItemRequireUpdate = true;
        Iterator<String> iter = updatedJson.keys();
        updatedJson.length();
        while ( iter.hasNext() ) {
            String key = iter.next();
            if (key.equals("paused")){
                isAnyItemRequireUpdate = false;
            }
            if (key.equals("requiresRefresh")){
                try {
                    boolean requiredRefresh = updatedJson.getBoolean("requiresRefresh");
                    if (!requiredRefresh){
                        isAnyItemRequireUpdate = false;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return isAnyItemRequireUpdate;
    }
}
