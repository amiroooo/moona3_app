package com.example.amir.moona3;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by Amir on 12/31/2016.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    private final AdapterClickDelegate adapterClickDelegate;

    public ArrayList<RecyclerItem> getDataArray() {
        return dataArray;
    }

    private ArrayList<RecyclerItem> dataArray;
    private HashMap<String, RecyclerItem> totalRoundArray;
    private int type;
    private Handler handler;
    private HashMap<String, Double> pointsList;
    private HashMap<String, String> descriptionList;



    public RecyclerAdapter(AdapterClickDelegate adapterClickDelegate, int type) {
        this.adapterClickDelegate = adapterClickDelegate;
        this.dataArray = new ArrayList<>();
        this.totalRoundArray = new HashMap<>();
        this.type = type;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notifyChangeInRoundItem(UpdateRoundDelegate event) {
        if (type == 1 || type == 3) {
            RoundItem roundItem = (RoundItem) totalRoundArray.get(event.getDocumentID());
            if (roundItem != null) {
                roundItem.updateByJson(event.getUpdatedValuesJson());
            }
            executeRefreshAdapterData();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notifyChangeInGroupItem(UpdateGroupDelegate event) {
        for (int i = 0; i < dataArray.size(); i++) {
            if (event.get_id().equals(dataArray.get(i).get_id())) {
                executeRefreshAdapterData();
                break;
            }
        }
        executeRefreshAdapterData();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemDeletion(ItemToDelete event) {
        if (type == 0) {
            if (event.getCollectionName().equals("groups")) {
                updateHashFromBackup();
                setDataFromHashMap();
            }
        }
        if (type == 1) {
            if (event.getCollectionName().equals("rounds")) {
                updateHashFromBackup();
                setDataFromHashMap();
            }
            if (event.getCollectionName().equals("groups")) {
                updateHashFromBackup();
                setDataFromHashMap();
            }
        }
        if (type == 3) {
            for (int i = 0; i < dataArray.size(); i++) {
                RecyclerItem ri = dataArray.get(i);
                if (ri.get_id().equals(event.getDocumentId())) {
                    dataArray.remove(i);
                    notifyDataSetChanged();
                }
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWrongRequestUpdatePoints(FindRoundByIdAndShowPerma event) {
        if (type == 1) {
            RoundItem roundItem = (RoundItem) totalRoundArray.get(event.get_id());
            if (roundItem != null) {
                EventBus.getDefault().post(new PermaRoundFragmentDelegate(roundItem));
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public synchronized void NotifyChandeDataOnRounds(GameStateChangeDelegate event) {
        executeRefreshAdapterData();
    }


    public void executeRefreshAdapterData() {
        if (handler == null) {
            handler = new Handler();
        }
        handler.removeCallbacksAndMessages(null);
        Runnable myRunnable = new Runnable() {
            public void run() {
                notifyDataSetChanged();
            }
        };
        handler.postDelayed(myRunnable, 1000);
    }


    public void updateHashFromBackup() {
        if (type == 0) {
            totalRoundArray = Config.groupsMap;
        }
        if (type == 1) {
            totalRoundArray = Config.roundsMap;
        }
    }


    public void setDataFromHashMap() {
        dataArray = new ArrayList<>();
        dataArray.addAll(totalRoundArray.values());
        Collections.sort(dataArray);
        executeRefreshAdapterData();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addGroup(GroupItem event) {
        if (type == 0) {
            this.totalRoundArray = Config.groupsMap;
            totalRoundArray.keySet().iterator();

            Log.d("RecyclerAdapter", "addGroup: " + totalRoundArray.size());
            setDataFromHashMap();
        }
    }

    public String recognitionId;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public synchronized void addRound(RoundItem event) {
        if (type == 1) {
            this.totalRoundArray = Config.roundsMap;
            setDataFromHashMap();
            /*
            if (this.totalRoundArray.containsKey(event.get_id())){

            }else {
                this.totalRoundArray.put(event.get_id(), event);

            }
            */
        }

        if (type == 3 && adapterClickDelegate != null && recognitionId != null && event.getGroup().equals(recognitionId)) {
            ((FilteredRoundsFragment) adapterClickDelegate).setFilteredData();
            notifyDataSetChanged();
        }
    }


    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (!EventBus.getDefault().isRegistered(holder)) {
            EventBus.getDefault().register(holder);
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (EventBus.getDefault().isRegistered(holder)) {
            EventBus.getDefault().unregister(holder);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (type == 0) {
            View rowView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_row, parent, false);
            return new GroupsViewHolder(rowView);
        } else if (type == 1 || type == 3) {
            View rowView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rounds_row, parent, false);
            return new RoundsViewHolder(rowView);
        } else {
            if (viewType == 0) {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.group_row, parent, false);
                return new GroupsViewHolder(rowView);
            } else {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.group_listing_row, parent, false);
                return new GroupsListingViewHolder(rowView);
            }

        }
    }

    @Override
    public int getItemViewType(int position) {
        RecyclerItem recyclerItem = dataArray.get(position);
        if (recyclerItem.isListing()) {
            return 1;
        } else
            return 0;

    }


    public void editGroup(String id){
        CreateNewGroup t = new CreateNewGroup((Context) ((MainActivity.PlaceholderFragment)adapterClickDelegate).fragmentCalls,id);
        t.show();
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.set_id(dataArray.get(position).get_id());
        if (type == 0) {
            final GroupItem groupItem = (GroupItem) dataArray.get(position);
            if (holder instanceof GroupsViewHolder) {
                GroupsViewHolder groupsViewHolder = (GroupsViewHolder) holder;
                holder.setTeamTextView(groupItem.getGroupName());
                groupsViewHolder.setGroupNumber(groupItem.getNumberAsString());
                //holder.setRoundPointsTextView(Integer.toString(groupItem.calculateGroupPoints()));
                groupsViewHolder.getTextView3().setText(groupItem.getDescription());
                groupsViewHolder.setRoundPointsTextView(((Double) Math.ceil(groupItem.getPoints() + groupItem.getAdminExtra())).toString());
                holder.getCardView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (adapterClickDelegate != null) {
                            adapterClickDelegate.onCardViewClick(type, groupItem);
                        }
                    }
                });
            }

            holder.setStateAppearence(0);

            if (holder.cardView != null) {
                holder.getCardView().setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if ((Config.AdminStatus == 0) && adapterClickDelegate != null) {
                            editGroup(groupItem.get_id());
                        }
                        return true;
                    }
                });
            }
        } else if (type == 1 || type == 3) {
            String roundItemId = dataArray.get(position).get_id();
            RoundItem roundItem = (RoundItem) Config.roundsMap.get(roundItemId);
            RoundsViewHolder roundsViewHolder = (RoundsViewHolder) holder;

            if (roundItem == null) {
                executeRefreshAdapterData();
                return;
            }
            holder.setIsRecyclable(false);

            GroupItem groupItem = (GroupItem) Config.groupsMap.get(roundItem.getGroup());
            if (groupItem != null) {
                roundsViewHolder.setTeamTextView(groupItem.getGroupName());
                roundsViewHolder.setRoundPointsTextView(roundItem.calculatePoints());
                roundsViewHolder.setRoundTypeTextView(roundItem.getGameTypeName());
                if (roundItem.getRound_type() == 0) {
                    roundsViewHolder.setRoundOrderTextView("סיבוב " + roundItem.getRound_order().toString());
                } else if (roundItem.getRound_type() == 1) {
                    roundsViewHolder.setRoundOrderTextView("חברתי");
                } else if (roundItem.getRound_type() == 2) {
                    roundsViewHolder.setRoundOrderTextView("סיבוב " + roundItem.getRound_order().toString());
                } else if (roundItem.getRound_type() == 3) {
                    roundsViewHolder.setRoundOrderTextView("חברתי");
                }
                //holder.setStateAppearence(roundItem.getIsDone());

                if (roundItem.getProgress() == 1) {
                    holder.setStateAppearence(1);
                } else if (roundItem.getProgress() == 2) {
                    holder.setStateAppearence(10);
                } else
                    holder.setStateAppearence(0);

                final RoundItem finalRoundItem = roundItem;
                holder.getCardView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (adapterClickDelegate != null) {
                            adapterClickDelegate.onCardViewClick(type, finalRoundItem);
                        }
                    }
                });
                if (type == 3) {
                    holder.getCardView().setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            if (adapterClickDelegate != null) {
                                adapterClickDelegate.onCardViewLongClick(type, finalRoundItem.get_id());
                            }
                            return true;
                        }
                    });
                }
            } else {
                roundsViewHolder.setTeamTextView("group is UNKNOWN!");
            }
        } else if (type == 2) {

            final GroupItem groupItem = (GroupItem) dataArray.get(position);
            if (holder instanceof GroupsViewHolder) {
                GroupsViewHolder groupsViewHolder = (GroupsViewHolder) holder;
                holder.setTeamTextView(groupItem.getGroupName());
                groupsViewHolder.setGroupNumber(groupItem.getNumberAsString());
                if (pointsList.containsKey(groupItem.get_id())) {
                    String s = pointsList.get(groupItem.get_id()).toString();
                    String upToNCharacters = s.substring(0, Math.min(s.length(), 5));
                    holder.setRoundPointsTextView(upToNCharacters);
                }
                if (winnerPos == 3){
                    String str = descriptionList.get(groupItem.get_id());
                    if (str != null) {
                        if (str.length() > 2) {
                            str = str.substring(0, str.length() - 2);
                        }
                        groupsViewHolder.getTextView3().setText(str);
                    }else{
                        groupsViewHolder.getTextView3().setText("");
                    }
                }else{
                    groupsViewHolder.getTextView3().setText("");
                }


            }
        }

    }

    private ArrayList<RoundPartItem> getGroupWorksCrackerPartsRoundId(String round_id) {
        RoundItem ri = (RoundItem) Config.roundsMap.get(round_id);
        ArrayList<RecyclerItem> roundsList = new ArrayList<>();
        roundsList.addAll(Config.roundsMap.values());
        ArrayList<RoundPartItem> roundPartsList = new ArrayList<>();
        for (int j = 0; j < roundsList.size(); j++) {
                RoundItem roundItem = (RoundItem) roundsList.get(j);
                if (roundItem.getGroup().equals(ri.getGroup())) {
                    ArrayList<RecyclerItem> tempRoundPartsList = new ArrayList<>();
                    tempRoundPartsList.addAll(Config.roundPartsMap.values());
                    for (int i = 0; i < tempRoundPartsList.size(); i++) {
                        RoundPartItem roundPartItem = (RoundPartItem) tempRoundPartsList.get(i);
                        if (roundPartItem.getRound_id().equals(roundItem.get_id()) && roundPartItem.getPartDataType().equals("time")) {
                            roundPartsList.add(roundPartItem);
                        }
                    }
                }
        }
        return roundPartsList;

    }

    private RoundPartItem getGroupHetrashmutPartsRoundId(String round_id) {
        ArrayList<RecyclerItem> tempRoundPartsList = new ArrayList<>();
        tempRoundPartsList.addAll(Config.roundPartsMap.values());
        for (int i = 0; i < tempRoundPartsList.size(); i++) {
            RoundPartItem roundPartItem = (RoundPartItem) tempRoundPartsList.get(i);
            if (roundPartItem.getRound_id().equals(round_id) && roundPartItem.getPartDataType().equals("room_impression")) {
                return roundPartItem;
            }
        }
        return null;

    }

    @Override
    public int getItemCount() {
        if (dataArray == null) return 0;
        return dataArray.size();
    }

    public void setRoundStart(String id, int time) {
        for (RecyclerItem recyclerItem : dataArray) {
            if (recyclerItem instanceof RoundItem) {
                RoundItem roundItem = (RoundItem) recyclerItem;
                if ((roundItem.get_id()).equals(id)) {
                    //roundItem.setIsDone(2);
                    //roundItem.setMatchEnd(time);
                }
            }
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int position) {
        this.dataArray.remove(position);

        //mRecyclerView.removeViewAt(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    public void buildFirstEditBoxItem() {
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        if (dataArray.size() == 0) {
            dataArray.add(recyclerItem);
        } else {
            dataArray.set(0, recyclerItem);
        }


        executeRefreshAdapterData();
    }

    private int chosenWinner = 0;

    private int currentTab = 0;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMoveToTab2(OnMoveToTab event) {
        currentTab = event.getTab();
        if (currentTab == 2 && type == 2) {
            getWinners(chosenWinner);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSomePointUpdated(UpdatePointsForGroupIdEvent event) {
        if (currentTab == 2 && type == 2) {
            getWinners(chosenWinner);
        }
    }

    public void setDataByArrayList(ArrayList<RecyclerItem> dataByArrayList) {
        Collections.sort(dataByArrayList);
        this.dataArray = dataByArrayList;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public void setRoundPointsTextView(String roundPointsTextView) {
            RoundPointsTextView.setText(roundPointsTextView);
        }

        public void setTeamTextView(String teamTextView) {
            TeamTextView.setText(teamTextView);
        }

        public CardView getCardView() {
            return cardView;
        }

        protected AutofitTextView TeamTextView;
        protected AutofitTextView RoundPointsTextView;
        protected CardView cardView;

        public String get_id() {
            return _id;
        }

        protected String _id;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void setStateAppearence(int i) {
            if (cardView == null) {
                return;
            }
            if (i == 4) {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimaryDone));
            } else if (i == 3 || i == 2) {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimaryOngoing));
            } else if (i == 1) {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimaryReady));
            } else if (i == 10) {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPassGroups));
            } else {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary));
            }
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void setStatus(RoundStatusEvent event) {
            if ((event.get_id()).equals(this._id)) {
                setStateAppearence(event.getStatus());
                RoundItem roundItem = (RoundItem) totalRoundArray.get(event.get_id());
                if (roundItem != null) {
                    //roundItem.setIsDone(event.getStatus());

                    if (event.getStatus() > 1 && event.getStatus() < 4) {
                        Log.d("Adapter", "setStatus: trying to open frag");
                        PermaRoundFragmentDelegate t = new PermaRoundFragmentDelegate(roundItem);
                        EventBus.getDefault().post(t);
                    }

                }
            }
        }

        public void set_id(String id) {
            this._id = id;
        }
    }


    class GroupsViewHolder extends ViewHolder {
        private final TextView groupNumberText;

        public TextView getTextView3() {
            return TextView3;
        }

        private final TextView TextView3;

        public void setRoundPointsTextView(String roundPointsTextView) {
            RoundPointsTextView.setText(roundPointsTextView);
        }

        public void setGroupNumber(String roundPointsTextView) {
            groupNumberText.setText(roundPointsTextView);
        }

        public void setTeamTextView(String teamTextView) {
            TeamTextView.setText(teamTextView);
        }

        public GroupsViewHolder(View itemView) {
            super(itemView);
            TeamTextView = (AutofitTextView) itemView.findViewById(R.id.group_name);
            RoundPointsTextView = (AutofitTextView) itemView.findViewById(R.id.group_points);
            cardView = (CardView) itemView.findViewById(R.id.card_view1);
            TextView3 = (TextView) itemView.findViewById(R.id.group_description);
            groupNumberText = (TextView) itemView.findViewById(R.id.number);


        }


    }

    class RoundsViewHolder extends ViewHolder {
        private final TextView RoundOrderTextView;
        private final TextView RoundTypeTextView;

        public void setRoundPointsTextView(String roundPointsTextView) {
            RoundPointsTextView.setText(roundPointsTextView);
        }

        public void setRoundOrderTextView(String text) {
            RoundOrderTextView.setText(text);
        }

        public void setRoundTypeTextView(String text) {
            RoundTypeTextView.setText(text);
        }

        public void setTeamTextView(String teamTextView) {
            TeamTextView.setText(teamTextView);
        }

        public RoundsViewHolder(View itemView) {
            super(itemView);
            TeamTextView = (AutofitTextView) itemView.findViewById(R.id.group_1);
            RoundPointsTextView = (AutofitTextView) itemView.findViewById(R.id.group_2);
            cardView = (CardView) itemView.findViewById(R.id.card_view2);
            RoundOrderTextView = (TextView) itemView.findViewById(R.id.round_order);
            RoundTypeTextView = (TextView) itemView.findViewById(R.id.round_type);

        }


        public TextView getRoundOrderTextView() {
            return RoundOrderTextView;
        }

        public TextView getRoundTypeTextView() {
            return RoundTypeTextView;
        }


        @Subscribe(threadMode = ThreadMode.MAIN)
        public void setPointsByEventBus(RoundNeedsUpdatePoints event) {
            if ((event.getRound_id()).equals(get_id())) {
                if (event.getRoundItem() == null) {
                    setRoundPointsTextView("0");
                } else
                    setRoundPointsTextView(event.getRoundItem().calculatePoints());
            }
        }


    }

    class ObstaclesViewHolder extends ViewHolder {
        private final TextView nameView;

        public TextView getNameView() {
            return nameView;
        }

        public TextView getDescriptionView() {
            return descriptionView;
        }

        public TextView getOwnerView() {
            return ownerView;
        }

        public TextView getMax_pointsView() {
            return max_pointsView;
        }

        public TextView getTickView() {
            return tickView;
        }

        private final TextView descriptionView;
        private final AutofitTextView ownerView;
        private final TextView max_pointsView;
        private final TextView tickView;

        public ObstaclesViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view3);
            nameView = (TextView) itemView.findViewById(R.id.name);
            descriptionView = (TextView) itemView.findViewById(R.id.description);
            ownerView = (AutofitTextView) itemView.findViewById(R.id.owner);
            max_pointsView = (TextView) itemView.findViewById(R.id.max_points);
            tickView = (TextView) itemView.findViewById(R.id.tick);
        }

    }

    private Integer  winnerPos = -1;
    public void getWinners(int position) {
        winnerPos = position;
        switch (position) {
            case 0: { // generic
                listGenericTotalWinnersList();
                break;
            }
            case 1: { //community
                listGroupsByBestCommunity();
                break;
            }
            case 2: { // escape package
                listGroupsByBestEscapePackage();
                break;
            }

            case 3: { // copter
                listGroupsByBestPuzzle();
                break;
            }

            case 4: { // innovation
                listGroupsByBestInnovation();
                break;
            }
            case 5: {
                listGroupsByBestCracker();
                break;
            }
            case 6: {
                listGroupsByBestRounds();
                break;
            }
        }
    }

    class GroupsListingViewHolder extends ViewHolder {

        Spinner winnerList;

        public GroupsListingViewHolder(View itemView) {
            super(itemView);
            winnerList = itemView.findViewById(R.id.winner_spinner);
            winnerList.setAdapter(getAdapterForSpinner(Config.groupsMap));
            winnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    chosenWinner = i;
                    getWinners(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }

        private ArrayAdapter<String> getAdapterForSpinner(HashMap<String, RecyclerItem> recyclerItems) {

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(itemView.getContext(), R.layout.spinner_item, new ArrayList<String>(Arrays.asList(itemView.getResources().getStringArray(R.array.winners_list))));


            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown); // The drop down view
            return spinnerArrayAdapter;
        }


    }


    private ArrayList<ArrayList<RoundPartItem>> getRoundPartsForByGroupId(String id) {
        ArrayList<RecyclerItem> roundsList = new ArrayList<>();
        roundsList.addAll(Config.roundsMap.values());
        ArrayList<ArrayList<RoundPartItem>> roundsWithPartsList = new ArrayList<>();
        for (int j = 0; j < roundsList.size(); j++) {
            RoundItem roundItem = (RoundItem) roundsList.get(j);
            if (roundItem.getGroup().equals(id)) {
                ArrayList<RecyclerItem> roundPartsList = new ArrayList<>();
                roundPartsList.addAll(Config.roundPartsMap.values());
                ArrayList<RoundPartItem> chosenRoundPartsList = new ArrayList<>();
                for (int i = 0; i < roundPartsList.size(); i++) {
                    RoundPartItem roundPartItem = (RoundPartItem) roundPartsList.get(i);
                    if (roundPartItem.getRound_id().equals(roundItem.get_id())) {
                        chosenRoundPartsList.add(roundPartItem);
                    }

                }
                roundsWithPartsList.add(chosenRoundPartsList);
            }
        }
        return roundsWithPartsList;
    }

    private static ArrayList<RoundPartItem> getAllRoundPartsForByGroupId(String id) {
        ArrayList<RecyclerItem> roundsList = new ArrayList<>();
        roundsList.addAll(Config.roundsMap.values());
        ArrayList<RoundPartItem> roundsWithPartsList = new ArrayList<>();
        for (int j = 0; j < roundsList.size(); j++) {
            RoundItem roundItem = (RoundItem) roundsList.get(j);
            if (roundItem.getGroup().equals(id)) {
                ArrayList<RecyclerItem> roundPartsList = new ArrayList<>();
                roundPartsList.addAll(Config.roundPartsMap.values());
                ArrayList<RoundPartItem> chosenRoundPartsList = new ArrayList<>();
                for (int i = 0; i < roundPartsList.size(); i++) {
                    RoundPartItem roundPartItem = (RoundPartItem) roundPartsList.get(i);
                    if (roundPartItem.getRound_id().equals(roundItem.get_id())) {
                        roundsWithPartsList.add(roundPartItem);
                    }

                }
            }
        }
        return roundsWithPartsList;
    }


    private ArrayList<RoundPartItem> getRoundPartsByDataType(ArrayList<RoundPartItem> roundPartList, String dataType) {
        ArrayList<RoundPartItem> filteredRoundPartsList = new ArrayList<>();
        for (int i = 0; i < roundPartList.size(); i++) {
            RoundPartItem roundPartItem = roundPartList.get(i);
            if (roundPartItem.getPartDataType().equals(dataType)) {
                filteredRoundPartsList.add(roundPartItem);
            }

        }
        return filteredRoundPartsList;
    }

    private static boolean isRoundAtOrder(String roundId, int round_order) {
        RecyclerItem recyler = Config.roundsMap.get(roundId);
        if (recyler != null) {
            RoundItem temp = (RoundItem) recyler;
            if (temp.getRound_order() == round_order) {
                return true;
            }
        }
        return false;
    }

    public static Double getGroupPlaceInVoltageDevision(String groupId, int round_order) {
        ArrayList<RecyclerItem> roundPartsList = new ArrayList<>();
        ArrayList<RoundPartItem> game3List = new ArrayList<>();
        roundPartsList.addAll(Config.roundPartsMap.values());
        for (RecyclerItem element : roundPartsList) {
            RoundPartItem roundPartItem = (RoundPartItem) element;
            if (roundPartItem.getPartDataType().equals("game3") && isRoundAtOrder(roundPartItem.getRound_id(), round_order)) {
                game3List.add(roundPartItem);
            }
        }
        Collections.sort(game3List, new Comparator<RoundPartItem>() {
            @Override
            public int compare(RoundPartItem roundPartItem1, RoundPartItem roundPartItem2) {
                Double voltage1 = (double) 0;
                Double voltage2 = (double) 0;
                if (roundPartItem1.getValue().has("volts")) {
                    try {
                        voltage1 = roundPartItem1.getValue().getDouble("volts");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (roundPartItem1.getValue().has("volts")) {
                    try {
                        voltage2 = roundPartItem2.getValue().getDouble("volts");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (voltage1 > voltage2) {
                    return -1;
                }
                if (voltage1 < voltage2) {
                    return 1;
                }
                return 0;
            }
        });

        ArrayList<RoundPartItem> roundPartItemList = getAllRoundPartsForByGroupId(groupId);
        ArrayList<String> roundPartItemIdList = new ArrayList<>();
        for (RoundPartItem element : roundPartItemList) {
            String id = element.get_id();
            roundPartItemIdList.add(id);
        }


        for (int i = 0; i < game3List.size(); i++) {
            RoundPartItem element = game3List.get(i);
            String id = element.get_id();
            if (roundPartItemIdList.contains(id)) {
                if (game3List.size() == 0 || (double) game3List.size() - (double) i == 0) {
                    return 0.0;
                } else
                    return ((double) game3List.size() - (double) i) / ((double) game3List.size());
            }
        }
        return 0.0;
    }

    public static Double getGame2RoundPartScoreRatio(JSONObject value) {
        Double totalValue = 0.0;
        if (value.has("big_ball")) {
            try {
                totalValue += value.getInt("big_ball") * 2;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (value.has("big_pyramid")) {
            try {
                totalValue += value.getInt("big_pyramid") * 4;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (value.has("big_cube")) {
            try {
                totalValue += value.getInt("big_cube") * 5;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (value.has("medium_ball")) {
            try {
                totalValue += value.getInt("medium_ball");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (value.has("medium_pyramid")) {
            try {
                totalValue += value.getInt("medium_pyramid") * 3;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (value.has("medium_cube")) {
            try {
                totalValue += value.getInt("medium_cube") * 2;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (value.has("small_ball")) {
            try {
                totalValue += value.getInt("small_ball") * 3;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (value.has("small_pyramid")) {
            try {
                totalValue += value.getInt("small_pyramid") * 5;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (value.has("small_cube")) {
            try {
                totalValue += value.getInt("small_cube") * 4;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        double reqValue = 0.0;
        if (value.has("req_points")) {
            try {
                reqValue = value.getInt("req_points");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (totalValue > reqValue) {
            totalValue = reqValue - (totalValue - reqValue);
        }
        if (reqValue == 0.0) {
            return reqValue;
        }
        return totalValue / reqValue;
    }

    private ArrayList<ArrayList<RoundPartItem>> filterRoundListToKeepOnlyDataType(ArrayList<ArrayList<RoundPartItem>> unfilteredList, String gameType) {
        ArrayList<ArrayList<RoundPartItem>> filteredList = new ArrayList<>();
        for (ArrayList<RoundPartItem> round : unfilteredList) {
            ArrayList<RoundPartItem> filteredRoundPartsList = new ArrayList<>();
            for (int i = 0; i < round.size(); i++) {
                if (round.get(i).getPartDataType().equals(gameType)) {
                    filteredRoundPartsList.add(round.get(i));
                }
            }
            filteredList.add(filteredRoundPartsList);
        }

        return filteredList;
    }


    private Double getSingleGameAverageRatio(String groupId, String dataType) {
        GroupItem groupItem = (GroupItem) Config.groupsMap.get(groupId);
        if (groupItem == null) {
            return -1.0;
        }

        ArrayList<ArrayList<RoundPartItem>> roundsAndPartsList = getRoundPartsForByGroupId(groupItem.get_id());
        ArrayList<ArrayList<RoundPartItem>> filteredGameRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, dataType);
        ArrayList<Double> gameRatioList = new ArrayList<>();
        int roundsWithTheGame = 0;
        switch (dataType) {
            case "game1": {

                for (int i = 0; i < filteredGameRoundList.size(); i++) {
                    ArrayList<RoundPartItem> element = filteredGameRoundList.get(i);
                    roundsWithTheGame += element.size();
                    Integer scoreHelper = 0;
                    Integer counterHelper = 0;
                    Integer bonusPoints = 0;
                    for (RoundPartItem element2 : element) {
                        if (element2.getValue().has("entered")) {
                            try {
                                if (element2.getValue().getInt("entered") > scoreHelper) {
                                    scoreHelper = element2.getValue().getInt("entered");
                                    if (scoreHelper == 7) {
                                        if (element2.getValue().has("last_update_time_remain")) {
                                            try {
                                                int remainingTime = element2.getValue().getInt("last_update_time_remain");
                                                remainingTime = remainingTime / 1000;
                                                if (remainingTime > 40) {
                                                    remainingTime = 40;
                                                }
                                                bonusPoints = (int) (0.5 * remainingTime);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                }
                                counterHelper += 1;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (counterHelper > 1) {
                        if (scoreHelper == 0) {
                            gameRatioList.add(0.0);
                        } else
                            gameRatioList.add(((double) scoreHelper * 30 + bonusPoints + (roundsWithTheGame * 0.01)) / 210);
                    } else if (counterHelper == 1) {
                        if (scoreHelper == 0) {
                            gameRatioList.add(0.0);
                        } else
                            gameRatioList.add(((double) scoreHelper * 20 + bonusPoints + (roundsWithTheGame * 0.01)) / 140);
                    }
                }
                break;
            }
            case "game2": {
                for (int i = 0; i < filteredGameRoundList.size(); i++) {
                    ArrayList<RoundPartItem> element = filteredGameRoundList.get(i);
                    Double scoreHelper = 0.0;
                    roundsWithTheGame += element.size();
                    Integer counterHelper = 0;
                    for (RoundPartItem element2 : element) {

                        double score2test = getGame2RoundPartScoreRatio(element2.getValue());
                        if (score2test > scoreHelper) {
                            scoreHelper = score2test;
                        }
                        counterHelper += 1;
                    }
                    if (counterHelper > 1) {
                        double scoreRatio = scoreHelper * 210;
                        if (scoreRatio > 210) {
                            scoreRatio = 210;
                        }
                        if (scoreRatio == 0) {
                            gameRatioList.add(0.0);
                        } else
                            gameRatioList.add((scoreRatio + (roundsWithTheGame * 0.01)) / 210);
                    } else if (counterHelper == 1) {
                        double scoreRatio = scoreHelper * 140;
                        if (scoreRatio > 140) {
                            scoreRatio = 140;
                        }
                        if (scoreRatio == 0) {
                            gameRatioList.add(0.0);
                        } else
                            gameRatioList.add((scoreRatio + (roundsWithTheGame * 0.01)) / 140);
                    }
                }
            }
            case "game3": {
                for (int i = 0; i < filteredGameRoundList.size(); i++) {
                    ArrayList<RoundPartItem> element = filteredGameRoundList.get(i);
                    Double scoreHelper = 0.0;
                    boolean counterHelper = false;

                    for (RoundPartItem element2 : element) {
                        counterHelper = false;
                        if (element2.getValue().has("volts")) {
                            scoreHelper = getGroupPlaceInVoltageDevision(groupItem.get_id(), element2.getRoundOrder());
                            counterHelper = true;
                        }
                    }
                    if (counterHelper) {
                        gameRatioList.add(scoreHelper);
                    }
                }
            }
        }
        Double score = 0.0;
        for (int i = 0; i < gameRatioList.size(); i++) {
            score += gameRatioList.get(i);
        }
        if (gameRatioList.size() > 0 && score > 0)
            score = score / gameRatioList.size();
        return score;
    }


    private Double getGameAverageRatio(String groupId) {
        GroupItem groupItem = (GroupItem) Config.groupsMap.get(groupId);
        if (groupItem == null) {
            return -1.0;
        }

        ArrayList<ArrayList<RoundPartItem>> roundsAndPartsList = getRoundPartsForByGroupId(groupItem.get_id());
        ArrayList<ArrayList<RoundPartItem>> filteredTimeRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "time");
        ArrayList<ArrayList<RoundPartItem>> filteredStoryRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "story");
        ArrayList<ArrayList<RoundPartItem>> filteredAtmosphereRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "atmosphere");
        ArrayList<ArrayList<RoundPartItem>> filteredPuzzleDiversityRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "puzzle_diversity");
        ArrayList<ArrayList<RoundPartItem>> filteredMechanismDiversityRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "mechanism_diversity");
        ArrayList<ArrayList<RoundPartItem>> filteredCluesRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "clues");
        ArrayList<ArrayList<RoundPartItem>> filteredLearningConnectionRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "learning_connection");
        ArrayList<ArrayList<RoundPartItem>> filteredEffectRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "effect");
        ArrayList<ArrayList<RoundPartItem>> filteredRoomImpressionRoundList = filterRoundListToKeepOnlyDataType(roundsAndPartsList, "room_impression");


        ArrayList<Double> gameTimeRatioList = new ArrayList<>();
        for (int i = 0; i < filteredTimeRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredTimeRoundList.get(i);
            Integer scoreHelper = 0;
            Integer result = 0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("entered")) {
                    try {
                        if (element2.getValue().getInt("entered") > scoreHelper) {
                            scoreHelper = element2.getValue().getInt("entered");
                            if (scoreHelper >= 1680  && scoreHelper < 1800){
                                result = 10;
                            }else if (scoreHelper < 1680) {
                                int testDiff = 1680 - scoreHelper;
                                testDiff = testDiff / 120;
                                result = (int) (10.0 - 1.2 * (testDiff));
                            }else if (scoreHelper == 2400){
                                result = 5;
                            }else if (scoreHelper > 1800){
                                int testDiff = scoreHelper - 1800;
                                testDiff = testDiff/120;
                                result = (int)(10.0 - (testDiff+1));
                            }
                        }
                        counterHelper += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (scoreHelper == 0) {
                    gameTimeRatioList.add(0.0);
                } else
                    gameTimeRatioList.add((double) result);
            }


        }

        Double scoreTime = 0.0;
        for (int i = 0; i < gameTimeRatioList.size(); i++) {
            scoreTime += gameTimeRatioList.get(i);
        }
        if (gameTimeRatioList.size() > 0 && scoreTime > 0) {
            scoreTime = 4 * (scoreTime / gameTimeRatioList.size());
        }

        ArrayList<Double> gameStoryRatioList = new ArrayList<>();
        for (int i = 0; i < filteredStoryRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredStoryRoundList.get(i);
            Integer scoreHelper = 0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("entered")) {
                    try {
                        if (element2.getValue().getInt("entered") > scoreHelper) {
                            scoreHelper = element2.getValue().getInt("entered");
                        }
                        counterHelper += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (scoreHelper == 0) {
                    gameStoryRatioList.add(0.0);
                } else
                    gameStoryRatioList.add((double) scoreHelper);
            }

        }

        Double scoreStory = 0.0;
        for (int i = 0; i < gameStoryRatioList.size(); i++) {
            scoreStory += gameStoryRatioList.get(i);
        }
        if (gameStoryRatioList.size() > 0 && scoreStory > 0) {
            scoreStory = 4 * (scoreStory / gameStoryRatioList.size());
        }

        ArrayList<Double> gameAtmosphereRatioList = new ArrayList<>();
        for (int i = 0; i < filteredAtmosphereRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredAtmosphereRoundList.get(i);
            Integer scoreHelper = 0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("entered")) {
                    try {
                        if (element2.getValue().getInt("entered") > scoreHelper) {
                            scoreHelper = element2.getValue().getInt("entered");
                        }
                        counterHelper += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (scoreHelper == 0) {
                    gameAtmosphereRatioList.add(0.0);
                } else
                    gameAtmosphereRatioList.add((double) scoreHelper);
            }


        }

        Double scoreAtmosphere = 0.0;
        for (int i = 0; i < gameAtmosphereRatioList.size(); i++) {
            scoreAtmosphere += gameAtmosphereRatioList.get(i);
        }
        if (gameAtmosphereRatioList.size() > 0 && scoreAtmosphere > 0) {
            scoreAtmosphere = 6 * (scoreAtmosphere / gameAtmosphereRatioList.size());
        }

        ArrayList<Double> gamePuzzleDiversityRatioList = new ArrayList<>();
        for (int i = 0; i < filteredPuzzleDiversityRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredPuzzleDiversityRoundList.get(i);
            Integer scoreHelper = 0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("point1")) {
                    try {
                        if (element2.getValue().getInt("point1") > 0) {
                            scoreHelper = element2.getValue().getInt("point1");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (element2.getValue().has("point2")) {
                    try {
                        if (element2.getValue().getInt("point2") > 0) {
                            scoreHelper += element2.getValue().getInt("point2");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (scoreHelper == 0) {
                    gamePuzzleDiversityRatioList.add(0.0);
                } else
                    gamePuzzleDiversityRatioList.add((double) scoreHelper);
            }


        }

        Double scorePuzzleDiversity = 0.0;
        for (int i = 0; i < gamePuzzleDiversityRatioList.size(); i++) {
            scorePuzzleDiversity += gamePuzzleDiversityRatioList.get(i);
        }
        if (gamePuzzleDiversityRatioList.size() > 0 && scorePuzzleDiversity > 0) {
            scorePuzzleDiversity = 4 * (scorePuzzleDiversity / gamePuzzleDiversityRatioList.size());
        }

        ArrayList<Double> gameMechanismDiversityRatioList = new ArrayList<>();
        for (int i = 0; i < filteredMechanismDiversityRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredMechanismDiversityRoundList.get(i);
            Double scoreHelper = 0.0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("point1")) {
                    try {
                        if (element2.getValue().getInt("point1") > 0) {
                            scoreHelper = element2.getValue().getDouble("point1") / 2;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (element2.getValue().has("point2")) {
                    try {
                        if (element2.getValue().getInt("point2") > 0) {
                            scoreHelper += element2.getValue().getDouble("point2");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (scoreHelper == 0) {
                    gameMechanismDiversityRatioList.add(0.0);
                } else
                    gameMechanismDiversityRatioList.add(scoreHelper);
            }


        }

        Double scoreMechanismDiversity = 0.0;
        for (int i = 0; i < gameMechanismDiversityRatioList.size(); i++) {
            scoreMechanismDiversity += gameMechanismDiversityRatioList.get(i);
        }
        if (gameMechanismDiversityRatioList.size() > 0 && scorePuzzleDiversity > 0) {
            scoreMechanismDiversity = 4 * (scoreMechanismDiversity / gameMechanismDiversityRatioList.size());
        }


        ArrayList<Double> gameCluesRatioList = new ArrayList<>();
        for (int i = 0; i < filteredCluesRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredCluesRoundList.get(i);
            Integer scoreHelper = 0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("entered")) {
                    try {
                        if (element2.getValue().getInt("entered") > scoreHelper) {
                            scoreHelper = element2.getValue().getInt("entered");
                        }
                        counterHelper += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (scoreHelper == 0) {
                    gameCluesRatioList.add(0.0);
                } else
                    gameCluesRatioList.add((double) scoreHelper);
            }


        }

        Double scoreClues = 0.0;
        for (int i = 0; i < gameCluesRatioList.size(); i++) {
            scoreClues += gameCluesRatioList.get(i);
        }
        if (gameCluesRatioList.size() > 0 && scoreClues > 0) {
            scoreClues = 2 * (scoreClues / gameCluesRatioList.size());
        }


        ArrayList<Double> gameLearningConnectionRatioList = new ArrayList<>();
        for (int i = 0; i < filteredLearningConnectionRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredLearningConnectionRoundList.get(i);
            Integer scoreHelper = 0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("entered")) {
                    try {
                        if (element2.getValue().getInt("entered") > scoreHelper) {
                            scoreHelper = element2.getValue().getInt("entered");
                        }
                        counterHelper += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (scoreHelper == 0) {
                    gameLearningConnectionRatioList.add(0.0);
                } else
                    gameLearningConnectionRatioList.add((double) scoreHelper);
            }


        }

        Double scoreLearningConnection = 0.0;
        for (int i = 0; i < gameLearningConnectionRatioList.size(); i++) {
            scoreLearningConnection += gameLearningConnectionRatioList.get(i);
        }
        if (gameLearningConnectionRatioList.size() > 0 && scoreLearningConnection > 0) {
            scoreLearningConnection = 4 * (scoreLearningConnection / gameLearningConnectionRatioList.size());
        }

        ArrayList<Double> gameEffectRatioList = new ArrayList<>();
        for (int i = 0; i < filteredEffectRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredEffectRoundList.get(i);
            Integer scoreHelper = 0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("entered")) {
                    try {
                        if (element2.getValue().getInt("entered") > scoreHelper) {
                            scoreHelper = element2.getValue().getInt("entered");
                        }
                        counterHelper += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (scoreHelper == 0) {
                    gameEffectRatioList.add(0.0);
                } else
                    gameEffectRatioList.add((double) scoreHelper);
            }


        }

        Double scoreEffect = 0.0;
        for (int i = 0; i < gameEffectRatioList.size(); i++) {
            scoreEffect += gameEffectRatioList.get(i);
        }
        if (gameEffectRatioList.size() > 0 && scoreEffect > 0) {
            scoreEffect = 2 * (scoreEffect / gameEffectRatioList.size());
        }

        ArrayList<Double> gameRoomImpressionList = new ArrayList<>();
        for (int i = 0; i < filteredRoomImpressionRoundList.size(); i++) {
            ArrayList<RoundPartItem> element = filteredRoomImpressionRoundList.get(i);
            Integer scoreHelper = 0;
            Integer counterHelper = 0;
            for (RoundPartItem element2 : element) {
                if (element2.getValue().has("entered")) {
                    try {
                        if (element2.getValue().getInt("entered") > scoreHelper) {
                            scoreHelper = element2.getValue().getInt("entered");
                        }
                        counterHelper += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (scoreHelper == 0) {
                    gameRoomImpressionList.add(0.0);
                } else
                    gameRoomImpressionList.add((double) scoreHelper);
            }


        }

        Double scoreRoomImpression = 0.0;
        for (int i = 0; i < gameRoomImpressionList.size(); i++) {
            scoreRoomImpression += gameRoomImpressionList.get(i);
        }
        if (gameRoomImpressionList.size() > 0 && scoreRoomImpression > 0) {
            scoreRoomImpression = 4 * (scoreRoomImpression / gameRoomImpressionList.size());
        }

        return scoreRoomImpression + scoreClues + scoreEffect + scoreLearningConnection + scoreTime + scoreStory + scoreAtmosphere + scorePuzzleDiversity + scoreMechanismDiversity;
    }


    private ArrayList<RoundPartItem> getGroupAverageCrackRounds(String groupId) {
        ArrayList<RoundPartItem> result = new ArrayList<>();
        ArrayList<RecyclerItem> all = new ArrayList<>();
        all.addAll(Config.roundPartsMap.values());
        for (int i = 0; i < all.size() ; i++) {
            RoundPartItem tempItem = (RoundPartItem)all.get(i);
            if (tempItem.getPartDataType().equals("team_work")){
                if (tempItem.getValue().has("group_id")) {
                    try {
                        String foundGroupId = tempItem.getValue().getString("group_id");
                        if (foundGroupId.equals(groupId)) {
                            result.add(tempItem);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return result;
    }

    private ArrayList<RoundPartItem> getGroupAveragePuzzleRounds(String groupId) {
        ArrayList<RoundPartItem> result = new ArrayList<>();
        ArrayList<RecyclerItem> roundsList = new ArrayList<>();
        roundsList.addAll(Config.roundsMap.values());
        ArrayList<ArrayList<RoundPartItem>> roundsWithPartsList = new ArrayList<>();
        for (int j = 0; j < roundsList.size(); j++) {
            RoundItem roundItem = (RoundItem) roundsList.get(j);
            if (roundItem.getGroup().equals(groupId)) {
                ArrayList<RecyclerItem> roundPartsList = new ArrayList<>();
                roundPartsList.addAll(Config.roundPartsMap.values());
                for (int i = 0; i < roundPartsList.size(); i++) {
                    RoundPartItem roundPartItem = (RoundPartItem) roundPartsList.get(i);
                    if (roundPartItem.getRound_id().equals(roundItem.get_id()) && roundPartItem.getPartDataType().equals("excelled_puzzle")) {
                        result.add(roundPartItem);
                    }

                }
            }
        }

        return result;
    }

    private void listGroupsByBestCracker(){
        pointsList = new HashMap<>();
        dataArray.clear();

        dataArray.addAll(Config.groupsMap.values());
        Collections.sort(dataArray, new Comparator<RecyclerItem>() {
            @Override
            public int compare(RecyclerItem recyclerItem1, RecyclerItem recyclerItem2) {

                ArrayList<RoundPartItem> firstRoundList = getGroupAverageCrackRounds(recyclerItem1.get_id());
                ArrayList<RoundPartItem> secondRoundList = getGroupAverageCrackRounds(recyclerItem2.get_id());
                double firstScore = 0;

                double secondScore = 0;
                double secondScore2 = 0;
                int firstCount = 0;
                int secondCount = 0;

                for (RoundPartItem element : firstRoundList) {
                    if (element.getValue().has("point1")) {
                        try {

                            int tempScore = element.getValue().getInt("point1")*5;

                            firstCount +=1;
                            double firstScore2 = 0;
                            ArrayList<RoundPartItem> roomResults = getGroupWorksCrackerPartsRoundId(element.getRound_id());
                            if (roomResults.size() == 3){
                                try {
                                    int winCount = 0;
                                    int currentTimeValue = 0;
                                    int elementPos = 0;
                                    int value1 = roomResults.get(0).getValue().getInt("entered");
                                    if (roomResults.get(0).getRound_id().equals(element.getRound_id())){
                                        currentTimeValue = value1;
                                    }
                                    int value2 = roomResults.get(1).getValue().getInt("entered");
                                    if (roomResults.get(1).getRound_id().equals(element.getRound_id())){
                                        currentTimeValue = value2;
                                    }
                                    int value3 = roomResults.get(2).getValue().getInt("entered");
                                    if (roomResults.get(2).getRound_id().equals(element.getRound_id())){
                                        currentTimeValue = value3;
                                    }
                                    if (value1 == value2 && value1 == value3 && value1 == 2400){
                                        firstScore2 = 16;
                                    }else {
                                        if (currentTimeValue < value1) {
                                            winCount += 1;
                                        }
                                        if (currentTimeValue < value2) {
                                            winCount += 1;
                                        }
                                        if (currentTimeValue < value3) {
                                            winCount += 1;
                                        }
                                        if (winCount == 2) {
                                            firstScore2 = 50;
                                        }
                                        if (winCount == 1) {
                                            firstScore2 = 25;
                                        }
                                        if (winCount == 0) {
                                            firstScore2 = 0;
                                        }
                                        tempScore += firstScore2;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            firstScore += tempScore;


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                for (RoundPartItem element : secondRoundList) {
                    if (element.getValue().has("point1")) {
                        try {

                            int tempScore = element.getValue().getInt("point1")*5;

                            secondCount +=1;
                            double firstScore2 = 0;
                            ArrayList<RoundPartItem> roomResults = getGroupWorksCrackerPartsRoundId(element.getRound_id());
                            if (roomResults.size() == 3){
                                try {
                                    int winCount = 0;
                                    int currentTimeValue = 0;
                                    int elementPos = 0;
                                    int value1 = roomResults.get(0).getValue().getInt("entered");
                                    if (roomResults.get(0).getRound_id().equals(element.getRound_id())){
                                        currentTimeValue = value1;
                                    }
                                    int value2 = roomResults.get(1).getValue().getInt("entered");
                                    if (roomResults.get(1).getRound_id().equals(element.getRound_id())){
                                        currentTimeValue = value2;
                                    }
                                    int value3 = roomResults.get(2).getValue().getInt("entered");
                                    if (roomResults.get(2).getRound_id().equals(element.getRound_id())){
                                        currentTimeValue = value3;
                                    }
                                    if (value1 == value2 && value1 == value3 && value1 == 2400){
                                        firstScore2 = 16;
                                    }else {
                                        if (currentTimeValue < value1) {
                                            winCount += 1;
                                        }
                                        if (currentTimeValue < value2) {
                                            winCount += 1;
                                        }
                                        if (currentTimeValue < value3) {
                                            winCount += 1;
                                        }
                                        if (winCount == 2) {
                                            firstScore2 = 50;
                                        }
                                        if (winCount == 1) {
                                            firstScore2 = 25;
                                        }
                                        if (winCount == 0) {
                                            firstScore2 = 0;
                                        }
                                        tempScore += firstScore2;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            secondScore += tempScore;


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                pointsList.put(recyclerItem1.get_id(),firstScore);
                pointsList.put(recyclerItem2.get_id(),secondScore);

                if (firstScore > secondScore){
                    return -1;
                }
                if (firstScore < secondScore){
                    return 1;
                }
                return 0;
            }
        });
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        dataArray.add(0,recyclerItem);
        executeRefreshAdapterData();

    }


    private int getTimePunishmentValue(Integer entered){
        if (entered == null){
            return 0;
        }
        int result = 0;
        if (entered >= 1680  && entered < 1800){
            result = 10;
        }else if (entered < 1680) {
            int testDiff = 1680 - entered;
            testDiff = testDiff / 120;
            result = (int) (10.0 - 1.2 * (testDiff));
        }else if (entered == 2400){
            result = 5;
        }else if (entered > 1800){
            int testDiff = entered - 1800;
            testDiff = testDiff/120;
            result = (int)(10.0 - (testDiff+1));
        }
        return result;
    }



    private Double getGroupCommunityResult(String groupId){
        ArrayList<RoundPartItem> firstRoundList = getRoundPartsByDataType(getAllRoundPartsForByGroupId(groupId),"community_score");
        double firstScore = 0.0;

        for (RoundPartItem element : firstRoundList) {
            if (element.getValue().has("score")) {
                try {
                    firstScore += (360.0/100)*element.getValue().getInt("score");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (element.getValue().has("milestone_score")) {
                try {
                    firstScore += (40.0/100)*element.getValue().getInt("milestone_score");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        return firstScore;
    }

    private Double getGroupEscapePackageResult(String groupId){
        ArrayList<RoundPartItem> firstRoundList = getRoundPartsByDataType(getAllRoundPartsForByGroupId(groupId),"escape_package");
        double firstScore = 0.0;

        for (RoundPartItem element : firstRoundList) {
            if (element.getValue().has("score")) {
                try {
                    firstScore += (180.0/100)*element.getValue().getInt("score");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (element.getValue().has("milestone_score")) {
                try {
                    firstScore += (20.0/100)*element.getValue().getInt("milestone_score");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        return firstScore;
    }

    private void listGroupsByBestCommunity(){

        pointsList = new HashMap<>();
        dataArray.clear();

        dataArray.addAll(Config.groupsMap.values());
        Collections.sort(dataArray, new Comparator<RecyclerItem>() {
            @Override
            public int compare(RecyclerItem recyclerItem1, RecyclerItem recyclerItem2) {

                ArrayList<RoundPartItem> firstRoundList = getRoundPartsByDataType(getAllRoundPartsForByGroupId(recyclerItem1.get_id()),"community_score");
                ArrayList<RoundPartItem> secondRoundList = getRoundPartsByDataType(getAllRoundPartsForByGroupId(recyclerItem2.get_id()),"community_score");
                double firstScore = 0.0;
                double secondScore = 0.0;
                int firstCount = 0;
                int secondCount = 0;

                for (RoundPartItem element : firstRoundList) {
                    if (element.getValue().has("score")) {
                        try {
                            firstScore += (360.0/100)*element.getValue().getInt("score");
                            firstCount +=1;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (element.getValue().has("milestone_score")) {
                        try {
                            firstScore += (40.0/100)*element.getValue().getInt("milestone_score");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                for (RoundPartItem element : secondRoundList) {
                    if (element.getValue().has("score")) {
                        try {
                            secondScore += (360.0/100)*element.getValue().getInt("score");
                            secondCount +=1;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (element.getValue().has("milestone_score")) {
                        try {
                            secondScore += (40.0/100)*element.getValue().getInt("milestone_score");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (firstCount > 0 && firstScore>0)
                    firstScore = firstScore/firstCount;
                if (secondCount > 0 && secondScore>0)
                    secondScore = secondScore/secondCount;

                pointsList.put(recyclerItem1.get_id(),firstScore);
                pointsList.put(recyclerItem2.get_id(),secondScore);

                if (firstScore > secondScore){
                    return -1;
                }
                if (firstScore < secondScore){
                    return 1;
                }
                return 0;
            }
        });
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        dataArray.add(0,recyclerItem);
        executeRefreshAdapterData();

    }


    private void listGroupsByBestEscapePackage(){

        pointsList = new HashMap<>();
        dataArray.clear();

        dataArray.addAll(Config.groupsMap.values());
        Collections.sort(dataArray, new Comparator<RecyclerItem>() {
            @Override
            public int compare(RecyclerItem recyclerItem1, RecyclerItem recyclerItem2) {

                ArrayList<RoundPartItem> firstRoundList = getRoundPartsByDataType(getAllRoundPartsForByGroupId(recyclerItem1.get_id()),"escape_package");
                ArrayList<RoundPartItem> secondRoundList = getRoundPartsByDataType(getAllRoundPartsForByGroupId(recyclerItem2.get_id()),"escape_package");
                double firstScore = 0.0;
                double secondScore = 0.0;
                int firstCount = 0;
                int secondCount = 0;

                for (RoundPartItem element : firstRoundList) {
                    if (element.getValue().has("score")) {
                        try {
                            firstScore += (180.0/100)*element.getValue().getInt("score");
                            firstCount +=1;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (element.getValue().has("milestone_score")) {
                        try {
                            firstScore += (20.0/100)*element.getValue().getInt("milestone_score");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                for (RoundPartItem element : secondRoundList) {
                    if (element.getValue().has("score")) {
                        try {
                            secondScore += (180.0/100)*element.getValue().getInt("score");
                            secondCount +=1;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (element.getValue().has("milestone_score")) {
                        try {
                            secondScore += (20.0/100)*element.getValue().getInt("milestone_score");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (firstCount > 0 && firstScore>0)
                    firstScore = firstScore/firstCount;
                if (secondCount > 0 && secondScore>0)
                    secondScore = secondScore/secondCount;

                pointsList.put(recyclerItem1.get_id(),firstScore);
                pointsList.put(recyclerItem2.get_id(),secondScore);

                if (firstScore > secondScore){
                    return -1;
                }
                if (firstScore < secondScore){
                    return 1;
                }
                return 0;
            }
        });
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        dataArray.add(0,recyclerItem);
        executeRefreshAdapterData();

    }

    private void listGroupsByBestInnovation(){
        pointsList = new HashMap<>();
        dataArray.clear();

        dataArray.addAll(Config.groupsMap.values());
        Collections.sort(dataArray, new Comparator<RecyclerItem>() {
            @Override
            public int compare(RecyclerItem recyclerItem1, RecyclerItem recyclerItem2) {

                ArrayList<RoundPartItem> firstRoundList = getRoundPartsByDataType(getAllRoundPartsForByGroupId(recyclerItem1.get_id()),"innovation");
                ArrayList<RoundPartItem> secondRoundList = getRoundPartsByDataType(getAllRoundPartsForByGroupId(recyclerItem2.get_id()),"innovation");
                double firstScore = 0;
                double secondScore = 0;
                int firstCount = 0;
                int secondCount = 0;

                for (RoundPartItem element : firstRoundList) {
                    if (element.getValue().has("entered")) {
                        try {
                            firstScore += element.getValue().getInt("entered")*10;
                            firstCount +=1;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                for (RoundPartItem element : secondRoundList) {
                    if (element.getValue().has("entered")) {
                        try {
                            secondScore += element.getValue().getInt("entered")*10;
                            secondCount +=1;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }


                if (firstScore>0 && firstCount > 0)
                    firstScore = firstScore/firstCount;
                if (secondScore>0 && secondCount > 0)
                    secondScore = secondScore/secondCount;

                pointsList.put(recyclerItem1.get_id(),firstScore);
                pointsList.put(recyclerItem2.get_id(),secondScore);

                if (firstScore > secondScore){
                    return -1;
                }
                if (firstScore < secondScore){
                    return 1;
                }
                return 0;
            }
        });
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        dataArray.add(0,recyclerItem);
        executeRefreshAdapterData();
    }




    private ArrayList<RecyclerItem> getSortedBestGamePerformers(final String dataType){
        final int minScore;
        final int maxScore;
        if (dataType.equals("game3")){
            maxScore = 105;
            minScore = 35;
        }else{
            maxScore = 140;
            minScore = 0;
        }
        ArrayList<RecyclerItem> order = new ArrayList<>();

        order.addAll(Config.groupsMap.values());
        Collections.sort(order, new Comparator<RecyclerItem>() {
            @Override
            public int compare(RecyclerItem recyclerItem1, RecyclerItem recyclerItem2) {
                Double score1 = getSingleGameAverageRatio(recyclerItem1.get_id(), dataType);
                Double score2 = getSingleGameAverageRatio(recyclerItem2.get_id(), dataType);

                pointsList.put(recyclerItem1.get_id(),(score1*maxScore) +minScore);
                pointsList.put(recyclerItem2.get_id(),(score2*maxScore) +minScore);

                if (score1 > score2){
                    return -1;
                }
                if (score1 < score2){
                    return 1;
                }
                return 0;
            }
        });
        return order;
    }


    private void listBestGroupsByEachGame(int gameNum){
        pointsList = new HashMap<>();
        dataArray.clear();
        //dataArray.addAll(Config.groupsMap.values());
        ArrayList<RecyclerItem> gameWinner = new ArrayList<>();
        switch (gameNum) {
            case 1: {
                gameWinner = getSortedBestGamePerformers("game1");
                break;
            }
            case 2: {
                gameWinner = getSortedBestGamePerformers("game2");
                break;
            }
            case 3: {
                gameWinner = getSortedBestGamePerformers("game3");
                break;
            }
        }

        dataArray = gameWinner;
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        dataArray.add(0,recyclerItem);
        executeRefreshAdapterData();
    }


    private void listGenericTotalWinnersList(){
        pointsList = new HashMap<>();
        dataArray.clear();
        dataArray.addAll(Config.groupsMap.values());
        Collections.sort(dataArray, new Comparator<RecyclerItem>() {
            @Override
            public int compare(RecyclerItem recyclerItem1, RecyclerItem recyclerItem2) {
                Double totalScore1 = getGroupTotalScore(recyclerItem1.get_id());
                Double totalScore2 = getGroupTotalScore(recyclerItem2.get_id());

                pointsList.put(recyclerItem1.get_id(),totalScore1);
                pointsList.put(recyclerItem2.get_id(),totalScore2);

                if (totalScore1 > totalScore2){
                    return -1;
                }
                if (totalScore1 < totalScore2){
                    return 1;
                }
                return 0;
            }
        });
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        dataArray.add(0,recyclerItem);
        executeRefreshAdapterData();
    }




    private Double getRoundAverageGameScore(String roundId,int roundOrder,String group_id){
        RoundItem roundItem = (RoundItem)Config.roundsMap.get(roundId);
        if (roundItem == null){
            return -1.0;
        }

        ArrayList<RecyclerItem> roundPartsList = new ArrayList<>();
        roundPartsList.addAll(Config.roundPartsMap.values());
        ArrayList<RoundPartItem> chosenRoundPartsList = new ArrayList<>();
        for (int i = 0; i < roundPartsList.size() ; i++) {
            RoundPartItem roundPartItem = (RoundPartItem) roundPartsList.get(i);
            if (roundPartItem.getRound_id().equals(roundId)){
                chosenRoundPartsList.add(roundPartItem);
            }

        }
        return getRoundGamePoints(chosenRoundPartsList,roundOrder,group_id);
    }




    @Subscribe(threadMode = ThreadMode.MAIN)
    public void requestUpdateGroupPoints(UpdatePointsForGroupIdEvent event){
        Double groupNewPoints = getGroupTotalScore(event.getId());
        JSONArray params = new JSONArray();
        params.put(event.getId());
        params.put(groupNewPoints);

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_GROUP_POINTS,params,new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                }
            });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGroupPoints");

    }


    Double getRoundGamePoints(ArrayList<RoundPartItem> chosenRoundPartsList, int roundOrder,String group_id) {
        ArrayList<RoundPartItem> game1List = new ArrayList<>();
        ArrayList<RoundPartItem> game2List = new ArrayList<>();
        ArrayList<RoundPartItem> game3List = new ArrayList<>();

        for (int i = 0; i < chosenRoundPartsList.size(); i++) {
            RoundPartItem roundPart = chosenRoundPartsList.get(i);
            switch (roundPart.getPartDataType()) {
                case "game1": {
                    game1List.add(roundPart);
                    break;
                }
                case "game2": {
                    game2List.add(roundPart);
                    break;
                }
                case "game3": {
                    game3List.add(roundPart);
                    break;
                }
            }
        }


        Integer scoreHelper1 = 0;
        Integer counterHelper1 = 0;
        Integer game1Score = 0;
        Integer bonusPoints = 0;
        for (RoundPartItem element : game1List) {
            if (element.getValue().has("entered")) {
                try {
                    if (element.getValue().getInt("entered") > scoreHelper1) {
                        scoreHelper1 = element.getValue().getInt("entered");
                        if (scoreHelper1 == 7) {
                            if (element.getValue().has("last_update_time_remain")) {
                                try {
                                    int remainingTime = element.getValue().getInt("last_update_time_remain");
                                    remainingTime = remainingTime / 1000;
                                    if (remainingTime > 40) {
                                        remainingTime = 40;
                                    }
                                    bonusPoints = (int) (0.5 * remainingTime);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    counterHelper1 += 1;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (counterHelper1 > 1) {
            game1Score = scoreHelper1 * 30 + (int)(bonusPoints*1.5);
        } else if (counterHelper1 == 1) {
            game1Score = scoreHelper1 * 20 + bonusPoints;
        }


        double scoreHelper2 = 0;
        Integer counterHelper2 = 0;
        Double game2Score = 0.0;

        for (RoundPartItem element : game2List) {

            double score2test = getGame2RoundPartScoreRatio(element.getValue());
            if (score2test > scoreHelper2) {
                scoreHelper2 = score2test;
            }
            counterHelper2 += 1;
        }
        if (counterHelper2 > 1) {
            double scoreRatio = scoreHelper2 * 210;
            if (scoreRatio > 210) {
                scoreRatio = 210;
            }
            game2Score = scoreRatio;
        } else if (counterHelper2 == 1) {
            double scoreRatio = scoreHelper2 * 140;
            if (scoreRatio > 140) {
                scoreRatio = 140;
            }
            game2Score = scoreRatio;
        }






        Double scoreHelper3 = 0.0;
        Integer counterHelper3 = 0;
        Double game3Score = 0.0;


        scoreHelper3 = ((35+(105*getGroupPlaceInVoltageDevision(group_id,roundOrder)))/140);
        for (RoundPartItem element : game3List) {
                if (element.getValue().has("volts")) {
                    counterHelper3 += 1;
                }
        }
        if (counterHelper3 > 0) {
            game3Score = scoreHelper3 * 140;
            if (counterHelper3 == 2) {
                game3Score = scoreHelper3 * 210;
            }
        }

        return game3Score + game1Score + game2Score;
    }


    private Double getCommunityScoreByRoundId(String roundId){
        if (pointsList == null) {
            pointsList = new HashMap<>();
        }
        ArrayList<RecyclerItem> roundPartsList = new ArrayList<>();
        roundPartsList.addAll(Config.roundPartsMap.values());
        ArrayList<RoundPartItem> chosenRoundPartsList = new ArrayList<>();
        for (int i = 0; i < roundPartsList.size() ; i++) {
            RoundPartItem roundPartItem = (RoundPartItem) roundPartsList.get(i);
            if (roundPartItem.getRound_id().equals(roundId)){
                chosenRoundPartsList.add(roundPartItem);
            }

        }

        if (chosenRoundPartsList.size() > 0){
            double total_score = 0.0;
            String communityRoundPart = chosenRoundPartsList.get(0).get_id();
            if (chosenRoundPartsList.get(0).getValue().has("score")) {
                try {
                    total_score = (390.0/100)*chosenRoundPartsList.get(0).getValue().getDouble("score");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (chosenRoundPartsList.get(0).getValue().has("milestone_score")) {
                try {
                    total_score += (40.0/100)*chosenRoundPartsList.get(0).getValue().getDouble("milestone_score");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            pointsList.put(communityRoundPart,total_score);
            return total_score;
        }
        return 0.0;
    }


    private Double getCopterScoreByRoundId(String roundId,String groupId){

        ArrayList<RecyclerItem> roundPartsList = new ArrayList<>();
        roundPartsList.addAll(Config.roundPartsMap.values());
        ArrayList<RoundPartItem> chosenRoundPartsList = new ArrayList<>();
        for (int i = 0; i < roundPartsList.size() ; i++) {
            RoundPartItem roundPartItem = (RoundPartItem) roundPartsList.get(i);
            if (roundPartItem.getRound_id().equals(roundId) && roundPartItem.getPartDataType().equals("copter")){
                chosenRoundPartsList.add(roundPartItem);
            }

        }
        Double score = 0.0;
        int count = 0;
        if (chosenRoundPartsList.size()>0){
            if (chosenRoundPartsList.get(0).getValue().has("entered")) {
                try {
                    score += chosenRoundPartsList.get(0).getValue().getDouble("entered");
                    count +=1;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        if (score>0 && count > 0)
            score = score/count;


        double ratio1 = getGameAverageRatio(groupId);

        return 12*score + 30*ratio1;

    }


    private Double getGroupTotalScore(String group_id) {
        Double gameResult = 0.0;
        Double comunityResult = 0.0;
        Double escapeResult = 0.0;
        Double adminExtra = 0.0;

        GroupItem group =  (GroupItem)Config.groupsMap.get(group_id);
        if (group != null) {
            adminExtra = group.getAdminExtra();
        }

        gameResult = getGameAverageRatio(group_id);
        comunityResult = getGroupCommunityResult(group_id);
        escapeResult = getGroupEscapePackageResult(group_id);


        double result = 0.0;
        result += gameResult;
        result += comunityResult;
        result += escapeResult;
        result += adminExtra;
        return result;
    }

    private void listGroupsByBestRounds(){
        pointsList = new HashMap<>();
        dataArray.clear();
        dataArray.addAll(Config.groupsMap.values());
        Collections.sort(dataArray, new Comparator<RecyclerItem>() {
            @Override
            public int compare(RecyclerItem recyclerItem1, RecyclerItem recyclerItem2) {

                double ratio1 = getGameAverageRatio(recyclerItem1.get_id());
                double ratio2 = getGameAverageRatio(recyclerItem2.get_id());

                pointsList.put(recyclerItem1.get_id(),ratio1);
                pointsList.put(recyclerItem2.get_id(),ratio2);

                if (ratio1 > ratio2){
                    return -1;
                }
                if (ratio1 < ratio2){
                    return 1;
                }
                return 0;
            }
        });
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        dataArray.add(0,recyclerItem);
        executeRefreshAdapterData();
    }






    private void listGroupsByBestPuzzle(){
        pointsList = new HashMap<>();
        descriptionList = new HashMap<>();
        dataArray.clear();

        dataArray.addAll(Config.groupsMap.values());
        Collections.sort(dataArray, new Comparator<RecyclerItem>() {
            @Override
            public int compare(RecyclerItem recyclerItem1, RecyclerItem recyclerItem2) {

                ArrayList<RoundPartItem> firstRoundList = getGroupAveragePuzzleRounds(recyclerItem1.get_id());
                ArrayList<RoundPartItem> secondRoundList = getGroupAveragePuzzleRounds(recyclerItem2.get_id());
                double firstScore = 0;

                double secondScore = 0;
                double secondScore2 = 0;
                int firstCount = 0;
                int secondCount = 0;
                String firstDescription = "";
                String secondDescription = "";
                for (RoundPartItem element : firstRoundList) {
                    if (element.getValue().has("entered")) {
                        try {
                            int tempScore = element.getValue().getInt("entered")*7;
                            int hetrashmut = 0;
                            firstCount +=1;
                            RoundPartItem roomResults = getGroupHetrashmutPartsRoundId(element.getRound_id());
                            try {
                                hetrashmut = roomResults.getValue().getInt("entered")*3;
                                firstScore += hetrashmut;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            firstScore += tempScore;
                            String puzzleName = "";

                            try {
                                puzzleName = element.getValue().getString("puzzle_name");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            int sumsingle = tempScore+hetrashmut;
                            firstDescription += "חידה: " + puzzleName + "["+ sumsingle +"] , ";
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                for (RoundPartItem element : secondRoundList) {
                    if (element.getValue().has("entered")) {
                        try {
                            int tempScore = element.getValue().getInt("entered")*7;
                            int hetrashmut = 0;
                            secondCount +=1;
                            RoundPartItem roomResults = getGroupHetrashmutPartsRoundId(element.getRound_id());
                            try {
                                hetrashmut = roomResults.getValue().getInt("entered")*3;
                                secondScore += hetrashmut;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            secondScore += tempScore;
                            String puzzleName = "";

                            try {
                                puzzleName = element.getValue().getString("puzzle_name");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            int sumsingle = tempScore+hetrashmut;
                            secondDescription += "חידה: " + puzzleName + "["+ sumsingle +"] , ";
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                pointsList.put(recyclerItem1.get_id(),firstScore);
                pointsList.put(recyclerItem2.get_id(),secondScore);

                descriptionList.put(recyclerItem1.get_id(),firstDescription);
                descriptionList.put(recyclerItem2.get_id(),secondDescription);

                if (firstScore > secondScore){
                    return -1;
                }
                if (firstScore < secondScore){
                    return 1;
                }
                return 0;
            }
        });
        RecyclerItem recyclerItem = new GroupItem();
        recyclerItem.setListing(true);
        dataArray.add(0,recyclerItem);
        executeRefreshAdapterData();

    }




    class RoundsOrderViewHolder extends ViewHolder {
        private final TextView nameView;

        public TextView getNameView() {
            return nameView;
        }

        public TextView getDurationView() {
            return durationView;
        }

        public TextView getOwnerView() {
            return ownerView;
        }

        public TextView getStageView() {
            return stageView;
        }

        public TextView getTypeView() {
            return typeView;
        }

        private final TextView durationView;
        private final TextView ownerView;
        private final TextView stageView;
        private final TextView typeView;

        /*
        @Subscribe(threadMode = ThreadMode.MAIN)
        public void notifyChangeInRoundItem(UpdateRoundDelegate event) {
            if (event.getDocumentID().equals(get_id())){
                roundItem.updateByJson(event.getUpdatedValuesJson());

            }
        }
        */

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void setSelectedStage(SelectedStageEvent event){
            if ((event.get_id()).equals(get_id())){
                setBgColorBySelection(true);
            }else
                setBgColorBySelection(false);
        }

        public void setBgColorBySelection(boolean isSelected){
            if (isSelected){
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimaryReady));
            }else
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary));
        }



        public RoundsOrderViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view3);
            nameView = (TextView) itemView.findViewById(R.id.name);
            durationView = (TextView) itemView.findViewById(R.id.round_duration);
            ownerView = (TextView) itemView.findViewById(R.id.owner);
            stageView = (TextView) itemView.findViewById(R.id.max_points);
            typeView = (TextView) itemView.findViewById(R.id.stage_type);

        }

    }

}
interface ItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
