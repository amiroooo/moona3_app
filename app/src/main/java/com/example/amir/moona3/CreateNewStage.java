package com.example.amir.moona3;

/**
 * Created by Amir on 1/8/2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;


/**
 * Created by Amir on 11/19/2016.
 */

public class CreateNewStage extends BottomSheetDialog implements View.OnClickListener{

    private Context context;
    private EditText team1Editor;
    private EditText roundDurationEditor;
    private EditText reviewDurationEditor;
    private Spinner stageType;



    private RoundOrder orderForEdit;

    public CreateNewStage(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public void setOrderForEdit(RoundOrder orderForEdit) {
        this.orderForEdit = orderForEdit;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = LayoutInflater.from(getContext()).inflate(R.layout.new_stage_sheet,null);
        setContentView(root);

        team1Editor = (EditText)root.findViewById(R.id.editVideoId);
        roundDurationEditor = (EditText)root.findViewById(R.id.edit_round_duration);
        reviewDurationEditor = (EditText)root.findViewById(R.id.edit_review_duration);
        stageType = (Spinner)root.findViewById(R.id.edit_type);

        Button confirmButton = (Button) root.findViewById(R.id.button_confirm);
        Button cancelButton = (Button) root.findViewById(R.id.button_cancel);


        if (orderForEdit != null){
            team1Editor.setText(orderForEdit.getName());
            roundDurationEditor.setText(orderForEdit.getRoundTime().toString());
            reviewDurationEditor.setText(orderForEdit.getReviewTime().toString());
            if (orderForEdit.getStageType() == 1){
                stageType.setSelection(1);
            }
            stageType.setEnabled(false);
            //stageType.setVisibility(View.GONE);
            confirmButton.setText("Save Changes");
        }



        confirmButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancel();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_confirm){
            if (orderForEdit == null) {

                String tempId = team1Editor.getText().toString();
                if (tempId.length() > 0) {
                    cancel();
                }
                JSONArray params = new JSONArray();
                params.put(team1Editor.getText().toString());
                params.put(roundDurationEditor.getText().toString());
                params.put(reviewDurationEditor.getText().toString());
                params.put(stageType.getSelectedItemPosition());


                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL + Config.ADD_NEW_STAGE, params, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        if (context != null) {
                            //((RefreshFromString)context).onStringDataRecieved(response,0);
                            context = null;
                            cancel();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_new_stage");
            }else{

                String tempId = team1Editor.getText().toString();
                if (tempId.length() > 0) {
                    cancel();
                }
                JSONArray params = new JSONArray();
                params.put(team1Editor.getText().toString());
                params.put(roundDurationEditor.getText().toString());
                params.put(reviewDurationEditor.getText().toString());
                params.put(stageType.getSelectedItemPosition());
                params.put(orderForEdit.getStage());

                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL + Config.EDIT_STAGE, params, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        if (context != null) {
                            //((RefreshFromString)context).onStringDataRecieved(response,0);
                            context = null;
                            cancel();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "edit_stage");

            }
        }

        if (view.getId() == R.id.button_cancel){
            cancel();
        }
    }
}
