package com.example.amir.moona3;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Amir on 12/31/2016.
 */

public class RoundPartItem extends RecyclerItem implements Parcelable {


    private String round_id;
    private String round_part_type;
    private JSONObject value;

    public RoundPartItem() {
    }

    public RoundPartItem(String id, String round_id, String round_part_type, JSONObject value) {
        set_id(id);
        this.round_id = round_id;
        this.round_part_type = round_part_type;
        this.value = value;
    }

    public RoundPartItem(RoundPartItem roundPartItem) {
        set_id(roundPartItem.get_id());
        this.round_id = roundPartItem.getRound_id();
        this.round_part_type = roundPartItem.getRound_part_type();
        this.value = roundPartItem.getValue();
    }

    protected RoundPartItem(Parcel in) {
        round_id = in.readString();
        round_part_type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(round_id);
        dest.writeString(round_part_type);
    }

    public boolean isProgressed(){
        if (getValue().has("progressed")){
            try {
                int progress = getValue().getInt("progressed");
                if (progress > 0){
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoundPartItem> CREATOR = new Creator<RoundPartItem>() {
        @Override
        public RoundPartItem createFromParcel(Parcel in) {
            return new RoundPartItem(in);
        }

        @Override
        public RoundPartItem[] newArray(int size) {
            return new RoundPartItem[size];
        }
    };

    public String getRound_id() {
        return round_id;
    }

    public void setRound_id(String round_id) {
        this.round_id = round_id;
    }

    public String getRound_part_type() {
        return round_part_type;
    }

    public void setRound_part_type(String round_part_type) {
        this.round_part_type = round_part_type;
    }

    public JSONObject getValue() {
        return value;
    }

    public void setValue(JSONObject value) {
        this.value = value;
    }

    public String getPartDataType(){
        if (getValue().has("data_type")){
            try {
                return getValue().getString("data_type");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "unk";
    }

    public Long getPartTimeLeft(){
        if (getValue().has("paused")){
            try {
                return getValue().getLong("paused");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public Integer getRoundOrder(){
        RoundItem roundItem = (RoundItem)Config.roundsMap.get(getRound_id());
        if (roundItem != null){
            return roundItem.getRound_order();
        }
        return null;
    }

    @Override
    String getOfficialName() {
        return "RoundPartItem";
    }
}
