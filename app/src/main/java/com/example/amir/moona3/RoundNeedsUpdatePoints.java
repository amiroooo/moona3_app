package com.example.amir.moona3;

/**
 * Created by Amir on 3/31/2018.
 */

class RoundNeedsUpdatePoints {
    String round_id;
    RoundItem roundItem;
    public RoundNeedsUpdatePoints(String round_id,RecyclerItem item) {
        this.round_id = round_id;
        this.roundItem = (RoundItem)item;
    }

    public String getRound_id() {
        return round_id;
    }

    public RoundItem getRoundItem() {
        return roundItem;
    }
}
