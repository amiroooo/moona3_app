package com.example.amir.moona3;

import android.os.CountDownTimer;
import android.widget.TextView;

import com.dd.CircularProgressButton;

public abstract class MainCountDownTimer extends CountDownTimer {
    public Long timeLeft;
    public TextView cronometer;
    public CircularProgressButton circularButton;
    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public MainCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }
}
