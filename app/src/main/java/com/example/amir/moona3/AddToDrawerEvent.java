package com.example.amir.moona3;

class AddToDrawerEvent {
    RoundItem roundItem;
    Integer position;

    public AddToDrawerEvent(RoundItem roundItem, Integer position) {
        this.roundItem = roundItem;
        this.position = position;
    }

    public RoundItem getRoundItem() {
        return roundItem;
    }

    public Integer getPosition() {
        return position;
    }
}
