package com.example.amir.moona3;

/**
 * Created by Amir on 12/31/2016.
 */

public class ObstacleItem extends RecyclerItem{
    private String name;
    private String description;
    private String owner;
    private String maxPoints;
    private String tickPoints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(String maxPoints) {
        this.maxPoints = maxPoints;
    }

    public String getTickPoints() {
        return tickPoints;
    }
    public int getTickPointsAsInt() {
        return Integer.parseInt(tickPoints);
    }
    public int getMaxTickPointsAsInt() {
        return Integer.parseInt(maxPoints);
    }
    public void setTickPoints(String tickPoints) {
        this.tickPoints = tickPoints;
    }
    @Override
    public String getOfficialName() {
        return getName();
    }
}
