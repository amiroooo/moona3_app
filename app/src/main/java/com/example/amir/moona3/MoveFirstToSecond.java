package com.example.amir.moona3;

class MoveFirstToSecond {
    String roundId;

    public MoveFirstToSecond(String roundId) {
        this.roundId = roundId;
    }

    public String getRoundId() {
        return roundId;
    }
}
