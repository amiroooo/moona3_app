package com.example.amir.moona3;

/**
 * Created by Amir on 1/15/2017.
 */
public class PointsStatusEvent {
    public Integer getLeftobs1() {
        return leftobs1;
    }

    public void setLeftobs1(int leftobs1) {
        this.leftobs1 = leftobs1;
    }

    public Integer getLeftobs2() {
        return leftobs2;
    }

    public void setLeftobs2(int leftobs2) {
        this.leftobs2 = leftobs2;
    }

    public Integer getRightobs1() {
        return rightobs1;
    }

    public void setRightobs1(int rightobs1) {
        this.rightobs1 = rightobs1;
    }

    public Integer getRightobs2() {
        return rightobs2;
    }

    public void setRightobs2(int rightobs2) {
        this.rightobs2 = rightobs2;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    private Integer leftobs1 =0;
    private Integer leftobs2 =0;
    private Integer rightobs1 =0;
    private Integer rightobs2 =0;

    public Integer getRightBonus() {
        return rightBonus;
    }

    public void setRightBonus(Integer rightBonus) {
        this.rightBonus = rightBonus;
    }

    public Integer getLeftBonus() {
        return leftBonus;
    }

    public void setLeftBonus(Integer leftBonus) {
        this.leftBonus = leftBonus;
    }

    private Integer rightBonus =0;
    private Integer leftBonus =0;

    private String _id;


}
