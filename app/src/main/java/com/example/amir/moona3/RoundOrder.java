package com.example.amir.moona3;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Amir on 1/17/2017.
 */
public class RoundOrder extends RecyclerItem{


    private Integer stage;
    private Integer roundTime;
    private Integer reviewTime;
    private JSONArray list;
    private Integer stageType;

    public RoundOrder(JSONArray list, String _id, Integer stage) {
        this.list = list;
        set_id(_id);
        this.stage = stage;
    }

    public JSONArray getList() {
        return list;
    }

    public void setList(JSONArray list) {
        this.list = list;
    }


    @Override
    String getOfficialName() {
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public Integer getStage() {
        return stage;
    }

    public Integer getStageType() {
        return stageType;
    }

    public void setStageType(Integer stageType) {
        this.stageType = stageType;
    }

    public Integer getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(Integer roundTime) {
        this.roundTime = roundTime;
    }

    public Integer getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(Integer reviewTime) {
        this.reviewTime = reviewTime;
    }


    public int getRoundQueueOrder(String id){
        for (int i=0;i<list.length();i++){
            try {
                if (list.getString(i).equals(id)){
                    return i;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }
}
