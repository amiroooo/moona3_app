package com.example.amir.moona3;

/**
 * Created by Amir on 1/8/2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Amir on 11/19/2016.
 */

public class CreateNewGroup extends BottomSheetDialog implements View.OnClickListener{

    private Context context;
    private EditText team1Editor;
    private EditText team2Editor;
    private EditText team3Editor;
    private String roundId;

    public String getRoundId() {
        return roundId;
    }

    public void setRoundId(String roundId) {
        this.roundId = roundId;
    }

    public CreateNewGroup(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public CreateNewGroup(@NonNull Context context,String roundId) {
        super(context);
        this.context = context;
        this.roundId = roundId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = LayoutInflater.from(getContext()).inflate(R.layout.new_group_sheet,null);
        setContentView(root);

        team1Editor = (EditText)root.findViewById(R.id.editVideoId);
        team2Editor = (EditText)root.findViewById(R.id.edit_group_description);
        team3Editor = (EditText)root.findViewById(R.id.edit_group_number);


        Button confirmButton = (Button) root.findViewById(R.id.button_confirm);
        Button cancelButton = (Button) root.findViewById(R.id.button_cancel);

        if (roundId != null) {
            GroupItem groupItem = (GroupItem)Config.groupsMap.get(roundId);
            team3Editor.setText(groupItem.getNumberAsString());
            team2Editor.setText(groupItem.getDescription());
            team1Editor.setText(groupItem.getGroupName());
            confirmButton.setText("ערוך קבוצה");
        }

        confirmButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancel();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_confirm){
            String tempId =  team1Editor.getText().toString();
            if (tempId.length() > 0){
                cancel();
            }
            JSONArray params = new JSONArray();
            params.put(team1Editor.getText().toString());
            params.put(team2Editor.getText().toString());
            params.put(team3Editor.getText());
            params.put(Config.AdminStatus);
            JsonArrayRequest stringRequest;
            if (roundId == null) {
                 stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL + Config.ADD_NEW_GROUP, params, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        if (context != null) {
                            ((RefreshFromString) context).onStringDataRecieved(response, 0);
                            context = null;
                            cancel();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                    }
                });
            }else{
                params.put(roundId);
                 stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL + Config.EDIT_GROUP, params, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        if (context != null) {
                            ((RefreshFromString) context).onStringDataRecieved(response, 0);
                            context = null;
                            cancel();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                    }
                });
            }

            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "edit/add_new_group");
        }

        if (view.getId() == R.id.button_cancel){
            cancel();
        }
    }
}
