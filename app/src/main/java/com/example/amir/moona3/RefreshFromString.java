package com.example.amir.moona3;

import org.json.JSONArray;

/**
 * Created by Amir on 12/31/2016.
 */
public interface RefreshFromString {
    void onStringDataRecieved(JSONArray data, int type);
    void requestNewRoundData();
}
