package com.example.amir.moona3;

class TabDataUpdate {
    int tabId;

    public TabDataUpdate(int tabId) {
        this.tabId = tabId;
    }

    public int getTabId() {
        return tabId;
    }
}
