package com.example.amir.moona3;

class TimeToFourty {
    private String time;
    private String roundId;
    public TimeToFourty(String roundId,String time) {
        this.time = time;
        this.roundId = roundId;
    }

    public String getTime() {
        return time;
    }

    public String getRoundId() {
        return roundId;
    }
}
