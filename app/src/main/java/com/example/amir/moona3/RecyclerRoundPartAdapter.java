package com.example.amir.moona3;

import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import com.dd.CircularProgressButton;
import com.shawnlin.numberpicker.NumberPicker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Amir on 3/23/2018.
 */

public class RecyclerRoundPartAdapter extends RecyclerView.Adapter<RecyclerRoundPartAdapter.RoundItemViewHolder> implements ItemTouchHelperAdapter {
    private ArrayList<RoundPartItem> adapterData;
    public MainCronViewHolder mainCron;
    public CountDownTimer mainCountdownTimer1;
    private Handler handler;
    private String roundId;
    final private static int DELAY_SEND_UPDATE_REQUEST = 3000;

    private RoundPartItem workTeamPart;
    private boolean isRoundPuzzle;

    public RoundPartItem getWorkTeamPart() {
        return workTeamPart;
    }


    public RecyclerRoundPartAdapter(String id) {
        this.roundId = id;
    }

    @Override
    public RoundItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0: {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.round_main_timer, parent, false);


                mainCron = new MainCronViewHolder(rowView);
                mainCron.setIsRecyclable(false);
                if (Config.MainCronViewMap.get(roundId) != null) {
                    MainCronViewHolder oldViewer = Config.MainCronViewMap.get(roundId);
                    mainCron.mainCountdownTimer = oldViewer.mainCountdownTimer;
                    mainCron.isPaused = oldViewer.isPaused;
                    mainCron.currentMainTimerLeft = oldViewer.currentMainTimerLeft;
                    mainCron.roundId = oldViewer.roundId;
                    mainCron.recyclerRoundPartAdapter = oldViewer.recyclerRoundPartAdapter;
                }
                Config.MainCronViewMap.put(roundId, mainCron);

                return mainCron;
            }
            case 1: {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game1_round_part, parent, false);
                return new Game1ViewHolder(rowView);
            }
            case 2: {//TODO
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game2_round_part, parent, false);
                return new Game2ViewHolder(rowView);
            }
            case 3: { //TODO
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game3_round_part, parent, false);
                return new Game3ViewHolder(rowView);
            }
            case 4: {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game1_round_part, parent, false);
                return new Game1ViewHolder(rowView);
            }
            case 5: {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game1_round_part, parent, false);
                return new Game1ViewHolder(rowView);
            }
            case 6: {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game1_round_part, parent, false);
                return new Game1ViewHolder(rowView);
            }
            case 7: {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.community_round_part, parent, false);
                return new CommunityValueViewHolder(rowView);
            }
            case 8: { //TODO
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game11_round_part, parent, false);
                return new Game11ViewHolder(rowView);
            }
            case 9: { //TODO
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game12_round_part, parent, false);
                return new Game12ViewHolder(rowView);
            }
            case 10: { //TODO
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game13_round_part, parent, false);
                return new Game13ViewHolder(rowView);
            }
            case 11: { //TODO
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.game14_round_part, parent, false);
                return new Game14ViewHolder(rowView);
            }
            default: {
                View rowView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.default_round_part, parent, false);
                return new ChoosePartDataViewHolder(rowView);
            }
        }

    }


    @Override
    public int getItemViewType(int position) {
        RoundPartItem roundPartItem = adapterData.get(position);
        switch (roundPartItem.getPartDataType()) {
            case "main_timer": {
                return 0;
            }
            case "game1": {
                return 1;
            }
            case "game2": {
                return 2;
            }
            case "game3": {
                return 3;
            }
            case "copter": {
                return 4;
            }
            case "catcher": {
                return 5;
            }
            case "innovation": {
                return 6;
            }
            case "time": {
                return 10;
            }
            case "story": {
                return 6;
            }
            case "atmosphere": {
                return 6;
            }
            case "puzzle_diversity": {
                return 8;
            }
            case "mechanism_diversity": {
                return 8;
            }
            case "clues": {
                return 6;
            }
            case "learning_connection": {
                return 6;
            }
            case "effect": {
                return 6;
            }
            case "room_impression": {
                return 6;
            }
            case "excelled_puzzle": {
                return 11;
            }
            case "team_work": {
                return 9;
            }

            case "community_score": {
                return 7;
            }
            case "escape_package": {
                return 7;
            }
            default: {
                return -1;
            }
        }
    }


    private ArrayList<String> idsToUpdatePoints;

    public synchronized void updateChangedPoints(String groupId) {
        if (handler == null) {
            handler = new Handler();
        }
        if (idsToUpdatePoints == null) {
            idsToUpdatePoints = new ArrayList<>();
        }
        if (groupId != null && !idsToUpdatePoints.contains(groupId)) {
            idsToUpdatePoints.add(groupId);
        }

        handler.removeCallbacksAndMessages(null);
        Runnable myRunnable = new Runnable() {
            public void run() {
                for (String id : idsToUpdatePoints) {
                    EventBus.getDefault().post(new UpdatePointsForGroupIdEvent(id));
                }
                idsToUpdatePoints.clear();
            }
        };
        handler.postDelayed(myRunnable, 2000);
    }

    private String getGroupNameByRoundId(String round_id) {
        RoundItem ri = (RoundItem) Config.roundsMap.get(round_id);
        if (ri != null) {
            GroupItem gi = (GroupItem) Config.groupsMap.get(ri.getGroup());
            return gi.getGroupName();
        }
        return "";
    }


    private String getGroupNumByRoundId(String round_id) {
        RoundItem ri = (RoundItem) Config.roundsMap.get(round_id);
        if (ri != null) {
            GroupItem gi = (GroupItem) Config.groupsMap.get(ri.getGroup());
            return gi.getNumberAsString();
        }
        return "";
    }


    @Override
    public void onBindViewHolder(RoundItemViewHolder holder, int position) {
        RoundPartItem roundPartItem = adapterData.get(position);
        holder.setRoundPartItem(position);
        if (holder instanceof ChoosePartDataViewHolder) {
            ChoosePartDataViewHolder choosePartDataViewHolder = (ChoosePartDataViewHolder) holder;
        } else if (holder instanceof MainCronViewHolder) {
            Log.d("test", "onBindViewHolder: Maincron bound");
            MainCronViewHolder mainCronViewHolder = (MainCronViewHolder) holder;
            mainCronViewHolder.bindTime = System.currentTimeMillis();
            RoundItem ri = (RoundItem) Config.roundsMap.get(roundPartItem.getRound_id());
            /*
            if (ri != null) {
                if (ri.getProgress() == 1) {
                    mainCronViewHolder.setRoundDone(true);
                } else {
                    mainCronViewHolder.setRoundDone(false);
                }
            }
            */
            //String dataType = roundPartItem.getPartDataType();

            mainCron = mainCronViewHolder;
            mainCron.recyclerRoundPartAdapter = this;
            if (mainCron.mainCountdownTimer != null) {
                if (mainCron.currentMainTimerLeft != null) {
                    mainCron.mainCountdownTimer.timeLeft = mainCron.currentMainTimerLeft;
                }
                if (mainCron.cronometer != null) {
                    mainCron.mainCountdownTimer.cronometer = mainCron.cronometer;
                }
                if (mainCron.circularButton != null) {
                    mainCron.mainCountdownTimer.circularButton = mainCron.circularButton;
                }
            }
            //mainCron.groupNameTextView.setText("שם קבוצה מארחת: " + getGroupNameByRoundId(roundPartItem.getRound_id()) + " מס חדר" + getGroupNumByRoundId(roundPartItem.getRound_id()));
            roundReferee = ri.getRefereeName();
            mainCron.refereeNameEditText.setText(ri.getRefereeName());

            if (isRoundPuzzle){
                mainCron.groupSpinner.setVisibility(View.VISIBLE);
                mainCron.groupPlay.setVisibility(View.VISIBLE);
            }else{
                mainCron.groupSpinner.setVisibility(View.INVISIBLE);
                mainCron.groupPlay.setVisibility(View.INVISIBLE);
            }
            if (isRoundPuzzle && workTeamPart != null) {
                if (workTeamPart.getValue().has("group_id")) {
                    try {
                        mainCron.setSpinnerSelection(workTeamPart.getValue().getString("group_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (ri.getRoundTimeLeft() != null && ri.getRoundTimeLeft() > 0) {
                //mainCron.currentMainTimerLeft = ri.getRoundTimeLeft();
                mainCron.isPaused = true;
                mainCron.setProgressData(ri.getRoundTimeLeft());
            } else {
                mainCron.isPaused = false;
            }
            mainCron.setRoundId(ri.get_id());
        } else if (holder instanceof Game11ViewHolder) {
            Game11ViewHolder game11ViewHolder = (Game11ViewHolder) holder;
            String dataType = roundPartItem.getPartDataType();
            game11ViewHolder.onBindChecks();
            try {
                if (roundPartItem.getValue().has("point1")) {
                    game11ViewHolder.firstPointButton.setValue(roundPartItem.getValue().getInt("point1"));
                    game11ViewHolder.firstPointButton.setSelectedTextColor(Color.GREEN);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                if (roundPartItem.getValue().has("point2")) {
                    game11ViewHolder.secondPointButton.setValue(roundPartItem.getValue().getInt("point2"));
                    game11ViewHolder.secondPointButton.setSelectedTextColor(Color.GREEN);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            switch (dataType) {
                case "puzzle_diversity": {
                    game11ViewHolder.setTitle("הערכת חידות");
                    break;
                }
                case "mechanism_diversity": {
                    game11ViewHolder.setTitle("הערכת מנגנונים");
                    break;
                }
            }
            game11ViewHolder.setDescription1("ציון רמה (0 - 10)");
            game11ViewHolder.setDescription2("ציון גיוון (0 - 10)");

        } else if (holder instanceof Game12ViewHolder) {
            Game12ViewHolder game12ViewHolder = (Game12ViewHolder) holder;
            if (roundPartItem.getValue().has("point1")) {
                try {
                    game12ViewHolder.firstPointButton.setValue(roundPartItem.getValue().getInt("point1"));
                    game12ViewHolder.firstPointButton.setSelectedTextColor(Color.GREEN);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {
                GroupItem gi = (GroupItem)Config.groupsMap.get(roundPartItem.getValue().getString("group_id"));
                game12ViewHolder.groupSpinner.setText(gi.getGroupName());
            } catch (JSONException e) {
                e.printStackTrace();
            }




        } else if (holder instanceof Game13ViewHolder) {
            Game13ViewHolder game13ViewHolder = (Game13ViewHolder) holder;

            try {
                if (roundPartItem.getValue().has("entered")) {
                    Integer timeInSeconds = roundPartItem.getValue().getInt("entered");
                    Integer minutes = timeInSeconds/60;
                    Integer seconds = timeInSeconds - minutes*60;
                    String minutesAsString = minutes.toString();
                    if (minutesAsString.length() == 1){
                        minutesAsString = "0" + minutesAsString;
                    }
                    String secondsAsString = seconds.toString();
                    if (secondsAsString.length() == 1){
                        secondsAsString = "0" + secondsAsString;
                    }
                    game13ViewHolder.endTimeEditText.setText(minutesAsString + ":" + secondsAsString);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }



        } else if (holder instanceof Game14ViewHolder) {
            Game14ViewHolder game14ViewHolder = (Game14ViewHolder) holder;

            try {
                if (roundPartItem.getValue().has("puzzle_name")) {
                    game14ViewHolder.endTimeEditText.setText(roundPartItem.getValue().getString("puzzle_name"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            try {
                if (roundPartItem.getValue().has("entered")) {
                    game14ViewHolder.excelledPointButton.setValue(roundPartItem.getValue().getInt("entered"));
                    game14ViewHolder.excelledPointButton.setSelectedTextColor(Color.GREEN);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (holder instanceof Game1ViewHolder) {
            Game1ViewHolder game1ViewHolder = (Game1ViewHolder) holder;
            String dataType = roundPartItem.getPartDataType();
            game1ViewHolder.onBindChecks();
            switch (dataType) {
                case "game1": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                        }
                        game1ViewHolder.setTitle("משחק כדורים וסלים (0 - 7)");
                        game1ViewHolder.setDescription("כדורים שנכנסו בהצלחה");
                        if (roundPartItem.getValue().has("last_update_timestamp")) {
                            game1ViewHolder.setRoundLastStamp(roundPartItem.getValue().getLong("last_update_timestamp"));
                        }
                        if (roundPartItem.getValue().has("last_update_time_remain")) {
                            game1ViewHolder.setRoundTimer(roundPartItem.getValue().getLong("last_update_time_remain"));
                        }
                        if (Config.isUserAdmin()) {
                            game1ViewHolder.resetGameButton.setVisibility(View.VISIBLE);
                        } else {
                            game1ViewHolder.resetGameButton.setVisibility(View.GONE);
                        }
                        game1ViewHolder.bindTime = System.currentTimeMillis();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case "copter": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }

                        game1ViewHolder.setTitle("הערכת רחפן");
                        game1ViewHolder.setDescription("ציון רחפן (0 - 10)");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case "catcher": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                        }
                        game1ViewHolder.setTitle("הערכת מנגנון");
                        game1ViewHolder.setDescription("ציון מנגנון (0 - 10)");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case "innovation": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }
                        game1ViewHolder.setTitle("הערכת יצירתיות");
                        game1ViewHolder.setDescription("ציון יצירתיות (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);
                    }
                    break;
                }
                case "story": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }
                        game1ViewHolder.setTitle("סיפור");
                        game1ViewHolder.setDescription("ציון סיפור (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);

                    }
                    break;
                }
                case "atmosphere": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }
                        game1ViewHolder.setTitle("אווירה");
                        game1ViewHolder.setDescription("ציון אווירה (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);

                    }
                    break;
                }
                case "clues": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }
                        game1ViewHolder.setTitle("הכוונה ורמזים");
                        game1ViewHolder.setDescription("ציון הכוונה ורמזים (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);

                    }
                    break;
                }
                case "learning_connection": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }

                        game1ViewHolder.setTitle("חיבור לחומר לימוד");
                        game1ViewHolder.setDescription("ציון חיבור לחומר לימוד (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);

                    }
                    break;
                }
                case "effect": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }
                        game1ViewHolder.setTitle("אפקט");
                        game1ViewHolder.setDescription("ציון אפקט (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);
                    }
                    break;
                }
                case "room_impression": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }
                        game1ViewHolder.setTitle("התרשמות כללית על החדר");
                        game1ViewHolder.setDescription("ציון התרשמות כללית על החדר (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);

                    }
                    break;
                }
                case "excelled_puzzle": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }
                        game1ViewHolder.setTitle("חידה מצטיינת");
                        game1ViewHolder.setDescription("ציון חידה מצטיינת (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);

                    }
                    break;
                }
                case "team_work": {
                    try {
                        if (roundPartItem.getValue().has("entered")) {
                            game1ViewHolder.ballsInBaskethandlerButton.setValue(Integer.parseInt(roundPartItem.getValue().getString("entered")));
                            game1ViewHolder.ballsInBaskethandlerButton.setSelectedTextColor(Color.GREEN);
                        }
                        game1ViewHolder.setTitle("צוות מפצח");
                        game1ViewHolder.setDescription("ציון צוות מפצח (0 - 10)");
                    } catch (JSONException e) {
                        game1ViewHolder.ballsInBaskethandlerButton.setValue(0);

                    }
                    break;
                }


            }
        } else if (holder instanceof Game3ViewHolder) {
            Game3ViewHolder game3ViewHolder = (Game3ViewHolder) holder;
            try {
                if (roundPartItem.getValue().has("volts")) {
                    game3ViewHolder.ballsInBaskethandlerButton.setText(roundPartItem.getValue().getString("volts"));
                }
                if (roundPartItem.getValue().has("last_update_timestamp")) {
                    game3ViewHolder.setRoundLastStamp(roundPartItem.getValue().getLong("last_update_timestamp"));
                }
                if (roundPartItem.getValue().has("last_update_time_remain")) {
                    game3ViewHolder.setRoundTimer(roundPartItem.getValue().getLong("last_update_time_remain"));
                }
                game3ViewHolder.setProgressData();
                if (Config.isUserAdmin()) {
                    game3ViewHolder.resetGameButton.setVisibility(View.VISIBLE);
                } else {
                    game3ViewHolder.resetGameButton.setVisibility(View.GONE);
                }
                game3ViewHolder.bindTime = System.currentTimeMillis();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (holder instanceof CommunityValueViewHolder) {
            CommunityValueViewHolder communityValueViewHolder = (CommunityValueViewHolder) holder;
            String dataType = roundPartItem.getPartDataType();
            try {
                if (roundPartItem.getValue().has("score1")) {
                    communityValueViewHolder.firstScoreEditText.setText(roundPartItem.getValue().getString("score1"));
                }
                if (roundPartItem.getValue().has("score2")) {
                    communityValueViewHolder.secondScoreEditText.setText(roundPartItem.getValue().getString("score2"));
                }
                if (roundPartItem.getValue().has("score3")) {
                    communityValueViewHolder.thirdScoreEditText.setText(roundPartItem.getValue().getString("score3"));
                }
                if (roundPartItem.getValue().has("milestone_score")) {
                    communityValueViewHolder.milestoneEditBox.setText(roundPartItem.getValue().getString("milestone_score"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            switch (dataType) {
                case "community_score": {
                    communityValueViewHolder.setTitle("מעורבות חברתית");
                    break;
                }
                case "escape_package": {
                    communityValueViewHolder.setTitle("ערכת הבריחה");
                    break;
                }
            }
            //if (Config.isUserAdmin()) {
                communityValueViewHolder.milestoneEditBox.setVisibility(View.VISIBLE);
                communityValueViewHolder.description2TextView.setVisibility(View.VISIBLE);
            //} else {
            //   communityValueViewHolder.milestoneEditBox.setVisibility(View.INVISIBLE);
            //   communityValueViewHolder.description2TextView.setVisibility(View.INVISIBLE);
            //}
        } else if (holder instanceof Game2ViewHolder) {
            Game2ViewHolder game2ViewHolder = (Game2ViewHolder) holder;
            Integer currentPoints = 0;
            try {
                if (roundPartItem.getValue().has("big_ball")) {
                    currentPoints += roundPartItem.getValue().getInt("big_ball") * 2;
                    game2ViewHolder.bigBall.setSelection(roundPartItem.getValue().getInt("big_ball"), true, true);
                }
                if (roundPartItem.getValue().has("big_cube")) {
                    currentPoints += roundPartItem.getValue().getInt("big_cube") * 5;
                    game2ViewHolder.bigCube.setSelection(roundPartItem.getValue().getInt("big_cube"), true, true);
                }
                if (roundPartItem.getValue().has("big_pyramid")) {
                    currentPoints += roundPartItem.getValue().getInt("big_pyramid") * 4;
                    game2ViewHolder.bigPyramid.setSelection(roundPartItem.getValue().getInt("big_pyramid"), true, true);
                }


                if (roundPartItem.getValue().has("medium_ball")) {
                    currentPoints += roundPartItem.getValue().getInt("medium_ball") * 1;
                    game2ViewHolder.mediumBall.setSelection(roundPartItem.getValue().getInt("medium_ball"), true, true);
                }
                if (roundPartItem.getValue().has("medium_cube")) {
                    currentPoints += roundPartItem.getValue().getInt("medium_cube") * 2;
                    game2ViewHolder.mediumCube.setSelection(roundPartItem.getValue().getInt("medium_cube"), true, true);
                }
                if (roundPartItem.getValue().has("medium_pyramid")) {
                    currentPoints += roundPartItem.getValue().getInt("medium_pyramid") * 3;
                    game2ViewHolder.mediumPyramid.setSelection(roundPartItem.getValue().getInt("medium_pyramid"), true, true);
                }


                if (roundPartItem.getValue().has("small_ball")) {
                    currentPoints += roundPartItem.getValue().getInt("small_ball") * 3;
                    game2ViewHolder.smallBall.setSelection(roundPartItem.getValue().getInt("small_ball"), true, true);
                }
                if (roundPartItem.getValue().has("small_cube")) {
                    currentPoints += roundPartItem.getValue().getInt("small_cube") * 4;
                    game2ViewHolder.smallCube.setSelection(roundPartItem.getValue().getInt("small_cube"), true, true);
                }
                if (roundPartItem.getValue().has("small_pyramid")) {
                    currentPoints += roundPartItem.getValue().getInt("small_pyramid") * 5;
                    game2ViewHolder.smallPyramid.setSelection(roundPartItem.getValue().getInt("small_pyramid"), true, true);
                }

                if (roundPartItem.getValue().has("req_points")) {
                    game2ViewHolder.requiredPointsSpinner.setSelection(roundPartItem.getValue().getInt("req_points") - 5, true, true);
                }

                if (roundPartItem.getValue().has("last_update_timestamp")) {
                    game2ViewHolder.setRoundLastStamp(roundPartItem.getValue().getLong("last_update_timestamp"));
                }
                if (roundPartItem.getValue().has("last_update_time_remain")) {
                    game2ViewHolder.setRoundTimer(roundPartItem.getValue().getLong("last_update_time_remain"));
                }
                //game2ViewHolder.timerButton.setProgress(0);
                game2ViewHolder.currentPoints.setText(currentPoints.toString());
                game2ViewHolder.setProgressData();
                if (Config.isUserAdmin()) {
                    game2ViewHolder.resetGameButton.setVisibility(View.VISIBLE);
                } else {
                    game2ViewHolder.resetGameButton.setVisibility(View.GONE);
                }
                game2ViewHolder.bindTime = System.currentTimeMillis();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        if (isRoundDone(roundPartItem.getRound_id())) {
            holder.onRoundMarkedDone();
        } else {
            if (holder instanceof MainCronViewHolder && isRoundRealDone(roundPartItem.getRound_id())) {
                holder.onRoundMarkedDone();
            } else
                holder.onRoundMarkedNotDone();
        }

    }


    public boolean isRoundDone(String roundId) {
        if (Config.isUserAdmin()) {
            return false;
        }
        RoundItem ri = (RoundItem) Config.roundsMap.get(roundId);
        if (ri.getProgress() == 1) {
            return true;
        }


        if (Config.isUserObserver()) {
            return true;
        }

        return false;
    }

    public boolean isRoundRealDone(String roundId) {
        RoundItem ri = (RoundItem) Config.roundsMap.get(roundId);
        if (ri.getProgress() == 1) {
            return true;
        }
        return false;
    }

    @Override
    public int getItemCount() {
        if (adapterData == null) return 0;
        return adapterData.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int position) {

    }

    public void setTeamCrackerPart(RoundPartItem workTeamPart){
        this.workTeamPart = workTeamPart;

    }

    public void setData(ArrayList<RoundPartItem> adapterData) {
        this.adapterData = adapterData;
        notifyDataSetChanged();
    }

    public ArrayList<RoundPartItem> getData() {
        return this.adapterData;
    }

    public String isRoundCanBeMarkedDone(){
        boolean evertythingIsFine = true;
        for (int i = 0; i < adapterData.size(); i++) {
            RoundPartItem rpi = adapterData.get(i);
            evertythingIsFine = rpi.isProgressed();

            if (!evertythingIsFine && !rpi.getPartDataType().equals("excelled_puzzle")){
                return getRoundPartRealName(rpi.getPartDataType());
            }
        }
        return "";
    }

    private String getRoundPartRealName(String partDataType) {
        switch (partDataType){
            case "unk":{
                return "משחק לא מוגדר";
            }
            case "game1":{
                return "משחק כדורים וסלים";
            }
            case "game2":{
                return "משחק גופים גאומטריים";
            }
            case "game3":{
                return "משחק גנרטור";
            }
            case "copter":{
                return "הערכת רחפן";
            }
            case "innovation":{
                return "הערכת יצירתיות";
            }
            case "catcher":{
                return "הערכת מנגנון";
            }
            case "community_score":{
                return "משחק חברתי";
            }

            case "time":{
                return "הערכת זמן";
            }
            case "story":{
                return "הערכת סיפור";
            }
            case "atmosphere":{
                return "הערכת אווירה";
            }
            case "puzzle_diversity":{
                return "הערכת חידות";
            }
            case "mechanism_diversity":{
                return "הערכת מנגנונים";
            }
            case "clues":{
                return "הערכת הכוונה ורמזים";
            }
            case "learning_connection":{
                return "הערכת חיבור ללימודים";
            }
            case "effect":{
                return "הערכת אפקט";
            }
            case "room_impression":{
                return "הערכת התרשמות";
            }
            case "excelled_puzzle":{
                return "הערכת חידה מצטיינת";
            }
            case "team_work":{
                return "הערכת צוות מפצח";
            }
            case "escape_package":{
                return "הערכת ערכת הבריחה";
            }

        }
        return "";
    }

    public void setIsRoundPuzzle(boolean isRoundPuzzle) {
        this.isRoundPuzzle = isRoundPuzzle;
    }

    abstract class RoundItemViewHolder extends RecyclerView.ViewHolder{
        private Integer roundPartItemPos;
        public RoundItemViewHolder(View itemView) {
            super(itemView);
        }

        public RoundPartItem getRoundPartItem() {
            if (roundPartItemPos == null){
                return null;
            }
            return adapterData.get(roundPartItemPos);
        }

        public void setRoundPartItem(int roundPartItem) {
            this.roundPartItemPos = roundPartItem;
        }


        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onRoundDataChange(UpdateRoundDelegate event){

            if (roundPartItemPos != null && getRoundPartItem() != null){
                if (Config.isUserAdmin()){
                    return;
                }
                if (event.getDocumentID().equals(getRoundPartItem().getRound_id())) {
                    RoundItem round = (RoundItem) Config.roundsMap.get(getRoundPartItem().getRound_id());
                    if (round != null) {
                        if (round.getProgress() == 1 ){
                            onRoundMarkedDone();
                        }else if (event.getUpdatedValuesJson().has("progress")){
                            try {
                                if (event.getUpdatedValuesJson().getInt("progress") == 1){
                                    onRoundMarkedDone();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
            }
        }
        abstract void onRoundMarkedDone();
        abstract void onRoundMarkedNotDone();
        abstract void onDetachHolder();
    }

    @Override
    public void onViewAttachedToWindow(RecyclerRoundPartAdapter.RoundItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (!EventBus.getDefault().isRegistered(holder)) {
            EventBus.getDefault().register(holder);
        }
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerRoundPartAdapter.RoundItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (EventBus.getDefault().isRegistered(holder)) {
            EventBus.getDefault().unregister(holder);
        }
    }

    private String roundReferee;
    class MainCronViewHolder extends RoundItemViewHolder{
        private final Button doneButton;
        private final RelativeLayout layout;
        private final Integer hostIndex;
        private TextView cronometer;
        private CircularProgressButton circularButton;
        private MainCountDownTimer mainCountdownTimer;
        public EditText refereeNameEditText;
        private String roundId;
        private Long currentMainTimerLeft;
        private boolean isPaused = false;
        public RecyclerRoundPartAdapter recyclerRoundPartAdapter;
        public Long bindTime;
        private ImprovedSpinner groupSpinner;
        public TextView groupPlay;

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onRoundDataChange(UpdateRoundDelegate event){
            if (event.getDocumentID().equals(getRoundPartItem().getRound_id())) {
                RoundItem ri = (RoundItem)Config.roundsMap.get(event.getDocumentID());
                if (ri != null) {
                    if (ri.getProgress() == 1) {
                        setRoundDone(true);
                    }else{
                        setRoundDone(false);
                    }
                }
            }
        }

        private Integer getGroupIndexById(String groupId) {
            ArrayList<RecyclerItem> arr = new ArrayList<>();
            arr.addAll(Config.groupsMap.values());
            Collections.sort(arr);
            for (int i = 0; i < arr.size(); i++) {
                GroupItem temp = (GroupItem)arr.get(i);
                if (temp.get_id().equals(groupId)){
                    return i;
                }
            }
            return -1;
        }


        private void setProgressData(Long timeLeft){
            if (timeLeft != null){

                currentMainTimerLeft = timeLeft;
                int left = 0;
                try {
                    left = (int)((currentMainTimerLeft*100)/(1000*60*getRoundPartItem().getValue().getInt("main_timer")));

                    if (left== 100){
                        left = 99;
                    }
                    if (left <= 0) {
                        if (mainCountdownTimer != null){
                            mainCountdownTimer.onFinish();
                            mainCountdownTimer.cancel();
                            mainCountdownTimer = null;
                        }
                        if (mainCountdownTimer1 != null){
                            mainCountdownTimer1.cancel();
                            mainCountdownTimer1 = null;
                        }
                    }else {
                        if (mainCountdownTimer != null){
                            mainCountdownTimer.cancel();
                            mainCountdownTimer = null;
                        }
                        if (mainCountdownTimer1 != null){
                            mainCountdownTimer1.cancel();
                            mainCountdownTimer1 = null;
                        }


                        circularButton.setProgress(left);
                        circularButton.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                circularButton.pauseProgress();
                                if (cronometer != null) {
                                    long second = (currentMainTimerLeft / 1000) % 60;
                                    long minute = (currentMainTimerLeft / (1000 * 60)) % 60;
                                    String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                                    cronometer.setText(time);
                                }


                            }
                        },1000);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        void onRoundMarkedDone() {
            doneButton.setText("סיבוב כבר הסתיים");
            setRoundDone(true);
            doneButton.setEnabled(false);
            groupSpinner.setEnabled(false);
            if (Config.isUserObserver())
                circularButton.setVisibility(View.INVISIBLE);
        }
        @Override
        void onRoundMarkedNotDone() {
            doneButton.setText("סיום סיבוב");
            setRoundDone(false);
            doneButton.setEnabled(true);
            groupSpinner.setEnabled(true);
        }


        @Override
        void onDetachHolder() {

        }

        public void setRoundDone(boolean isTrue){
            if (isTrue){
                layout.setBackground(itemView.getResources().getDrawable(R.drawable.color8));
            }else{
                layout.setBackground(itemView.getResources().getDrawable(R.drawable.color2));
            }
        }

        public CircularProgressButton getCircularButton(){
            return circularButton;
        }

        public void setSpinnerSelection(String spinnerSelection) {
            if (spinnerSelection == null){
                return;
            }
            groupSpinner.setSelection(getGroupIndexById(spinnerSelection), true, true);
        }


        public MainCronViewHolder(View itemView) {
            super(itemView);

            cronometer = (TextView)itemView.findViewById(R.id.crono_timer_text);
            groupPlay = (TextView)itemView.findViewById(R.id.group_play);





            doneButton = (Button)itemView.findViewById(R.id.mark_finish_round_button);
            final Button pauseButton = (Button)itemView.findViewById(R.id.pause_round_button);
            pauseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bindTime + 5000 < System.currentTimeMillis()) {
                        if (recyclerRoundPartAdapter == null) {
                            return;
                        }
                        if (recyclerRoundPartAdapter.mainCron == null) {
                            return;
                        }
                        if (!recyclerRoundPartAdapter.mainCron.isPaused && recyclerRoundPartAdapter.mainCron.mainCountdownTimer == null) {
                            return;
                        }
                        if (recyclerRoundPartAdapter.mainCron.isPaused && recyclerRoundPartAdapter.mainCron.currentMainTimerLeft == null) {
                            return;
                        }

                        pauseButton.setEnabled(false);
                        if (recyclerRoundPartAdapter.mainCron.isPaused) {
                            recyclerRoundPartAdapter.mainCron.circularButton.resumeProgress();
                            recyclerRoundPartAdapter.mainCron.isPaused = false;
                            recyclerRoundPartAdapter.mainCron.setRoundPauseStatus(false);
                            recyclerRoundPartAdapter.mainCron.roundScanStartCounter(recyclerRoundPartAdapter.mainCron.currentMainTimerLeft.intValue());
                        } else if (recyclerRoundPartAdapter.mainCron.mainCountdownTimer.timeLeft != null) {
                            recyclerRoundPartAdapter.mainCron.isPaused = true;
                            recyclerRoundPartAdapter.mainCron.setRoundPauseStatus(true);
                            recyclerRoundPartAdapter.mainCron.circularButton.pauseProgress();
                            if (recyclerRoundPartAdapter.mainCron.mainCountdownTimer != null) {
                                recyclerRoundPartAdapter.mainCron.currentMainTimerLeft = recyclerRoundPartAdapter.mainCron.mainCountdownTimer.timeLeft;
                                recyclerRoundPartAdapter.mainCron.mainCountdownTimer.cancel();
                                recyclerRoundPartAdapter.mainCron.mainCountdownTimer = null;
                            }

                            if (mainCountdownTimer1 != null) {
                                mainCountdownTimer1.cancel();
                                mainCountdownTimer1 = null;
                            }

                        }
                        pauseButton.setEnabled(true);
                    }
                }
            });


            layout = (RelativeLayout)itemView.findViewById(R.id.layout_for_bg);
            refereeNameEditText = (EditText) itemView.findViewById(R.id.referee_name);
            refereeNameEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            refereeNameEditText.addTextChangedListener( new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                    Log.d("tttt", "afterTextChanged: " +s.toString());
                    JSONArray params = new JSONArray();

                    params.put(roundId);
                    params.put(Config.AdminStatus);
                    params.put(s.toString());

                    JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_REF,params,new Response.Listener<JSONArray>() {

                            @Override
                            public void onResponse(JSONArray response) {

                                try {
                                    String tempref = response.getString(0);
                                    if (tempref != null){
                                        roundReferee = tempref;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                            }
                        });
                        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateRoundReferee");
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            doneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (roundReferee == null || roundReferee.isEmpty()){
                        Toast.makeText(mainCron.getCircularButton().getContext(), " שם שופט " + mainCron.getCircularButton().getContext().getResources().getString(R.string.score_missing),Toast.LENGTH_LONG).show();
                        return;
                    }
                    String canBeDone = isRoundCanBeMarkedDone();
                    if (canBeDone.length()==0) {
                        JSONArray params = new JSONArray();
                        params.put(getRoundPartItem().getRound_id());
                        params.put(1);


                        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL + Config.MARK_ROUND_DONE, params, new Response.Listener<JSONArray>() {

                            @Override
                            public void onResponse(JSONArray response) {

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                            }
                        });
                        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "markDone");
                    }else{
                        Toast.makeText(mainCron.getCircularButton().getContext(), canBeDone + " " + mainCron.getCircularButton().getContext().getResources().getString(R.string.score_missing),Toast.LENGTH_LONG).show();

                    }
                }
            });
            circularButton = (CircularProgressButton)itemView.findViewById(R.id.circular_timer_button_start);


            String groupId = getGroupIdFromRoundId(getRoundId());
            hostIndex = getGroupIndexById(groupId);
            groupSpinner = (ImprovedSpinner) itemView.findViewById(R.id.crack_group);
            groupSpinner.setAdapter( getAdapterForSpinner(Config.groupsMap));
            groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (workTeamPart != null) {
                        updateCrackerPoints(null, ((GroupItem) (groupSpinner.getSelectedItem())).get_id());
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });



            if (Config.isUserObserver())
                circularButton.setVisibility(View.INVISIBLE);

            circularButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (circularButton.getProgress()==0) {
                        try {
                            //circularButton.setClickable(false);
                            roundScanStartCounter(1000 * 60 * getRoundPartItem().getValue().getInt("main_timer"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        if (mainCron.isAllowedChangeTime){
                            mainCron.isAllowedChangeTime = false;
                            circularButton.pauseProgress();
                        }else{
                            mainCron.isAllowedChangeTime = true;
                            circularButton.resumeProgress();
                        }
                    }
                }
            });


        }

        public boolean isAllowedChangeTime = true;

        private ArrayAdapter<RecyclerItem> getAdapterForSpinner(HashMap<String,RecyclerItem> recyclerItems){
            ArrayList<RecyclerItem> arr = new ArrayList<>();
            arr.addAll(recyclerItems.values());
            Collections.sort(arr);
            ArrayAdapter<RecyclerItem> spinnerArrayAdapter = new ArrayAdapter<RecyclerItem>(itemView.getContext(), android.R.layout.simple_spinner_item, arr ){

                @Override
                public boolean isEnabled(int position) {
                    // TODO Auto-generated method stub
                    if (hostIndex == position) {
                        return false;
                    }
                    return true;
                }
                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View mView = super.getDropDownView(position, convertView, parent);
                    TextView mTextView = (TextView) mView;
                    if (hostIndex == position) {
                        mTextView.setTextColor(Color.GRAY);
                    } else {
                        mTextView.setTextColor(Color.WHITE);
                    }
                    return mView;
                }
            };
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            return spinnerArrayAdapter;
        }

        private void setRoundPauseStatus(boolean setPause){
            if (mainCron.currentMainTimerLeft != null) {
                JSONArray params = new JSONArray();
                params.put(mainCron.getRoundPartItem().getRound_id());
                params.put(Config.AdminStatus);
                if (!setPause){
                    params.put(-1);
                }else {
                    params.put(mainCron.currentMainTimerLeft);
                }

                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL + Config.SET_ROUND_TIME_LEFT, params, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "markDone");
            //}else{
            //    Toast.makeText(doneButton.getContext(), "הסיבוב איננו ",Toast.LENGTH_LONG).show();
            }
        }

        private void roundScanStartCounter(final int kkk){
            if (mainCron.mainCountdownTimer != null){
                mainCron.mainCountdownTimer.cancel();
                mainCron.mainCountdownTimer = null;
            }
            if (mainCountdownTimer1 != null){
                mainCountdownTimer1.cancel();
                mainCountdownTimer1 = null;
            }
            recyclerRoundPartAdapter.mainCron.mainCountdownTimer = new MainCountDownTimer(kkk, 1000) {


                public void onTick(long millisUntilFinished) {
                    timeLeft = millisUntilFinished;
                    Log.d("dddd", "onTick: " + millisUntilFinished);
                    /*
                    if (mainCron == null){
                        mainCountdownTimer.cancel();
                        mainCountdownTimer = null;
                    }
                    */
                    if (cronometer != null) {
                        long second = (millisUntilFinished / 1000) % 60;
                        long minute = (millisUntilFinished / (1000 * 60)) % 60;
                        String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                        cronometer.setText(time);
                    }

                    long fromFourty = 2400000-millisUntilFinished;
                    long second2 = (fromFourty / 1000) % 60;
                    long minute2 = (fromFourty / (1000 * 60)) % 60;
                    String time2 = String.format(Locale.ENGLISH,"%02d:%02d", minute2, second2);
                    if (isAllowedChangeTime) {
                        EventBus.getDefault().post(new TimeToFourty(roundId, time2));
                    }
                    int left = (int)(millisUntilFinished*100/kkk);
                    if (left== 100){
                        left = 99;
                    }
                    circularButton.setProgress(left);
                }

                public void onFinish() {
                    if (cronometer != null){
                        cronometer.setText("");
                    }
                    circularButton.setProgress(100);
                    Toast.makeText(circularButton.getContext(), R.string.main_round_timer_done,Toast.LENGTH_LONG).show();
                    Vibrator v = (Vibrator) circularButton.getContext().getSystemService(circularButton.getContext().VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    }else{
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                }
            };
            recyclerRoundPartAdapter.mainCountdownTimer1 = mainCron.mainCountdownTimer;
            mainCron.mainCountdownTimer.circularButton = circularButton;
            mainCron.mainCountdownTimer.cronometer = cronometer;
            mainCron.mainCountdownTimer.timeLeft = currentMainTimerLeft;
            recyclerRoundPartAdapter.mainCron.mainCountdownTimer.start();

        }

        public void setRoundId(String roundId) {
            this.roundId = roundId;
        }

        public String getRoundId() {
            if (getRoundPartItem() != null && getRoundPartItem().getRound_id() != null) {
                return getRoundPartItem().getRound_id();
            }
            return RecyclerRoundPartAdapter.this.roundId;
        }
    }



    class CommunityValueViewHolder extends RoundItemViewHolder{
        private TextView descriptionTextView;
        private TextView titleTextView;
        private Button updateVoltageButton;

        private EditText firstScoreEditText;
        private EditText secondScoreEditText;
        private EditText thirdScoreEditText;

        public EditText milestoneEditBox;
        public TextView description2TextView;

        @Override
        void onRoundMarkedDone() {
            firstScoreEditText.setFocusable(false);
            secondScoreEditText.setFocusable(false);
            thirdScoreEditText.setFocusable(false);
            milestoneEditBox.setFocusable(false);
        }

        @Override
        void onRoundMarkedNotDone() {
            firstScoreEditText.setFocusable(true);
            secondScoreEditText.setFocusable(true);
            thirdScoreEditText.setFocusable(true);
            milestoneEditBox.setFocusable(true);
        }


        @Override
        void onDetachHolder() {

        }

        public void setTitle(String title){
            titleTextView.setText(title);
        }

        public void setDescription(String title){
            descriptionTextView.setText(title);
        }

        public CommunityValueViewHolder(final View itemView) {
            super(itemView);

            descriptionTextView = (TextView)itemView.findViewById(R.id.point_description);
            description2TextView = (TextView)itemView.findViewById(R.id.point_description2);

            titleTextView = (TextView)itemView.findViewById(R.id.title);
            firstScoreEditText = (EditText)itemView.findViewById(R.id.community_score_editbox);
            secondScoreEditText = (EditText)itemView.findViewById(R.id.community_score_editbox2);
            thirdScoreEditText = (EditText)itemView.findViewById(R.id.community_score_editbox3);

            milestoneEditBox = (EditText)itemView.findViewById(R.id.milestone_score_editbox);

            updateVoltageButton = (Button)itemView.findViewById(R.id.button);
            updateVoltageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    double score1 = -1.0;
                    double score2 = -1.0;
                    double score3 = -1.0;

                    double mile = 0.0;
                    try {
                        score1 = Double.parseDouble(firstScoreEditText.getText().toString());
                    }catch (NumberFormatException e){

                    }
                    try {
                        score2 = Double.parseDouble(secondScoreEditText.getText().toString());
                    }catch (NumberFormatException e){

                    }
                    try {
                        score3 = Double.parseDouble(thirdScoreEditText.getText().toString());
                    }catch (NumberFormatException e){

                    }


                    try {
                        mile = Double.parseDouble(milestoneEditBox.getText().toString());
                    }catch (NumberFormatException e){

                    }
                    /*
                    if (score1 < 0 || score1 > 100){
                        Toast.makeText(itemView.getContext(),"ציונים צריכים להיות בין 0 ל 100",Toast.LENGTH_LONG);
                        return;
                    }
                    if (score2 < 0 || score2 > 100){
                        Toast.makeText(itemView.getContext(),"ציונים צריכים להיות בין 0 ל 100",Toast.LENGTH_LONG);
                        return;
                    }
                    if (score3 < 0 || score3 > 100){
                        Toast.makeText(itemView.getContext(),"ציונים צריכים להיות בין 0 ל 100",Toast.LENGTH_LONG);
                        return;
                    }
                    */
                    if (score1 == -1.0 && score2 == -1.0 &&  score3 == -1.0){
                        Toast.makeText(itemView.getContext(),"יש צורך לתת ציון אחד לפחות מ 3",Toast.LENGTH_LONG);
                        return;
                    }

                    if (mile < 0 || mile > 100){
                        Toast.makeText(itemView.getContext(),"ציונים צריכים להיות בין 0 ל 100",Toast.LENGTH_LONG);
                        return;
                    }
                    updateCommunityScorePoints(score1,score2,score3, mile);

                }
            });
        }

        private void updateCommunityScorePoints(double newValue1, double newValue2, double newValue3, double milestone) {
            JSONArray params = new JSONArray();
            int progress = 1;
            if (newValue1 == -1.0 || newValue2 == -1.0 || newValue3 == -1.0) {
                progress = 0;
            }

            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {

                data.put("score1",newValue1);
                data.put("score2",newValue2);
                data.put("score3",newValue3);
                if (newValue1 == -1.0 ) {
                    newValue1 = 0.0;
                }
                if (newValue2 == -1.0 ) {
                    newValue2 = 0.0;
                }
                if (newValue3 == -1.0 ) {
                    newValue3 = 0.0;
                }
                double total = (newValue1 + newValue2 + newValue3)/3;
                data.put("score",total);
                data.put("milestone_score",milestone);
                data.put("progressed", progress);

                params.put(data);

                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateCommunityScorePoints");
                updateChangedPoints(getGroupIdFromRoundId(getRoundPartItem().getRound_id()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void resetGame(String roundPartId){
        JSONArray params = new JSONArray();

        params.put(roundPartId);
        params.put(Config.AdminStatus);
        params.put(roundId);
            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.RESET_ROUND_PART,params,new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                }
            });
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "resetGame");
    }


    class Game1ViewHolder extends RoundItemViewHolder{

        private final EditText lastEntryTimeEditText;
        private TextView descriptionTextView;
        private TextView titleTextView;
        private TextView lastHitTextView;
        Button resetGameButton;
        private TextView cronometer;
        private CircularProgressButton timerButton;
        private CountDownTimer mainCountdownTimer;

        private NumberPicker ballsInBaskethandlerButton;


        final private Integer ROUND_TIME = 2;
        private Long startTime = null;
        private Long timeLeft;
        private Long roundLastStamp;
        private Long cmillisUntilFinished;
        private Long rmillisUntilFinished;
        Long bindTime;

        @Override
        void onRoundMarkedDone() {
            ballsInBaskethandlerButton.setEnabled(false);
        }

        @Override
        void onRoundMarkedNotDone() {
            ballsInBaskethandlerButton.setEnabled(true);
        }

        @Override
        void onDetachHolder() {
            if (mainCountdownTimer != null){
                mainCountdownTimer.cancel();
                mainCountdownTimer = null;
            }
        }


        public void setRoundTimer(Long time){
            if ( rmillisUntilFinished == null ) {
                if (roundLastStamp + time > System.currentTimeMillis()) {
                    timeLeft = time;
                    startTime = time;
                    //timerButton.setClickable(false);
                    roundScanStartCounter((int) (startTime - getDiffFromLastStampToNow()));
                } else {
                /*
                if (timerButton.getProgress() == 0 || timerButton.getProgress() == 100) {
                    timerButton.setProgress(-1);
                    timerButton.setClickable(true);
                }
                */
                }
            }
            lastEntryTimeEditText.setVisibility(View.VISIBLE);
            long second = (time / 1000) % 60;
            long minute = (time / (1000 * 60)) % 60;
            String timeLeft = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
            lastEntryTimeEditText.setText(timeLeft);
            lastHitTextView.setVisibility(View.VISIBLE);
        }

        public void setRoundLastStamp(long roundLastStamp) {
            this.roundLastStamp = roundLastStamp;
        }


        private Long getDiffFromLastStampToNow(){
            if (roundLastStamp != null) {
                return System.currentTimeMillis() - roundLastStamp;
            }
            return (long)0;
        }

        public void setTitle(String title){
            titleTextView.setText(title);
        }

        public void setDescription(String title){
            descriptionTextView.setText(title);
        }

        public void onBindChecks(){
            if (getRoundPartItem().getPartDataType().equals("game1") && !Config.isUserObserver()){
                cronometer.setVisibility(View.VISIBLE);
                timerButton.setVisibility(View.VISIBLE);
            }else{
                cronometer.setVisibility(View.GONE);
                timerButton.setVisibility(View.GONE);
            }
            setProgressData();
        }

        private void setProgressData(){
            Long timeLeft = getRoundPartItem().getPartTimeLeft();
            if (timeLeft != null){

                rmillisUntilFinished = timeLeft;
                int left =  (int)((rmillisUntilFinished*100)/(1000*60*ROUND_TIME));
                if (left== 100){
                    left = 99;
                }
                if (left <= 0) {
                    mainCountdownTimer.onFinish();
                    mainCountdownTimer.cancel();
                }else {
                    timerButton.setProgress(left);
                    timerButton.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            timerButton.pauseProgress();
                            if (cronometer != null) {
                                long second = (rmillisUntilFinished / 1000) % 60;
                                long minute = (rmillisUntilFinished / (1000 * 60)) % 60;
                                String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                                cronometer.setText(time);
                            }
                        }
                    },1000);

                }

                if (mainCountdownTimer != null){
                    mainCountdownTimer.cancel();
                    mainCountdownTimer = null;
                }
            }
        }



        public Game1ViewHolder(View itemView) {
            super(itemView);

            descriptionTextView = (TextView)itemView.findViewById(R.id.point_description);
            titleTextView = (TextView)itemView.findViewById(R.id.title);

            lastEntryTimeEditText = (EditText)itemView.findViewById(R.id.last_catch_timer);
            lastHitTextView = (TextView)itemView.findViewById(R.id.last_hit_text);


            resetGameButton = (Button)itemView.findViewById(R.id.change_game_button);
            resetGameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetGame(getRoundPartItem().get_id());
                }
            });
            cronometer = (TextView)itemView.findViewById(R.id.crono_timer_text);
            timerButton = (CircularProgressButton)itemView.findViewById(R.id.circular_timer_button_start);
            timerButton.setProgress(0);

            timerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bindTime + 5000 < System.currentTimeMillis()) {
                        if (timerButton.getProgress() <= 0 || timerButton.getProgress() == 100) {
                            //timerButton.setClickable(false);
                            timerButton.setProgress(0);
                            roundScanStartCounter(1000 * 60 * ROUND_TIME);
                        } else {
                            if (rmillisUntilFinished != null) {
                                roundScanStartCounter(rmillisUntilFinished.intValue());
                                updateGameTimer(getRoundPartItem().get_id(), getRoundPartItem().getRound_id(), -1);
                                timerButton.resumeProgress();
                                rmillisUntilFinished = null;
                            } else {
                                rmillisUntilFinished = cmillisUntilFinished;
                                updateGameTimer(getRoundPartItem().get_id(), getRoundPartItem().getRound_id(), cmillisUntilFinished);
                                timerButton.pauseProgress();
                                if (mainCountdownTimer != null) {
                                    mainCountdownTimer.cancel();
                                }
                            }
                        }
                    }
                }
            });
            timerButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (timerButton.getProgress() !=0) {
                        timerButton.resumeProgress();
                        timerButton.setProgress(-1);
                        timerButton.setStrokeColor(Color.BLUE);
                        timerButton.setClickable(true);
                        if (mainCountdownTimer != null) {
                            mainCountdownTimer.cancel();
                        }
                        mainCountdownTimer = null;
                        rmillisUntilFinished = null;
                        cmillisUntilFinished = null;
                        lastEntryTimeEditText.setText("");
                        lastHitTextView.setVisibility(View.INVISIBLE);
                    }
                    return true;
                }
            });


            ballsInBaskethandlerButton = (NumberPicker) itemView.findViewById(R.id.balls_in_basket_elegant_button);
            ballsInBaskethandlerButton.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    Log.d("ttttt", "onValueChange: ");
                    updateFirstPoint(newVal);
                }
            });

        }

        private void roundScanStartCounter(final int kkk){
            if (mainCountdownTimer != null){
                mainCountdownTimer.cancel();
            }
            mainCountdownTimer = new CountDownTimer(kkk, 1000) {

                public void onTick(long millisUntilFinished) {
                    cmillisUntilFinished = millisUntilFinished;
                    if (cronometer != null) {
                        long second = (millisUntilFinished / 1000) % 60;
                        long minute = (millisUntilFinished / (1000 * 60)) % 60;
                        String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                        cronometer.setText(time);
                    }
                    if (itemView == null ||  !itemView.isShown()){
                        mainCountdownTimer.cancel();
                        mainCountdownTimer = null;
                    }
                    int left =  (int)((millisUntilFinished*100)/(1000*60*ROUND_TIME));
                    if (left== 100){
                        left = 99;
                    }
                    timeLeft = millisUntilFinished;
                    if (left <= 0) {
                        mainCountdownTimer.onFinish();
                        mainCountdownTimer.cancel();
                    }else {
                        timerButton.setProgress(left);
                    }
                }

                public void onFinish() {
                    if (cronometer != null){
                        cronometer.setText("");
                    }
                    timeLeft = null;
                    timerButton.setClickable(true);
                    timerButton.setCompleteText("Time Up");
                    timerButton.setProgress(100);
                    Toast.makeText(timerButton.getContext(), R.string.main_round_timer_done,Toast.LENGTH_LONG).show();
                    Vibrator v = (Vibrator) timerButton.getContext().getSystemService(timerButton.getContext().VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    }else{
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                }
            };
            mainCountdownTimer.start();

        }
        private Handler point1handler;
        public synchronized void updateFirstPoint(final int newValue){
            if (point1handler == null){
                point1handler =  new Handler();
            }

            point1handler.removeCallbacksAndMessages(null);
            Runnable myRunnable = new Runnable() {
                public void run() {
                    updateGame1Points(newValue);
                }
            };
            point1handler.postDelayed(myRunnable,DELAY_SEND_UPDATE_REQUEST);
        }



        private void updateGame1Points(int newValue) {
            JSONArray params = new JSONArray();

            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {
                data.put("entered",newValue);
                data.put("progressed",1);
                if (timeLeft != null) {
                    Long time_remains = timeLeft;
                    data.put("last_update_time_remain", time_remains);
                    data.put("last_update_timestamp",  System.currentTimeMillis());
                }
                params.put(data);

                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame1Points");

                updateChangedPoints(getGroupIdFromRoundId(getRoundPartItem().getRound_id()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    class Game2ViewHolder extends RoundItemViewHolder{
        private final TextView cronometer;
        public final CircularProgressButton timerButton;


        public ImprovedSpinner bigCube;
        public ImprovedSpinner bigPyramid;
        public ImprovedSpinner bigBall;
        public ImprovedSpinner mediumCube;
        public ImprovedSpinner mediumPyramid;
        public ImprovedSpinner mediumBall;
        public ImprovedSpinner smallCube;
        public ImprovedSpinner smallPyramid;
        public ImprovedSpinner smallBall;
        Button resetGameButton;
        public ImprovedSpinner requiredPointsSpinner;
        public TextView currentPoints;

        private CountDownTimer mainCountdownTimer;

        private Long cmillisUntilFinished;
        private Long rmillisUntilFinished;


        final private Integer ROUND_TIME = 2;
        private Long startTime = null;
        private Long timeLeft;
        private Long roundLastStamp;
        public Long bindTime;


        public void setRoundTimer(Long time){
            if (rmillisUntilFinished == null) {
                if (roundLastStamp + time > System.currentTimeMillis()) {
                    timeLeft = time;
                    startTime = time;
                    timerButton.setClickable(false);
                    roundScanStartCounter((int) (startTime - getDiffFromLastStampToNow()));
                } else {
                    if (timerButton.getProgress() == 0 || timerButton.getProgress() == 100) {
                        timerButton.setProgress(-1);
                        timerButton.setClickable(true);
                    }
                }
            }
        }

        public void setRoundLastStamp(long roundLastStamp) {
            this.roundLastStamp = roundLastStamp;
        }


        private Long getDiffFromLastStampToNow(){
            if (roundLastStamp != null) {
                return System.currentTimeMillis() - roundLastStamp;
            }
            return (long)0;
        }


        @Override
        void onDetachHolder() {
            if (mainCountdownTimer != null){
                mainCountdownTimer.cancel();
                mainCountdownTimer = null;
            }
        }
        public void setProgressData(){
            Long timeLeft = getRoundPartItem().getPartTimeLeft();
            if (timeLeft != null){

                rmillisUntilFinished = timeLeft;
                int left =  (int)((rmillisUntilFinished*100)/(1000*60*ROUND_TIME));
                if (left== 100){
                    left = 99;
                }
                if (left <= 0) {
                    mainCountdownTimer.onFinish();
                    mainCountdownTimer.cancel();
                }else {
                    timerButton.setProgress(left);
                    timerButton.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            timerButton.pauseProgress();
                            if (cronometer != null) {
                                long second = (rmillisUntilFinished / 1000) % 60;
                                long minute = (rmillisUntilFinished / (1000 * 60)) % 60;
                                String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                                cronometer.setText(time);
                            }
                        }
                    },1000);

                }

                if (mainCountdownTimer != null){
                    mainCountdownTimer.cancel();
                    mainCountdownTimer = null;
                }
            }
        }

        public Game2ViewHolder(View itemView) {
            super(itemView);

            cronometer = (TextView)itemView.findViewById(R.id.crono_timer_text);
            timerButton = (CircularProgressButton)itemView.findViewById(R.id.circular_timer_button_start);
            currentPoints = (TextView)itemView.findViewById(R.id.current_points);
            requiredPointsSpinner = (ImprovedSpinner)itemView.findViewById(R.id.max_points);
            resetGameButton = (Button)itemView.findViewById(R.id.change_game_button);
            resetGameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetGame(getRoundPartItem().get_id());
                }
            });
            timerButton.setProgress(0);
            timerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bindTime + 5000 < System.currentTimeMillis()) {
                        if (timerButton.getProgress() <= 0 || timerButton.getProgress() == 100) {
                            //timerButton.setClickable(false);
                            timerButton.setProgress(0);
                            roundScanStartCounter(1000 * 60 * ROUND_TIME);
                        } else {
                            if (rmillisUntilFinished != null) {
                                roundScanStartCounter(rmillisUntilFinished.intValue());
                                updateGameTimer(getRoundPartItem().get_id(), getRoundPartItem().getRound_id(), -1);
                                timerButton.resumeProgress();
                                rmillisUntilFinished = null;
                            } else {
                                rmillisUntilFinished = cmillisUntilFinished;
                                updateGameTimer(getRoundPartItem().get_id(), getRoundPartItem().getRound_id(), cmillisUntilFinished);
                                timerButton.pauseProgress();
                                if (mainCountdownTimer != null) {
                                    mainCountdownTimer.cancel();
                                }
                            }
                        }

                    }
                }
            });
            timerButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (timerButton.getProgress() !=0) {
                        timerButton.resumeProgress();
                        timerButton.setProgress(-1);
                        timerButton.setClickable(true);
                        if (mainCountdownTimer != null) {
                            mainCountdownTimer.cancel();
                        }
                        mainCountdownTimer = null;
                        rmillisUntilFinished = null;
                        cmillisUntilFinished = null;
                    }
                    return true;
                }
            });
            bigCube = itemView.findViewById(R.id.big_cube);
            bigCube.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("big_cube",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            bigPyramid = itemView.findViewById(R.id.big_pyramid);
            bigPyramid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("big_pyramid",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            bigBall = itemView.findViewById(R.id.big_ball);
            bigBall.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("big_ball",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });







            mediumCube = itemView.findViewById(R.id.medium_cube);
            mediumCube.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("medium_cube",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            mediumPyramid = itemView.findViewById(R.id.medium_pyramid);
            mediumPyramid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("medium_pyramid",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            mediumBall = itemView.findViewById(R.id.medium_ball);
            mediumBall.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("medium_ball",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });






            smallCube = itemView.findViewById(R.id.small_cube);
            smallCube.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("small_cube",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            smallPyramid = itemView.findViewById(R.id.small_pyramid);
            smallPyramid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("small_pyramid",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            smallBall = itemView.findViewById(R.id.small_ball);
            smallBall.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("small_ball",position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            requiredPointsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateGame2Points("req_points",position+5);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });




        }
        private void roundScanStartCounter(final int kkk){
            if (mainCountdownTimer != null){
                mainCountdownTimer.cancel();
            }
            mainCountdownTimer = new CountDownTimer(kkk, 1000) {

                public void onTick(long millisUntilFinished) {
                    cmillisUntilFinished = millisUntilFinished;
                    if (cronometer != null) {
                        long second = (millisUntilFinished / 1000) % 60;
                        long minute = (millisUntilFinished / (1000 * 60)) % 60;
                        String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                        cronometer.setText(time);
                    }
                    if (itemView == null ||  !itemView.isShown()){
                        mainCountdownTimer.cancel();
                        mainCountdownTimer = null;
                    }
                    int left =  (int)((millisUntilFinished*100)/(1000*60*ROUND_TIME));
                    if (left== 100){
                        left = 99;
                    }
                    timeLeft = millisUntilFinished;
                    if (left <= 0) {
                        mainCountdownTimer.onFinish();
                        mainCountdownTimer.cancel();
                    }else {
                        timerButton.setProgress(left);
                    }
                }

                public void onFinish() {
                    if (cronometer != null){
                        cronometer.setText("");
                    }
                    timeLeft = null;
                    timerButton.setProgress(100);
                    Toast.makeText(timerButton.getContext(), R.string.main_round_timer_done,Toast.LENGTH_LONG).show();
                    Vibrator v = (Vibrator) timerButton.getContext().getSystemService(timerButton.getContext().VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    }else{
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                }
            };
            mainCountdownTimer.start();

        }


        @Override
        void onRoundMarkedDone() {

            bigCube.setEnabled(false);
            bigPyramid.setEnabled(false);
            bigBall.setEnabled(false);

            mediumCube.setEnabled(false);
            mediumPyramid.setEnabled(false);
            mediumBall.setEnabled(false);

            smallCube.setEnabled(false);
            smallPyramid.setEnabled(false);
            smallBall.setEnabled(false);
            if (Config.isUserObserver())
                timerButton.setVisibility(View.INVISIBLE);
        }

        @Override
        void onRoundMarkedNotDone() {
            bigCube.setEnabled(true);
            bigPyramid.setEnabled(true);
            bigBall.setEnabled(true);

            mediumCube.setEnabled(true);
            mediumPyramid.setEnabled(true);
            mediumBall.setEnabled(true);

            smallCube.setEnabled(true);
            smallPyramid.setEnabled(true);
            smallBall.setEnabled(true);


        }

        private void updateGame2Points(String key,int newValue) {
            JSONArray params = new JSONArray();
            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {
                data.put(key,newValue);
                if (timeLeft != null) {
                    Long time_remains = timeLeft;
                    data.put("last_update_time_remain", time_remains);
                    data.put("last_update_timestamp",  System.currentTimeMillis());
                }
                //data.put("req_points", requiredPointsSpinner.getSelectedItem().toString());
                data.put("progressed",1);
                params.put(data);

                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame2Points");
                updateChangedPoints(getGroupIdFromRoundId(getRoundPartItem().getRound_id()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    class Game3ViewHolder extends RoundItemViewHolder{
        private final TextView cronometer;
        private final CircularProgressButton timerButton;
        private TextView descriptionTextView;
        private TextView titleTextView;
        private Button updateVoltageButton;
        Button resetGameButton;
        private EditText ballsInBaskethandlerButton;
        private CountDownTimer mainCountdownTimer;

        final private Integer ROUND_TIME = 1;
        private Long startTime = null;
        private Long timeLeft;
        private Long roundLastStamp;
        private Long cmillisUntilFinished;
        private Long rmillisUntilFinished;
        Long bindTime;

        @Override
        void onRoundMarkedDone() {
            ballsInBaskethandlerButton.setFocusable(false);
            if (Config.isUserObserver())
                timerButton.setVisibility(View.INVISIBLE);
        }

        @Override
        void onRoundMarkedNotDone() {
            ballsInBaskethandlerButton.setFocusable(true);
        }


        @Override
        void onDetachHolder() {
            if (mainCountdownTimer != null){
                mainCountdownTimer.cancel();
                mainCountdownTimer = null;
            }
        }

        public void setRoundTimer(Long time){
            if (rmillisUntilFinished == null) {
                if (roundLastStamp + time > System.currentTimeMillis()) {
                    timeLeft = time;
                    startTime = time;
                    timerButton.setClickable(false);
                    timerButton.setProgress(99);
                    roundScanStartCounter((int) (startTime - getDiffFromLastStampToNow()));
                } else {
                    if (timerButton.getProgress() == 0 || timerButton.getProgress() == 100) {
                        timerButton.setProgress(-1);
                        timerButton.setClickable(true);
                    }
                }
            }
        }


        public void setProgressData(){
            Long timeLeft = getRoundPartItem().getPartTimeLeft();
            if (timeLeft != null){

                rmillisUntilFinished = timeLeft;
                int left =  (int)((rmillisUntilFinished*100)/(1000*60*ROUND_TIME));
                if (left== 100){
                    left = 99;
                }
                if (left <= 0) {
                    mainCountdownTimer.onFinish();
                    mainCountdownTimer.cancel();
                }else {
                    timerButton.setProgress(left);
                    timerButton.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            timerButton.pauseProgress();
                            if (cronometer != null) {
                                long second = (rmillisUntilFinished / 1000) % 60;
                                long minute = (rmillisUntilFinished / (1000 * 60)) % 60;
                                String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                                cronometer.setText(time);
                            }
                        }
                    },1000);

                }

                if (mainCountdownTimer != null){
                    mainCountdownTimer.cancel();
                    mainCountdownTimer = null;
                }
            }
        }




        public void setTitle(String title){
            titleTextView.setText(title);
        }

        public void setDescription(String title){
            descriptionTextView.setText(title);
        }

        public Game3ViewHolder(View itemView) {
            super(itemView);
            cronometer = (TextView)itemView.findViewById(R.id.crono_timer_text);
            resetGameButton = (Button)itemView.findViewById(R.id.change_game_button);
            resetGameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetGame(getRoundPartItem().get_id());
                }
            });
            timerButton = (CircularProgressButton)itemView.findViewById(R.id.circular_timer_button_start);
            timerButton.setProgress(0);
            timerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bindTime + 5000 < System.currentTimeMillis()) {
                        if (timerButton.getProgress() <= 0 || timerButton.getProgress() == 100) {
                            //timerButton.setClickable(false);
                            timerButton.setProgress(0);
                            roundScanStartCounter(1000 * 60 * ROUND_TIME);
                        } else {
                            if (rmillisUntilFinished != null) {
                                roundScanStartCounter(rmillisUntilFinished.intValue());
                                updateGameTimer(getRoundPartItem().get_id(), getRoundPartItem().getRound_id(), -1);
                                timerButton.resumeProgress();
                                rmillisUntilFinished = null;
                            } else {
                                rmillisUntilFinished = cmillisUntilFinished;
                                updateGameTimer(getRoundPartItem().get_id(), getRoundPartItem().getRound_id(), cmillisUntilFinished);
                                timerButton.pauseProgress();
                                if (mainCountdownTimer != null) {
                                    mainCountdownTimer.cancel();
                                }
                            }
                        }

                    }

                }
            });
            timerButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (timerButton.getProgress() !=0) {
                        timerButton.resumeProgress();
                        timerButton.setProgress(-1);
                        timerButton.setClickable(true);
                        if (mainCountdownTimer != null) {
                            mainCountdownTimer.cancel();
                        }
                        mainCountdownTimer = null;
                        rmillisUntilFinished = null;
                        cmillisUntilFinished = null;
                    }
                    return true;
                }
            });
            descriptionTextView = (TextView)itemView.findViewById(R.id.point_description);
            titleTextView = (TextView)itemView.findViewById(R.id.title);
            ballsInBaskethandlerButton = (EditText)itemView.findViewById(R.id.voltage_editbox);
            ballsInBaskethandlerButton.setGravity(Gravity.CENTER);
            ballsInBaskethandlerButton.setImeOptions(EditorInfo.IME_ACTION_DONE);
            updateVoltageButton = (Button)itemView.findViewById(R.id.button);
            updateVoltageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ballsInBaskethandlerButton.getText() != null && !TextUtils.isEmpty(ballsInBaskethandlerButton.getText())) {
                        updateGame3Points(Double.parseDouble(ballsInBaskethandlerButton.getText().toString()));
                    }
                }
            });
        }

        private void roundScanStartCounter(final int kkk){
            if (mainCountdownTimer != null){
                mainCountdownTimer.cancel();
            }
            mainCountdownTimer = new CountDownTimer(kkk, 1000) {

                public void onTick(long millisUntilFinished) {
                    cmillisUntilFinished = millisUntilFinished;
                    if (cronometer != null) {
                        long second = (millisUntilFinished / 1000) % 60;
                        long minute = (millisUntilFinished / (1000 * 60)) % 60;
                        String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                        cronometer.setText(time);
                    }
                    if (itemView == null ||  !itemView.isShown()){
                        mainCountdownTimer.cancel();
                        mainCountdownTimer = null;
                    }

                    int left =  (int)((millisUntilFinished*100)/(1000*60*ROUND_TIME));
                    if (left== 100){
                        left = 99;
                    }
                    timeLeft = millisUntilFinished;
                    if (left <= 0) {
                        mainCountdownTimer.onFinish();
                        mainCountdownTimer.cancel();
                    }else
                        timerButton.setProgress(left);

                }

                public void onFinish() {
                    if (cronometer != null){
                        cronometer.setText("");
                    }
                    timeLeft = null;
                    timerButton.setProgress(100);
                    Toast.makeText(timerButton.getContext(), R.string.main_round_timer_done,Toast.LENGTH_LONG).show();
                    Vibrator v = (Vibrator) timerButton.getContext().getSystemService(timerButton.getContext().VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    }else{
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                }
            };
            mainCountdownTimer.start();

        }


        private void updateGame3Points(double newValue) {

            JSONArray params = new JSONArray();

            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {
                data.put("volts",newValue);
                data.put("progressed",1);

                if (timeLeft != null) {
                    Long time_remains = timeLeft;
                    data.put("last_update_time_remain", time_remains);
                    data.put("last_update_timestamp",  System.currentTimeMillis());
                }
                params.put(data);

                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame3Points");
                updateChangedPoints(getGroupIdFromRoundId(getRoundPartItem().getRound_id()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        public void setRoundLastStamp(long roundLastStamp) {
            this.roundLastStamp = roundLastStamp;
        }


        private Long getDiffFromLastStampToNow(){
            if (roundLastStamp != null) {
                return System.currentTimeMillis() - roundLastStamp;
            }
            return (long)0;
        }

    }



    class Game11ViewHolder extends RoundItemViewHolder{

        private TextView titleTextView;

        private TextView description1TextView;
        private TextView description2TextView;

        private NumberPicker firstPointButton;
        private NumberPicker secondPointButton;

        private Handler point1handler;
        private Handler point2handler;


        final private Integer ROUND_TIME = 2;
        private Long timeLeft;


        @Override
        void onRoundMarkedDone() {
            firstPointButton.setEnabled(false);
            secondPointButton.setEnabled(false);
        }

        @Override
        void onRoundMarkedNotDone() {
            firstPointButton.setEnabled(true);
            secondPointButton.setEnabled(true);
        }

        @Override
        void onDetachHolder() {

        }


        public void setTitle(String title){
            titleTextView.setText(title);
        }

        public void setDescription1(String title){
            description1TextView.setText(title);
        }
        public void setDescription2(String title){
            description2TextView.setText(title);
        }

        public void onBindChecks(){

        }


        public Game11ViewHolder(View itemView) {
            super(itemView);

            description1TextView = (TextView)itemView.findViewById(R.id.first_description);
            description2TextView = (TextView)itemView.findViewById(R.id.second_description);
            titleTextView = (TextView)itemView.findViewById(R.id.title);


            firstPointButton = (NumberPicker) itemView.findViewById(R.id.first_point_button);
            secondPointButton = (NumberPicker)itemView.findViewById(R.id.second_point_button);
            firstPointButton.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    updateFirstPoint(newVal);
                }
            });

            secondPointButton.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    updateSecondPoint(newVal);
                }
            });

        }


        public synchronized void updateFirstPoint(final int newValue){
            if (point1handler == null){
                point1handler =  new Handler();
            }

            point1handler.removeCallbacksAndMessages(null);
            Runnable myRunnable = new Runnable() {
                public void run() {
                    updateGame1Points(newValue);
                }
            };
            point1handler.postDelayed(myRunnable,DELAY_SEND_UPDATE_REQUEST);
        }


        private void updateGame1Points(int newValue) {
            JSONArray params = new JSONArray();

            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {
                data.put("point1",newValue);
                data.put("progressed",1);
                params.put(data);
                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame11Point1");

                updateChangedPoints(getGroupIdFromRoundId(getRoundPartItem().getRound_id()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }



        public synchronized void updateSecondPoint(final int newValue){
            if (point2handler == null){
                point2handler =  new Handler();
            }

            point2handler.removeCallbacksAndMessages(null);
            Runnable myRunnable = new Runnable() {
                public void run() {
                    updateGame2Points(newValue);
                }
            };
            point2handler.postDelayed(myRunnable,DELAY_SEND_UPDATE_REQUEST);
        }


        private void updateGame2Points(int newValue) {
            JSONArray params = new JSONArray();

            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {
                data.put("point2",newValue);
                data.put("progressed",1);
                params.put(data);
                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame11Point2");

                updateChangedPoints(getGroupIdFromRoundId(getRoundPartItem().getRound_id()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }



    }



    class Game12ViewHolder extends RoundItemViewHolder{

        private TextView titleTextView;

        private TextView description1TextView;

        private NumberPicker firstPointButton;
        public TextView groupSpinner;

        private Handler point1handler;


        @Override
        void onRoundMarkedDone() {
            firstPointButton.setEnabled(false);
        }

        @Override
        void onRoundMarkedNotDone() {
            firstPointButton.setEnabled(true);
        }

        @Override
        void onDetachHolder() {

        }

        public void setTitle(String title){
            titleTextView.setText(title);
        }

        public void setDescription1(String title){
            description1TextView.setText(title);
        }


        public void onBindChecks(){

        }


        public Game12ViewHolder(View itemView) {
            super(itemView);

            description1TextView = (TextView)itemView.findViewById(R.id.first_description);
            titleTextView = (TextView)itemView.findViewById(R.id.title);
            groupSpinner = (TextView)itemView.findViewById(R.id.crack_group);


            firstPointButton = (NumberPicker) itemView.findViewById(R.id.first_point_button);
            firstPointButton.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    updateFirstPoint(newVal);
                }
            });


        }






        public synchronized void updateFirstPoint(final int newValue){
            if (point1handler == null){
                point1handler =  new Handler();
            }

            point1handler.removeCallbacksAndMessages(null);
            Runnable myRunnable = new Runnable() {
                public void run() {
                    String groupId = null;
                    try {
                        groupId = workTeamPart.getValue().getString("group_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    updateCrackerPoints(newValue,groupId);
                }
            };
            point1handler.postDelayed(myRunnable,DELAY_SEND_UPDATE_REQUEST);
        }








    }

    class Game13ViewHolder extends RoundItemViewHolder{


        public EditText endTimeEditText;

        private Handler point1handler;


        @Override
        void onRoundMarkedDone() {
            endTimeEditText.setEnabled(false);
        }

        @Override
        void onRoundMarkedNotDone() {
            endTimeEditText.setEnabled(true);
        }

        @Override
        void onDetachHolder() {

        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onRoundDataChange(TimeToFourty event){
            if (event.getRoundId().equals(getRoundPartItem().getRound_id())) {
                endTimeEditText.setText(event.getTime());
            }
        }





        public Game13ViewHolder(View itemView) {
            super(itemView);

            endTimeEditText = (EditText)itemView.findViewById(R.id.time_end);

            endTimeEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            endTimeEditText.addTextChangedListener( new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                    String timeAsString = s.toString();
                    if (timeAsString.length() == 5 && timeAsString.substring(2, 3).equals(":")) {
                        try {
                            int minutes = Integer.parseInt(timeAsString.substring(0, 2));
                            int seconds = Integer.parseInt(timeAsString.substring(3));
                            Integer newValue = minutes * 60 + seconds;
                            updateFirstPoint(newValue);
                            endTimeEditText.setTextColor(Color.GREEN);
                        } catch (NumberFormatException ex) {
                            endTimeEditText.setTextColor(Color.RED);
                        }
                    }else{
                        endTimeEditText.setTextColor(Color.RED);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

        }



        public synchronized void updateFirstPoint(final int newValue){
            if (point1handler == null){
                point1handler =  new Handler();
            }

            point1handler.removeCallbacksAndMessages(null);
            Runnable myRunnable = new Runnable() {
                public void run() {
                    updateGame1Points(newValue);
                }
            };
            point1handler.postDelayed(myRunnable,DELAY_SEND_UPDATE_REQUEST);
        }


        private void updateGame1Points(int newValue) {
            JSONArray params = new JSONArray();

            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {
                if (newValue >2400){
                    newValue = 2400;
                }
                data.put("entered",newValue);
                data.put("progressed",1);
                params.put(data);
                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame13Point");

                updateChangedPoints(getGroupIdFromRoundId(getRoundPartItem().getRound_id()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }







    }



    class Game14ViewHolder extends RoundItemViewHolder{


        public final NumberPicker excelledPointButton;
        public EditText endTimeEditText;

        private Handler point1handler;


        @Override
        void onRoundMarkedDone() {
            endTimeEditText.setEnabled(false);
            excelledPointButton.setEnabled(false);
        }

        @Override
        void onRoundMarkedNotDone() {
            endTimeEditText.setEnabled(true);
            excelledPointButton.setEnabled(true);
        }

        @Override
        void onDetachHolder() {

        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onRoundDataChange(TimeToFourty event){
            if (event.getRoundId().equals(getRoundPartItem().getRound_id())) {
                endTimeEditText.setText(event.getTime());
            }
        }



        public Game14ViewHolder(View itemView) {
            super(itemView);

            endTimeEditText = (EditText)itemView.findViewById(R.id.time_end);
            excelledPointButton = (NumberPicker)itemView.findViewById(R.id.excelled_points);
            excelledPointButton.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    updatePoint(newVal,null);
                }
            });





            endTimeEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            endTimeEditText.addTextChangedListener( new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                    String str = s.toString();
                    updatePoint(excelledPointButton.getValue(),str);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

        }



        public synchronized void updatePoint(final int newValue,final String puzzleName){
            if (point1handler == null){
                point1handler =  new Handler();
            }

            point1handler.removeCallbacksAndMessages(null);
            Runnable myRunnable = new Runnable() {
                public void run() {
                    updateGame14Points(newValue,puzzleName);
                }
            };
            point1handler.postDelayed(myRunnable,DELAY_SEND_UPDATE_REQUEST);
        }


        private void updateGame14Points(int newValue,String puzzleName) {
            JSONArray params = new JSONArray();

            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {
                if (newValue >2400){
                    newValue = 2400;
                }
                data.put("entered",newValue);
                if (puzzleName != null){
                    data.put("puzzle_name",puzzleName);
                }
                data.put("progressed",1);
                params.put(data);
                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame13Point");

                updateChangedPoints(getGroupIdFromRoundId(getRoundPartItem().getRound_id()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }







    }

    public String getGroupIdFromRoundId(String roundId){
        RoundItem roundItem = (RoundItem)Config.roundsMap.get(roundId) ;
        return roundItem.getGroup();
    }

    private void updateCrackerPoints(Integer newValue ,String groupId) {
        JSONArray params = new JSONArray();

        if (workTeamPart == null){
            Toast.makeText(MonaAdminApp.getInstance().getApplicationContext(),"ניקוד צוות מפצח לא התעדכן!!",Toast.LENGTH_LONG).show();
            return;
        }
        params.put(workTeamPart.get_id());
        params.put(Config.AdminStatus);
        params.put(workTeamPart.getRound_id());
        JSONObject data = new JSONObject();
        try {
            if (newValue != null) {
                data.put("point1", newValue);
            }
            if (groupId != null) {
                data.put("group_id", groupId);
            }
            data.put("progressed",1);
            params.put(data);
            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                }
            });
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame11Point1");

            updateChangedPoints(getGroupIdFromRoundId(workTeamPart.getRound_id()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void updateGameTimer(String roundPartId,String roundId,long remainedTime) {
        JSONArray params = new JSONArray();

        params.put(roundPartId);
        params.put(Config.AdminStatus);
        params.put(roundId);
        JSONObject data = new JSONObject();
        try {
            data.put("paused",remainedTime);

            params.put(data);

            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                }
            });
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "updateGame1Points");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    class ChoosePartDataViewHolder extends RoundItemViewHolder{
        Spinner gamePartPicker;
        Button confirmNewGameButton;
        public ChoosePartDataViewHolder(View itemView) {
            super(itemView);
            gamePartPicker = (Spinner)itemView.findViewById(R.id.pick_game_spinner);
            confirmNewGameButton = (Button)itemView.findViewById(R.id.pick_game_button);
            confirmNewGameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("ttttt", "onClick: ");
                    //TODO send request specify game.
                    //progress wait.
                    //update properly the addition

                    requestAddGame(gamePartPicker.getSelectedItemPosition());
                }
            });
        }

        private void requestAddGame(int selectedItemPosition) {
            Integer t = selectedItemPosition+1;
            JSONArray params = new JSONArray();
            params.put(getRoundPartItem().get_id());
            params.put(Config.AdminStatus);
            params.put(getRoundPartItem().getRound_id());
            JSONObject data = new JSONObject();
            try {
                data.put("data_type","game" + t.toString());
                //data.put("entered",0);
                params.put(data);

                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND_PART,params,new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Amir", "pick game error: " + error.getMessage());
                    }
                });
                MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "requestAddGame");

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        void onDetachHolder() {

        }
        @Override
        void onRoundMarkedDone() {
            if (Config.isUserObserver()) {
                confirmNewGameButton.setEnabled(false);
            }else{
                confirmNewGameButton.setEnabled(true);
            }
        }

        @Override
        void onRoundMarkedNotDone() {
            if (Config.isUserObserver()) {
                confirmNewGameButton.setEnabled(false);
            }else{
                confirmNewGameButton.setEnabled(true);
            }
        }


    }


}
