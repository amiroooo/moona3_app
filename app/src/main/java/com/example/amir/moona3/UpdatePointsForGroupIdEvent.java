package com.example.amir.moona3;

/**
 * Created by Amir on 3/28/2018.
 */

public class UpdatePointsForGroupIdEvent {
    String id;
    public UpdatePointsForGroupIdEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
